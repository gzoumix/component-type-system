module ReplicationSystem.Server.ConnectionThread;

export ConnectionThreadImpl;

import * from ReplicationSystem.Environment.Files;
import * from ReplicationSystem.Environment.DataTypes;
import * from ReplicationSystem.Environment.ReplicationSnapshot.Interfaces;

import * from ReplicationSystem.Client.Interfaces;
import * from ReplicationSystem.Server.Interfaces;

import * from ReplicationSystem.Schedulers;
import * from ReplicationSystem.Server.SyncServerAcceptor;

[COG]  
class ConnectionThreadImpl(
		[Far] ClientJob job, 
		[Far] SyncServer server, 
		Int id) implements ConnectionThread {

	SyncServerClientCoordinator coord;
	Maybe<Command> cmd = Nothing;
	Schedules schedules = EmptySet;
	
	ReplicationSnapshot startReplicationUpdate() {
		//expects only one schedule per replication job
		//assert size(schedules) == 1;
			
		Schedule schedule = snd(next(schedules));
			
		// register and refresh snapshot
		Fut<Unit> rp = coord!startReplicationUpdate(schedule, this); await rp?;
		
		Fut<ReplicationSnapshot> sp = server!getReplicationSnapshot(schedule);
		return sp.get;
	}
	
	Unit finishReplicationUpdate() {
		//assert size(schedules) == 1;
		Fut<Unit> rp = coord!finishReplicationUpdate(snd(next(schedules)), this); 
		await rp?;
	}
	
	Unit run() {
		Fut<SyncServerClientCoordinator> c = server!getCoordinator(); await c?;
		this.coord = c.get;
		
		// wait for client's command
		await this.cmd != Nothing;
		
		// Send schedules
		schedules = this.sendSchedule();
		
		if (cmd != Just(ListSchedule)) {
			
			// Get replication items
			ReplicationSnapshot snapshot = this.startReplicationUpdate();
			
			Fut<TransactionId> idf = snapshot!getIndexingId(); await idf?;
			TransactionId tid = idf.get;
			
			Fut<Bool> b = job!registerReplicationItems(tid); await b?;
			Bool register = b.get;
			
			Set<Set<File>> filesets = EmptySet;
			if (register) {
				Fut<Set<ServerReplicationItem>> nis = snapshot!getItems(ssname(fromJust(cmd)));	
				await nis?; Set<ServerReplicationItem> newitems = nis.get;
				filesets = this.registerItems(newitems);
			}
			
			// start snapshot
			Fut<Unit> rp = this.job!command(StartSnapShot); await rp?;
			
			while (hasNext(filesets)) {
				Pair<Set<Set<File>>,Set<File>> nfs = next(filesets);
				filesets = fst(nfs);
				Set<File> fileset = snd(nfs); 
				this.transferItems(fileset);
			}
			
			// end snapshot		
			rp = this.job!command(EndSnapShot); await rp?;
			
			// tidy up
			this.finishReplicationUpdate();
		}
	}
	
	// send one or more schedules to client job
	Set<Schedule> sendSchedule() {
		assert isJust(cmd);
		Set<Schedule> results = EmptySet;
		if (cmd == Just(ListSchedule)) {
			Fut<Set<Schedule>> ssf = server!listSchedules();
			await ssf?; results = ssf.get;
		} else { 
			Fut<Schedule> ssf = server!getSchedule(ssname(fromJust(cmd)));
			await ssf?; Schedule s = ssf.get;
			results = Insert(s,EmptySet);
		}
		Fut<Unit> rp = this.job!receiveSchedule(results); await rp?;
		return results;
	}
	
	ClientId forClient() {
		Fut<ClientId> id = job!forClient();
		return id.get;
	}
	
	[Atomic] Unit command(Command c) { 
		this.cmd = Just(c); 
	}
	
	/*
	 * Register replication items with client 
	 * Returns a set of files to be replicated
	 */
	Set<Set<File>> registerItems(Set<ServerReplicationItem> items) {
		Set<Set<File>> regs = EmptySet;	
	
		List<ServerReplicationItem> itemsList = setToList(items);
		//iterate over possible check points
		while (length(itemsList) > 0) {
			ServerReplicationItem item = head(itemsList);
			itemsList = tail(itemsList);

			// For now convert to a set
			// will convert it into directory
			Fut<FileEntry> entryf = item!getContents(); await entryf?;
			FileEntry entry = entryf.get;
			regs = Insert(entryToFiles(entry),regs);
		}
		
		return regs;
	}
	
	Unit transferItems(Set<File> fileset) {
		while (hasNext(fileset)) {
			Pair<Set<File>,File> nf = next(fileset); 
			fileset = fst(nf);
			File file = snd(nf);
			FileSize tsize = fileContent(file);
			
			Fut<Unit> rp = job!command(AppendSearchFile); await rp?;
			Fut<Maybe<FileSize>> fs = job!processFile(fst(file)); await fs?;
			Maybe<FileSize> content = fs.get;
			
			FileSize size = 0;
			if (isJust(content)) { 
				size = fromJust(content);
			}
			
			if (size > tsize) {
				rp = job!command(OverwriteFile);
				await rp?;
				rp = job!processContent(file);
				await rp?;
			} else {
				// find out how much is still need to be replicated
				if (tsize - size > 0) {
					rp = job!command(ContinueFile);
					await rp?;
					
					file = file(fst(file),tsize - size);
					rp = job!processContent(file);
					await rp?;
				} else {
					rp = job!command(SkipFile);
					await rp?;
				}
			}

		}
		Fut<Unit> rp = job!command(EndSearchFile); await rp?;
	}
}