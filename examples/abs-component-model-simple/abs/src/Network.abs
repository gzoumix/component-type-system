module Replication.Network;

export *;

import * from ReplicationSystem.Client.Interfaces;
import * from ReplicationSystem.Server.Interfaces;
import * from ReplicationSystem.Interfaces;
import * from ReplicationSystem.Environment.Interfaces;
import * from ReplicationSystem.Tests;
import * from ReplicationSystem.Environment.Files;

interface Network {
	Unit shutDown(ClientConnector client);
	Bool powerDown();
	Unit shutDownUpdater();
}

class Network(SyncServer server, Int clients) implements Network {

	Bool powerDown = False;
	List<ClientDataBase> databases = Nil;
	
	Unit shutDown(ClientConnector client) {
		Fut<ClientDataBase> df = client!getClientDataBase();
		ClientDataBase db = df.get;
		databases = Cons(db, databases);
		if (length(databases) == clients) {
			powerDown = True;
		}
	}
	
	Bool powerDown() {
		return powerDown;
	}
	
	Unit shutDownUpdater() {
		assert powerDown == True;
		Fut<Unit> ss = server!requestShutDown(); ss.get;
		while (length(databases) > 0) {
			Tester tester = new cog TesterImpl(server, head(databases)); 
			tester!analyse();
			databases = tail(databases);
		}
	}
}