module ReplicationSystem.Server.SyncServerClientCoordinator;

export *;

import * from ReplicationSystem.Environment.Files;
import * from ReplicationSystem.Environment.DataTypes;
import * from ReplicationSystem.Environment.ReplicationSnapshot.Interfaces;
import * from ReplicationSystem.Server.Interfaces;

// CSP model CoordinatorProcess
// Java class com.fredhopper.replication.server.SyncServerClientCoordinator
[COG] 
class SyncServerClientCoordinatorImpl implements SyncServerClientCoordinator {

	Map<Schedule,Set<ConnectionThread>> threadMaps = EmptyMap;
	Map<Schedule,ReplicationSnapshot> snapshots = EmptyMap;
	
	//Cannot be used in concurrent setting
	Set<ConnectionThread> threads = EmptySet;
	
	//take this out from the constructor so that it
	//can be modified
	Unit setSnapshots(Map<Schedule,ReplicationSnapshot> ss) {
		snapshots = ss;
		threadMaps = setToMap(keys(ss),EmptySet);
	}
	
	// Setting up a replication session
	Unit startReplicationUpdate(Schedule s, ConnectionThread worker) {
		assert contains(keys(threadMaps),s);
		Set<ConnectionThread> threads = Insert(worker,lookupUnsafe(threadMaps,s));
		threadMaps = put(threadMaps,s,threads);
		if (size(threads) == 1) {
			//transition from 0 -> 1
			this.refreshSnapShot(s);
		}
	}
	
	// Tidy up after a replication session
	Unit finishReplicationUpdate(Schedule s, ConnectionThread worker) {
		assert contains(keys(threadMaps),s);
		Set<ConnectionThread> threads = remove(lookupUnsafe(threadMaps,s),worker);
		threadMaps = put(threadMaps,s,threads);
		if (size(threads) == 0) {
			//transition from 1 -> 0
			this.clearSnapshot(s);
		}
	}
	
	[Atomic]
	ReplicationSnapshot getSnapshot(Schedule s) {
		assert contains(keys(snapshots),s); 
		return lookupUnsafe(snapshots,s);
	}
	
	Unit refreshSnapShot(Schedule s) {
		//must contain snapshot for this schedule
		ReplicationSnapshot snapshot = this.getSnapshot(s);
		Fut<Unit> unit = snapshot!refreshSnapshot(); unit.get;
	}
	
	Unit clearSnapshot(Schedule s) {
		//must contain snapshot for this schedule
		assert contains(keys(snapshots),s); 
		ReplicationSnapshot snapshot = lookupUnsafe(snapshots,s);
		Fut<Unit> unit = snapshot!clearSnapshot(); unit.get;
	}
}