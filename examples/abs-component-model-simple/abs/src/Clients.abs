module ReplicationSystem.Client;

export *;

import * from ABS.DC;
import * from ABS.Scheduler;

import * from ReplicationSystem.Environment.DataTypes;
import * from ReplicationSystem.Environment.Files;
import * from ReplicationSystem.Environment.StateMachine;

import * from ReplicationSystem.Client.Interfaces;
import * from ReplicationSystem.Interfaces;
import * from ReplicationSystem.Server.Interfaces;
import * from ReplicationSystem.Environment.Interfaces;

import * from ReplicationSystem.Environment.DataBases;
import * from ReplicationSystem.Client.ClientJob;
import * from Replication.Network;
import * from ReplicationSystem.Schedulers;
import * from ReplicationSystem.ReplicationSystem;

// Implementation of SyncClient 
// Java class com.fredhopper.application.SyncClient
[COG] [Scheduler: edf(queue)] 
class SyncClientImpl(
	[Final] Int maxJobs, 
	[Final] ClientId id,
	[Final] ReplicationSystem sys) implements JobMaker, InternalClient, ClientConnector {
	
	Either<Schedule,Schedules> noSchedule = Left(NoSchedule);
	StateMachine machine = stateMachine();
	State state = Start;
	
	Network network;
	ServerAcceptor acceptor;
	ClientDataBase db;
	
	List<Schedule> hit = Nil;
	List<Schedule> missed = Nil;
	
	Int jobRecords = 0;
	Int jobHistories = 0;
	Int currentTransactionId = -1;
	
	//port 
	ClientBehaviour behaviour;
	

	[Atomic]
	Int jobCount() {
		return jobHistories;
	}
	
	Unit runScheduled(List<Pair<JobType, Schedule>> scheduled) {
		while (scheduled != Nil) {
			JobType jb = fst(head(scheduled));
			Schedule schedule = snd(head(scheduled));
			this!scheduleJob(jb, schedule);
			scheduled = tail(scheduled);
		}
	}
	
	critical Unit scheduleJob(JobType jb, Schedule schedule) {
		behaviour.scheduleJob(jb, schedule);
	}
	
	Unit makeJob(StateMachineChanger changer, JobType jb, Schedule schedule) {
		ClientJob job = new cog ClientJobImpl(maxJobs, this, jb, schedule, changer, jobHistories);
		job!executeJob();
		
		jobHistories = jobHistories + 1;
		jobRecords = jobRecords + 1;
	}

	Unit nextJob(Schedule schedule) {
		if (jobHistories >= maxJobs) {
			behaviour.requestShutDown();
		} else {	
			behaviour.nextJob(schedule);
		}
	}
	
	Bool isShutdownRequested() {
		Bool sd = behaviour.isShutdownRequested();
		return sd;
	}
	
	Unit requestShutDown() {
		behaviour.requestShutDown();
	}
	
	Unit becomesState(Either<Schedule,Schedules> schedule, State s) {
		behaviour.becomesState(schedule, s);
	}
	
	Unit finishJob() {
		jobRecords = jobRecords - 1;
	}
	
	ClientId getId() {
		return id;
	}
	
	ServerAcceptor getAcceptor() {
		return acceptor;
	}
	
	//Unit < {} | [g | behaviour = ClientBehaviour ] > switchJobs(Bool sequential) {
	Unit switchJobs(Bool sequential) {
		ClientBehaviour bh = null;
		if (sequential) {
			bh = new SequentialBehavior(network, this, hit, missed); 
		} else {
			bh = new ConcurrentBehavior(network, this, hit, missed);
		}
		ClientBehaviour old = this.behaviour;
		
		await |this|;
		rebind this:behaviour = bh;
		
		State state = old.getState();
		behaviour.setState(state);
		
		List<Pair<JobType, Schedule>> scheduled = old.getScheduledJobs();
		this.runScheduled(scheduled);
	}
	
	Unit run() {
		behaviour = new SequentialBehavior(network, this, hit, missed);
		behaviour.setState(Start);
		this.switchJobs(True);
		
		// Makes a transition
		this.becomesState(noSchedule, WaitToBoot);
		
		// wait for acceptor to be ready
		await acceptor != null;
		
		// starts a boot job
		StateMachineChanger changer = new cog SequentialStateMachineChanger(this);
		this.makeJob(changer, Boot, NoSchedule);
	}
	
	ClientDataBase getClientDataBase() {
		return db;
	}
	
	DataBase getDataBase() {
		return db;
	}
	
	Unit setAcceptor([Far] ServerAcceptor acc) {
		acceptor = acc;
	}
	
	Unit setNetwork(Network network) {
		this.network = network;
	}

		{
    		// initialize the client side data base
    		db = new DataBaseImpl(-1);
    	}

	
}

interface JobMaker extends InternalClient, ClientConnector {
	Unit makeJob(StateMachineChanger changer, JobType jb, Schedule schedule);
}

interface ClientBehaviour {
	List<Pair<JobType, Schedule>> getScheduledJobs();
	State getState();
	Unit setState(State state);
	Unit setNext(Schedule schedule);
	Unit scheduleJob(JobType jb, Schedule schedule);
	Bool isShutdownRequested();
	Unit requestShutDown();
	Unit becomesState(Either<Schedule,Schedules> schedule, State state);
	Unit nextJob(Schedule schedule);
	Unit windDown();
}

interface StateMachineChanger {
	Unit setSchedules(Schedules schedules);
	Unit setSchedule(Schedule schedule);
	Unit becomeState(State state);
}

class SequentialStateMachineChanger(InternalClient client) implements StateMachineChanger {

	Schedules schedules = EmptySet;
	Schedule schedule = NoSchedule;

	Unit setSchedules(Schedules schedules) {
		this.schedules = schedules;
	}
	
	Unit setSchedule(Schedule schedule) {
		this.schedule = schedule;
	}
	
	Unit becomeState(State state) {
		Fut<Unit> uf = client!becomesState(Left(schedule), state); uf.get;
	}
}

class SequentialBehavior(
	Network network,
	JobMaker jobMaker, 
	List<Schedule> hit,
	List<Schedule> missed) implements ClientBehaviour {

	StateMachine machine = stateMachine();
	State state = Start;
	StateMachineChanger changer = null;
	Bool blocked = False;
	Bool finish = False;
	Bool shutDown = False; 
	Bool next = False; 
	List<Pair<JobType, Schedule>> scheduled = Nil;
	
	Unit setState(State state) {
		this.state = state;
	}
	
	State getState() {
		return state;
	}
	
	Bool isShutdownRequested() {
		return shutDown;
	}
	
	Unit requestShutDown() {
		shutDown = True;
		await finish;
		network!shutDown(jobMaker);
	}
	
	List<Pair<JobType, Schedule>> getScheduledJobs() {
		return scheduled;
	}

	Unit scheduleJob(JobType jb, Schedule schedule) {
		if (changer == null) {
			changer = new cog SequentialStateMachineChanger(jobMaker);
		}
	
		// wait for the next available slot
		await next || shutDown || blocked;
		
		if (blocked) {
			scheduled = Cons(Pair(jb, schedule), scheduled);
		}
		
		// only proceed if a shutdown 
		// request has not been made.
		if (~shutDown && ~blocked) {
			// block subsequent ones
			this.setNext(schedule);
			jobMaker!makeJob(changer,jb,schedule);
			hit = Cons(schedule,hit);
			
			//wait for this job to finish
			await finish;
		} else {
			//record those schedules that are missed
			missed = Cons(schedule,missed);
		}
	}
	
	Unit windDown() {
		blocked = True;
	}
	
	Unit nextJob(Schedule schedule) {
		next = True; //next job can proceed
		finish = True; //this job is finished
	}
	
	Unit setNext(Schedule schedule) {
		next = False; //block next job from proceed
		finish = False; //this job has started
	}
	
	Unit becomesState(Either<Schedule,Schedules> schedule, State state) {
		Set<State> tos = lookupDefault(machine,this.state,EmptySet);
		assert tos != EmptySet; // this is an end state
		assert contains(tos,state); // cannot proceed to specified state
		this.state = state;	
	}

}

class ConcurrentStateMachineChanger(InternalClient client, JobType job) implements StateMachineChanger {

	Schedules schedules = EmptySet;
	Schedule schedule = NoSchedule;

	Unit setSchedules(Schedules schedules) {
		this.schedules = schedules;
	}
	
	Unit setSchedule(Schedule schedule) {
		this.schedule = schedule;
	}

	Unit becomeState(State state) {
		if (state == WaitToBoot) {
			Fut<Unit> unit = client!becomesState(Left(schedule), state); unit.get;
		} else if (state == Booting) { 
			Fut<Unit> unit = client!becomesState(Left(schedule), state); unit.get;
		} else if (state == WorkOnReplicate) { 
			Fut<Unit> unit = client!becomesState(Left(schedule),ManyWorkOnReplicate(schedule)); unit.get;
		} else if (state == WaitToReplicate) { 
			if (schedule == NoSchedule) {
				// transition from single to many state
				// this job must be a boot job 
				assert job == Boot;
				Fut<Unit> unit = client!becomesState(Right(schedules), WaitToReplicate); unit.get;
			} else {
				// transition from many to many state
				Fut<Unit> unit = client!becomesState(Left(schedule), ManyWaitToReplicate(schedule)); unit.get;
			}
		} else if (state == End) { 
			if (schedule == NoSchedule) {
				// transition from single to single state
				// this job must be a boot job 
				assert job == Boot;
				Fut<Unit> unit = client!becomesState(Left(NoSchedule),End); unit.get;
			} else {
				// transition from many to many state
				Fut<Unit> unit = client!becomesState(Left(schedule),ManyEnd(schedule)); unit.get;
			}
		}
	}
}

class ConcurrentBehavior(
	Network network,
	JobMaker jobMaker, 
	List<Schedule> hit,
	List<Schedule> missed) implements ClientBehaviour {

	StateMachine mmachine = concurrentStateMachine();
	Either<State,ManyState> state = Left(Start);
	
	Map<Schedule,Bool> nexts = EmptyMap; //if the job of particular schedule can proceed
	Map<Schedule,Bool> finishes = EmptyMap; //if there is a running job of particular schedule
	Bool blocked = False; //if this port is blocked
	Bool shutDown = False; //if the client has shutdown 
	List<Pair<JobType, Schedule>> scheduled = Nil;
	
	Unit setState(State state) {
		this.state = Left(state);
	}
	
	State getState() {
		assert isLeft(state); 
		return left(state);
	}
	
	Bool isShutdownRequested() {
		return shutDown;
	}
	
	Unit requestShutDown() {
		shutDown = True;
		await isAllTrues(finishes);
		network!shutDown(jobMaker);
	}
	
	List<Pair<JobType, Schedule>> getScheduledJobs() {
		return scheduled;
	}

	Unit scheduleJob(JobType jb, Schedule schedule) {
		// wait for the next available slot
		await (lookupDefault(nexts,schedule,True) || shutDown || blocked);
		
		if (blocked) {
			scheduled = Cons(Pair(jb, schedule), scheduled);
		}
		
		// only proceed if a shutdown 
		// request has not been made.
		if (~shutDown && ~blocked) {
			// block subsequent onces in the same schedule
			this.setNext(schedule);
			StateMachineChanger changer = new cog ConcurrentStateMachineChanger(jobMaker, jb);
			jobMaker!makeJob(changer,jb,schedule);
			hit = Cons(schedule,hit);
			
			//wait for this job to finish
			assert contains(keys(finishes), schedule);
			await lookupUnsafe(finishes,schedule);
		} else {
			//record those schedules that are missed
			missed = Cons(schedule,missed);
		}
	}
	
	Unit windDown() {
		blocked = True;
	}
	
	Unit nextJob(Schedule schedule) {
		nexts = put(nexts,schedule,True);
		finishes = put(finishes,schedule,True);
	}
	
	Unit setNext(Schedule schedule) {
		nexts = put(nexts,schedule,False);
		finishes = put(finishes,schedule,False);
	}
	
	Unit becomesState(Either<Schedule,Schedules> schedule, State s) {
		if (isLeft(schedule) && left(schedule) == NoSchedule) {
			//no schedule
			//must be on single state
			assert isLeft(state);
			Set<State> tos = lookupDefault(mmachine,left(state),EmptySet);
			assert tos != EmptySet; // this is an end state
			assert contains(tos,s); // cannot proceed to specified state
			state = Left(s);
		} else {
			//has schedule
			if (isLeft(state)) {
				//moving to many states, caller must supply all schedules
				assert isRight(schedule) && s == WaitToReplicate; 
				Schedules ss = right(schedule);
				//set up concurrent state machine
				mmachine = join(mmachine,makeManyStates(ss));
				//set current state to many states
				state = Right(setWaitToReplicate(ss));
			} else {
				//already at many states, a schedule must be specified
				assert isLeft(schedule) && left(schedule) != NoSchedule;
				Schedule sc = left(schedule);
				
				//we are at many state
				ManyState ms = right(state);
				
				// the current many state must be configured
				// for the specified schedule
				assert contains(keys(ms),sc);
				
				Set<State> tos = lookupDefault(mmachine, lookupUnsafe(ms,sc), EmptySet); 
				assert tos != EmptySet; // this is an end state
				assert contains(tos,s); // cannot proceed to specified state
				state = Right(put(ms,sc,s)); 
			}
		} 
	}

}
