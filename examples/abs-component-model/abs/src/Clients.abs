module ReplicationSystem.Client;

export *;

import * from ABS.DC;
import * from ABS.Scheduler;
import * from System.Resources;

import * from ReplicationSystem.Environment.DataTypes;
import * from ReplicationSystem.Environment.Files;
import * from ReplicationSystem.Environment.StateMachine;

import * from ReplicationSystem.Client.Interfaces;
import * from ReplicationSystem.Interfaces;
import * from ReplicationSystem.Server.Interfaces;
import * from ReplicationSystem.Environment.Interfaces;

import * from ReplicationSystem.Environment.DataBases;
import * from ReplicationSystem.Client.ClientJob;
import * from Replication.Network;
import * from ReplicationSystem.Schedulers;

// Implementation of SyncClient 
// Java class com.fredhopper.application.SyncClient
[COG] [Scheduler: edf(queue)] 
class SyncClientImpl(
	[Final] Int maxJobs, 
	[Final] ClientId id,
	[Far] Recorder recorder) implements InternalClient, ClientConnector {
	
	port ClientBaseAutomata behavior;
	
	
	Network network;
	ServerAcceptor acceptor;
	ClientDataBase db;
	
	Bool shutDown = False;
	Set<ClientJob> jobRecords = EmptySet;
	
	//measurement
	List<ClientJob> jobHistories = Nil;
	List<JobData> jobDatas = Nil;
	List<Schedule> hit = Nil;
	List<Schedule> missed = Nil;
	
	Int time = 0;
	Int currentTransactionId = -1;
	
	{
		// initialize the client side data base
		db = new DataBaseImpl();
	}
	
	[Atomic]
	Int jobCount() {
		return length(jobHistories);
	}
	
	Unit checkProgress() {
		Int ct = timeval(now());
		assert this.time < ct;
		this.time = ct;
	}
	
	Unit scheduleJob(JobType jb, Schedule s) {
		// wait for its time to be initiated
		// and the next available slot
		// records the amount of extra waiting time 
		[Deadline: Duration(sched(s))] Int blockTime = this.waitFor(s);
		//this.checkProgress();
		Deadline remaining = subtractFromDuration(dline(s),blockTime);
		
		// only proceed if a shutdown 
		// request has not been made.
		if (durationValue(remaining) > 0 && ~shutDown) {
			// block subsequent onces in the same schedule
			behavior.setNext(s);
			[Deadline: remaining] this.makeJob(jb,blockTime,s);
			hit = Cons(s,hit);
		} else {
			//record those schedules that are missed
			Fut<Unit> u = recorder!recordUnexecuted(); u.get;
			missed = Cons(s,missed);
			this.scheduleJob(jb, s); //reschedule
		}
	}
	
	
	Int waitFor(Schedule schedule) {
		Int wait = sched(schedule);
	  	await duration(wait,wait);
	  	Time st = now();
	  	await (next || shutDown);
	  	Int diff = timeDifference(now(),st);
	  	return diff;
	}
	
	Unit makeJob(JobType jb, Int blockTime, Schedule schedule) {
		ClientJob job = new cog ClientJobImpl(maxJobs,this,jb,blockTime,schedule,length(jobHistories));
		[Deadline: deadline()] job!executeJob();
			
		jobHistories = Cons(job,jobHistories);
		jobRecords = Insert(job,jobRecords);
	}
	
	Unit finishJob(ClientJob job, Maybe<JobData> jobData, Bool missed, Schedule schedule) {
		if (missed) {
			Fut<Unit> fu = recorder!recordUnexecuted(); fu.get;
			this.missed = Cons(schedule, this.missed);
		} else if (isJust(jobData)) {
			jobDatas = Cons(fromJust(jobData),jobDatas);
			Fut<Unit> fu = recorder!record(fromJust(jobData)); fu.get;
		}
		jobRecords = remove(jobRecords,job);
	}
	
	Unit nextJob(Schedule s) {
		next = True;
	}
	
	ClientId getId() {
		return id;
	}
	
	Bool isShutdownRequested() {
		return shutDown;
	}
	
	Unit requestShutDown() {
		shutDown = True;
		await jobRecords == EmptySet;
		network!shutDown(this);
	}
		
	ServerAcceptor getAcceptor() {
		return acceptor;
	}
	
	Unit run() {
		// Makes a transition
		this.becomesState(WaitToBoot);
		
		// wait for acceptor to be ready
		await acceptor != null;
		
		// starts a boot job
		this.makeJob(Boot,0,NoSchedule);
	}
	
	ClientDataBase getClientDataBase() {
		return db;
	}
	
	DataBase getDataBase() {
		return db;
	}
	
	Unit becomesState(State state) {
		Set<State> tos = lookupDefault(machine,this.state,EmptySet);
		assert tos != EmptySet; // this is an end state
		assert contains(tos,state); // cannot proceed to specified state
		this.state = state;	
	}
	
	Unit setAcceptor([Far] ServerAcceptor acc) {
		acceptor = acc;
	}
	
	Unit setNetwork(Network network) {
		this.network = network;
	}
	
	Unit waitToBoot() { 
		this.becomesState(WaitToBoot);
	}
	
	Unit boot() { 
		this.becomesState(Booting);
	}
	
	Unit start() { 
		this.becomesState(Booting);
	}
	
	Unit waitToReplicate() { 
		this.becomesState(WaitToReplicate);
	}
	
	Unit replicate() { 
		this.becomesState(WorkOnReplicate);
	}
	
	Unit end() { 
		this.becomesState(End);
	}
	
		
}


class ClientControlerImpl(InternalClient client) {

  Unit switchToSequential() {
  
  }

  Unit switchToConcurrent() {
  
  }


}

class ClientSequentialBehaviorImpl() {

	StateMachine machine = stateMachine();
	State state = Start;
	Bool next = False;

	Unit setNext(Schedule schedule) {
		next = False;
	}

	Unit makeJob(JobType jb, Int blockTime, Schedule schedule) {
		ClientJob job = new cog ClientJobImpl(maxJobs,this,jb,blockTime,schedule,length(jobHistories));
		[Deadline: deadline()] job!executeJob();
			
		jobHistories = Cons(job,jobHistories);
		jobRecords = Insert(job,jobRecords);
	}

}

class ClientConcurrentBehaviorImpl() {

	StateMachine mmachine = concurrentStateMachine();
	Either<State,ManyState> state = Left(Start);
	Map<Schedule,Bool> nexts = EmptyMap;

	Unit setNext(Schedule schedule) {
		nexts = put(nexts,schedule,False);
	}


	Unit makeJob(JobType jb, Int blockTime, Schedule schedule) {
		ClientJob job = new cog ClientJobImpl(maxJobs,this,jb,blockTime,schedule,length(jobHistories));
		[Deadline: deadline()] job!executeJob();
			
		jobHistories = Cons(job,jobHistories);
		jobRecords = Insert(job,jobRecords);
	}


}

