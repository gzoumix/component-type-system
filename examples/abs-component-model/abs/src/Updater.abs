module ReplicationSystem.Server.Update;

export *;

import * from ReplicationSystem.Environment.DataTypes;
import * from ReplicationSystem.Server.Interfaces;
import * from ReplicationSystem.Environment.Interfaces;
import * from ReplicationSystem.Environment.Files;

// An updater that repeatedly changes the underlying data structure
class UpdaterImpl([Final] Int updates, ConcurrentSyncServer server) 
implements Updater { 
	
	Bool sd = False;
	
	List<Map<FileId,FileContent>> histories = Nil;
	
	[Final] Int best = 5;
	[Final] Int worst = 10;
	[Final] FileSize limit = 5;
	
	List<FileId> replicationItems = 
		list["indices/itemstore/i1",
			 "indices/itemstore/i2",
			 "indices/itemstore/log/j1",
			 "indices/search/s1",
			 "indices/search/s2",
			 "indices/tree/t1",
			 "indices/tree/log/j2",
			 "config/random.xml",
			 "config/business.xml"];
			 
	Unit run() {
		Fut<UpdatableDataBase> fd = server!getUpdatableDataBase();
		UpdatableDataBase db = fd.get;
		
		Map<FileId,FileContent> changes = EmptyMap;
		Int count = 0;
		while (~sd && (updates < 0 || count < updates)) {
			changes = this.makeChange();
			histories = Cons(changes,histories);
			if (changes != EmptyMap) {
				Fut<Unit> u = db!update(changes); u.get;
			}
			await duration(best,worst);
			count = count + 1;
		}
		this.shutDown();
	}
	
	Map<FileId,FileContent> makeChange() {
		List<FileId> fs = this.chooseFile();
		Map<FileId,FileContent> result = this.assignContent(fs,limit);	 
		return result;
	}
	
	//shutdown updater
	Unit shutDown() {
		sd = True;
	}
	
	Map<FileId,FileContent> assignContent(List<FileId> w, FileSize limit) {
		Map<FileId,FileContent> result = EmptyMap;
		while (w != Nil) {
			Int rand = random(limit);
			result = InsertAssoc(file(head(w),rand + 1),result);
			w = tail(w);
		}
		return result;
	}
	
	List<FileId> chooseFile() {
		List<FileId> files = replicationItems;
		List<FileId> result = Nil;
		while (files != Nil) {
			Int rand = random(2);
			if (rand == 0) {
				result = Cons(head(files),result);
			}
			files = tail(files);
		}
		return result;
	}
	
}