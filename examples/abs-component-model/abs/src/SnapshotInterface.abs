module ReplicationSystem.Environment.ReplicationSnapshot.Interfaces;

export *;

import * from ReplicationSystem.Environment.Files;
import * from ReplicationSystem.Environment.DataTypes;

interface ReplicationSnapshot {

	/*
	 * view snapshotspec {
     *    call_ReplicationSnapshot.refreshSnapshot r,
	 *    call_ReplicationSnapshot.clearSnapshot c,
	 *    call_ReplicationSnapshot.getItems i
	 * }
	 */
	 
	/*
	 * START ::= R		START.updated = S.updated
	 *   R   ::= r C 	S.updated = True;
	 *         | /\ 	S.updated = False;
	 *   C   ::= i C	S.updated = True;
     *         | c R 	S.updated = False;
	 */

	[Atomic] Unit refreshSnapshot();
	
	/*
	 * Cleaning replication snapshot 
	 */
	Unit clearSnapshot();
	Int getIndexingId();
	Set<ServerReplicationItem> getItems(String name);
	
	/*
	 * Only exists at model level
	 */
	//@ ensures \result == snapshotspec.updated();
	//Bool hasUpdated();
	
}

interface BasicReplicationItem {
	FileEntry getContents();
	[Atomic] Unit cleanup();
	FileId getAbsoluteDir();
}

/*
 * Represents an item to be replicated to 
 * the sync clients. Global for the SyncServer
 *
 * Items could be abstracted as data type 
 * but data types cannot be modified by deltas! 
 */
interface ServerReplicationItem extends BasicReplicationItem {
	Command getCommand();
	ReplicationItemType getType();
	[Atomic] Unit refresh();
}