module ReplicationSystem.Environment.DataBases;

export *;

import * from ReplicationSystem.Environment.DataTypes;
import * from ReplicationSystem.Environment.Files;
import * from ReplicationSystem.Environment.Interfaces;

def Map<A,B> firstValues<A,B>(Map<A,List<B>> mp, B default) =
	case mp {
		EmptyMap => EmptyMap;
		InsertAssoc(Pair(x,ls),xs) => 
			case ls {
				Nil => InsertAssoc(Pair(x,default),firstValues(xs,default));
				Cons(y,ys) => InsertAssoc(Pair(x,y),firstValues(xs,default));
			};
	};

/*
 * A simple model of a database mimicking 
 * a sequence of updates of files
 */
[Plain] 
class DataBaseImpl
implements ServerDataBase, ClientDataBase, UpdatableDataBase {
	
	Int count = 0; //for debug
	
	// client data base
	Map<String,List<TransactionId>> transactions = EmptyMap;
	
	// server data base
	// history of transactions
	TransactionHistories histories = Nil;
	
	// server data base
	// current transaction 
	Pair<TransactionId,Map<FileId,FileContent>> currentTransaction = Pair(-1,EmptyMap);
	
	// Begin with the root location (id = 0)
	Directory rdir = rootDir();

	[Atomic]
	TransactionHistories getTransactions() {
		return histories;
	}
	
	[Atomic] 
	Unit update(Map<FileId,FileContent> changes) {
		rdir = updateDirWithContents(rdir,changes);
		currentTransaction = Pair(fst(currentTransaction) + 1,changes);
		histories = Cons(currentTransaction,histories);
	}
	
	[Atomic]
	TransactionId refresh() {
		count = count + 1;
		return fst(currentTransaction);
	}
	
	// Returns 0 if file not found.
	[Atomic] 
	FileContent getContent(FileId qualified) {
		Maybe<FileContent> result = Nothing;  
		if (qualified == rootId()) {
			result = Just(getFileContent(Right(rdir)));
		} else {
			result = getFromEntryIn(rdir,qualified);
		}
		assert result != Nothing;
		return fromJust(result);
	}
	
	[Atomic] 
	Bool hasFile(FileId qualified) {
		return hasQualifiedEntriesIn(rdir,qualified);
	}
  
	// Updates file store
	// ClientDataBase
	[Atomic]
	Directory getRoot() {
		return rdir;
	}

	[Atomic] 
	Bool prepareReplicationItem(TransactionId p, Schedule schedule) {
		Bool result = False;
		String name = schedname(schedule);
		List<TransactionId> tids = lookupDefault(transactions,name,Nil);
		if (~ contains(set(tids),p)) {
			transactions = put(transactions,name,Cons(p,tids));
			result = True;
		}
		return result;
	}

	[Atomic]
	Map<String,TransactionId> lastTransactionIds() {
		return firstValues(transactions,-1);
	}

	[Atomic] 
	Unit updateFile(FileId qualified, FileSize size) {
		rdir = updateDirWithFile(rdir,file(qualified,size));
	}
	
	[Atomic] 
	Maybe<FileContent> listFilesAt(FileId qualifiedDir) {
		return getFromEntryIn(rdir,qualifiedDir);
	}
	
	[Atomic] 
	Set<FileId> listFiles() {
		Set<FileId> allqualified = getFileIdFromDir(rdir);
		return allqualified;
	}
}