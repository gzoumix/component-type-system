module ReplicationSystem.Tests;

export TesterImpl, Tester;

import * from ReplicationSystem.Environment.DataTypes;
import * from ReplicationSystem.Environment.Files;

import * from ReplicationSystem.Environment.Interfaces;
import * from ReplicationSystem.Interfaces;
import * from ReplicationSystem.Client.Interfaces;
import * from ReplicationSystem.Server.Interfaces;

/*
 * Checks that the file store in the data base of the actual node
 * is the same as the file store in the data base of the expected node.  
 */
class TesterImpl(ServerNode expected, Client actual) implements Tester {
	
	Schedules schedules = EmptySet;
	Map<String,TransactionId> scheduleResults = EmptyMap;
	Set<Triple<FileId,FileContent,FileContent>> result = EmptySet;
	
	Unit analyse() {
		Fut<UpdatableDataBase> fe = expected!getUpdatableDataBase();
		UpdatableDataBase e = fe.get;
		
		//get schedules from server nodes 
		Fut<Schedules> schf = expected!listSchedules();
		schedules = schf.get;
		
		//get transaction histories (changes) from server file system
		Fut<TransactionHistories> tf = e!getTransactions();
		TransactionHistories transactions = tf.get;
		
		Fut<ClientDataBase> fa = actual!getClientDataBase();
		ClientDataBase a = fa.get;
		
		//get the reference to the last change from client file system
		Fut<Map<String,TransactionId>> idf = a!lastTransactionIds();
		scheduleResults = idf.get;
		
		//get the underlying file system from the client
		Fut<Directory> rf = a!getRoot();
		Directory act = rf.get;
		
		//construct a directory from the changes upto the last change
		//the client has received. 
		this.checkDatas(scheduleResults,reverse(transactions),act);
	}
	
	Unit checkDatas(Map<String,TransactionId> tids, TransactionHistories th, Directory act) {
		//check against schedule!
		while (hasNext(schedules)) {
			Pair<Schedules,Schedule> nt = next(schedules);
			schedules = fst(nt); Schedule s = snd(nt);
			Int tid = lookupDefault(tids,schedname(s),-1);
			if (tid != -1) {
				//generate test oracles with changes
				Directory exp = applyChanges(rootDir(),th,tid);
				List<Item> is = items(s);
				while (is != Nil) {
					this.checkData(head(is),exp,act);
					is = tail(is);
				}
			}
		}
	}
	
	Bool hasFile(DataBase b, FileId f) {
		Fut<Bool> fb = b!hasFile(f); 
		return fb.get;
	}
	
	Unit checkData(Item i, Directory exp, Directory act) {
		if (isLeft(item(i))) {
			FileId id = left(item(i));
			
			Bool eh = hasQualifiedEntriesIn(exp,id);
			Bool ah = hasQualifiedEntriesIn(act,id);
			assert eh == ah;
			
			if (eh) {
				FileContent ce = fromJust(getFromEntryIn(exp,id));
				FileContent ca = fromJust(getFromEntryIn(act,id)); 
				if (isFile(ce)) {
					this.compareFile(file(id,content(ce)),file(id,content(ca)));
				} else {
					this.compareDir(dir(id,entries(ce)),dir(id,entries(ca)));
				}
			}
		} 
		
		//for testing
		if (isRight(item(i))) {
			FileId id = fst(right(item(i)));
			String pattern = snd(right(item(i)));
				
			Bool eh = hasQualifiedEntriesIn(exp,id);
			Bool ah = hasQualifiedEntriesIn(act,id);
			
			if (eh != ah && eh) {
				// check if exp only contains non matching files
				FileContent ce = fromJust(getFromEntryIn(exp,id));
				assert ~isFile(ce);
				assert emptySet(filters(pattern,getFileIdFromDir(dir(id,entries(ce)))));
			} else if (eh) {
				FileContent ce = fromJust(getFromEntryIn(exp,id));
				FileContent ca = fromJust(getFromEntryIn(act,id)); 
				if (isFile(ce)) {
					if (filter(pattern,id)) {
						this.compareFile(file(id,content(ce)),file(id,content(ca)));				
					}
				} else {
					this.compareDirWithPattern(pattern,dir(id,entries(ce)),dir(id,entries(ca)));
				}			
			}
		}
	}
	
	Unit compareDirWithPattern(String pattern, Directory e, Directory a) {
		this.compareEntrySets(
			filters(pattern,getFileIdFromDir(e)),
			filters(pattern,getFileIdFromDir(a)),
			qualifyFileEntry(entries(snd(e)),fst(e)),
			qualifyFileEntry(entries(snd(a)),fst(a)));
	}
	
	Unit compareDir(Directory e, Directory a) {
		this.compareEntrySets(
			getFileIdFromDir(e),
			getFileIdFromDir(a),
			qualifyFileEntry(entries(snd(e)),fst(e)),
			qualifyFileEntry(entries(snd(a)),fst(a)));
	}
	
	Unit compareEntrySets(
		Set<FileId> eids,
		Set<FileId> aids,
		Map<FileId,FileContent> ee, 
		Map<FileId,FileContent> ae) {
		
		assert size(eids) == size(aids);
		while (hasNext(eids)) {
			Pair<Set<FileId>,FileId> nd = next(eids);
			FileId id = snd(nd); eids = fst(nd);
			FileContent es = lookupDefault(ee,id,NoContent);
			FileContent as = lookupDefault(ae,id,NoContent);
			result = Insert(Triple(id,es,as),result);
			assert es == as;
		}
	}
	
	Unit compareFile(File e, File a) {
		FileId id = getFileId(Left(e));
		FileContent es = getFileContent(Left(e));
		FileContent as = getFileContent(Left(a));
		result = Insert(Triple(id,es,as),result);			
		assert es == as;
	}
	
}