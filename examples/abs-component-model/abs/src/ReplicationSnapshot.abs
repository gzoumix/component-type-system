module ReplicationSystem.Server.ReplicationSnapshot;

export *;

import * from ReplicationSystem.Environment.Files;
import * from ReplicationSystem.Environment.DataTypes;
import * from ReplicationSystem.Environment.Interfaces;
import * from ReplicationSystem.Environment.ReplicationSnapshot.Interfaces;

import SearchDirectoryItem from ReplicationSystem.Server.ReplicationItem;

//For testing
import * from ReplicationSystem.Server.BaseReplicationItem;

class ReplicationSnapshotImpl(
	ServerDataBase db, 
	Schedules schedules) implements ReplicationSnapshot {

	Int count = 0;
	Int update = 0;
	
	//the transaction id after refreshing snapshot;
	TransactionId tid = -1;
	
	// if snapshot is cleaned
	Bool clean = True;
	
	Map<String,Set<ServerReplicationItem>> repItems = EmptyMap;
	
	Set<ServerReplicationItem> getItems(String name) {
		return lookupDefault(repItems,name,EmptySet);
	}
	
	/*
	 * Updating replication snapshot
	 */
	[Atomic] Unit refreshSnapshot() {
		count = count + 1; //for debug
		if (clean) {
			tid = db.refresh();
			update = update + 1; //for debug
				
			this.createReplicationItems();
				
			Set<String> names = keys(repItems);
			while (hasNext(names)) {
				Pair<Set<String>,String> nn = next(names);
				Set<ServerReplicationItem> titems = lookup(repItems,snd(nn));
				while (hasNext(titems)) {
					Pair<Set<ServerReplicationItem>,ServerReplicationItem> ni = next(titems);
					ServerReplicationItem item = snd(ni);
					item.refresh();
					titems = fst(ni);
				}
				names = fst(nn);
			}
			clean = False;
		}
		
	}
	
	[Atomic] Unit createReplicationItems() {
		Schedules tsc = schedules;
		while (hasNext(tsc)) {
			Pair<Schedules,Schedule> ns = next(tsc);
			this.replicationItems(snd(ns));
			tsc = fst(ns);
		}
	}
	
	[Atomic] Unit replicationItems(Schedule schedule) {
		List<Item> is = items(schedule);
		Set<ServerReplicationItem> sitems = EmptySet;
		while (is != Nil) {
			ServerReplicationItem r = this.replicationItem(head(is));
			sitems = Insert(r,sitems);
			is = tail(is); 
		}
		repItems = InsertAssoc(Pair(schedname(schedule),sitems),repItems);
	}
	
	[Atomic] ServerReplicationItem replicationItem(Item i) {
		ServerReplicationItem item = null;
		if (isSearchItem(i)) {
			item = new SearchDirectoryItem(left(item(i)),this.db);
		}
		
		//for testing
		if (item == null && isFileItem(i)) {
			Pair<FileId,String> it = right(item(i));
			item = new ReplicationFilePattern(fst(it),snd(it),this.db);				
		}
		
		//for testing
		if (item == null && isLogItem(i)) { 
			item = new ReplicationLogItem(left(item(i)),this.db);
		}
		
		return item; 
	}
	
	//Clear snapshot
	Unit clearSnapshot() {
		repItems = EmptyMap;
		clean = True;
	}
	
	Int getIndexingId() {
		return tid;
	}

}