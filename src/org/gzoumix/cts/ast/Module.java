package org.gzoumix.cts.ast;


import org.gzoumix.cts.ast.declaration.IDeclaration;
import org.gzoumix.cts.ast.statement.StatementBlock;
import org.gzoumix.cts.typing.Environment;
import org.gzoumix.cts.utils.Utils;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

public class Module extends ASTNode {

  private List<Environment.SearchPath<String>> _imports;
  private List<Environment.SearchPath<String>> _exports;
  private List<IDeclaration> _declarations;
  private StatementBlock _main;

  public Module(List<String> name) {
    _name = name;
    _imports = new LinkedList<>();
    _exports = new LinkedList<>();
    _declarations = new LinkedList<>();
    _main = null;

    _subDeclatation = new List[2];
    _subDeclatation[0] = _declarations;
    _subDeclatation[1] = new LinkedList(); // for the main
  }

  public Module(List<String> name, Collection<? extends Environment.SearchPath<String>> imports, Collection<? extends Environment.SearchPath<String>> exports,
                Collection<? extends IDeclaration> declarations, StatementBlock main) {
    _name = new LinkedList<>(name);
    _imports = new LinkedList<>(imports);
    _exports = new LinkedList<>(exports);
    _declarations = new LinkedList<>(declarations);
    _main = main;

    _subDeclatation = new List[2];
    _subDeclatation[0] = _declarations;
    _subDeclatation[1] = new LinkedList();
    if(main != null) { _subDeclatation[1].add(main); }
  }


  public void addImport(Environment.SearchPath<String> im) { _imports.add(im); }
  public void addExport(Environment.SearchPath<String> ex) { _exports.add(ex); }
  public void addDeclaration(IDeclaration decl) { _declarations.add(decl); }


  public void setMain(StatementBlock main) { if(_main == null) {_main = main; _subDeclatation[1].add(main); } }
  public boolean hasMain() { return _main != null; }
  public StatementBlock getMain() { return _main; }
  public List<IDeclaration> getDeclarations() {  return _declarations; }
}
