package org.gzoumix.cts.ast;


import org.gzoumix.cts.ast.statement.StatementBlock;

import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

public class CompilationUnit extends ASTNode {

  private static final List<String> _NAME_STATIC;
  static{
    _NAME_STATIC = new LinkedList<>();
    _NAME_STATIC.add("CompilationUnit");
  }

  private String _file_name;
  private List<Module> _modules;
  private StatementBlock _main;

  public CompilationUnit(String file_name) {
    _name = new LinkedList<>(_NAME_STATIC);
    _name.add(file_name);

    _file_name = file_name;
    _modules = new LinkedList<>();
    _main = null;

    _subDeclatation = new List[1];
    _subDeclatation[0] = _modules;
  }

  public CompilationUnit(String file_name, Collection<? extends Module> modules) {
    _name = new LinkedList<>(_NAME_STATIC);
    _name.add(file_name);

    _file_name = file_name;
    _modules = new LinkedList<>(modules);
    _main = null;
    for(Module module: modules) {
      if(module.hasMain()) { _main = module.getMain(); return; }
    }

    _subDeclatation = new List[1];
    _subDeclatation[0] = _modules;
  }

  public void addModule(Module m) {
    _modules.add(m);
    if((m.hasMain()) && (_main == null)) { _main = m.getMain(); }
  }


  //public void setMain(StatementBlock m) { _main = m; }
  public boolean hasMain() { return _main != null; }
  public StatementBlock getMain() { return _main; }

  public List<Module> getModules() { return _modules; }
}

