package org.gzoumix.cts.ast.expression;

import org.gzoumix.cts.ast.ASTNode;

import java.util.LinkedList;
import java.util.List;

public class ExpressionGet extends ASTNode implements IExpression {

  private static final List<String> _NAME_STATIC;
  static{
    _NAME_STATIC = new LinkedList<>();
    _NAME_STATIC.add("ExpressionGet");
  }

  private List<IExpression> _exp;

  public ExpressionGet(IExpression exp) {
    _name = _NAME_STATIC;

    _exp = new LinkedList<>(); _exp.add(exp);

    _subDeclatation = new List[1];
    _subDeclatation[0] = _exp;
  }

  public IExpression getSubExpression() { return _exp.get(0); }
}
