package org.gzoumix.cts.ast.expression;

import org.gzoumix.cts.ast.ASTNode;
import org.gzoumix.cts.ast.declaration.IDeclarationFunction;

import java.util.LinkedList;
import java.util.List;

public class ExpressionFunctionApplication extends ASTNode implements IExpressionPure {

  private static final List<String> _NAME_STATIC;
  static{
    _NAME_STATIC = new LinkedList<>();
    _NAME_STATIC.add("ExpressionFunctionApplication");
  }


  private List<IDeclarationFunction> _func;
  private List<IExpressionPure> _params;

  public ExpressionFunctionApplication(IDeclarationFunction func, List<IExpressionPure> params) {
    _name = _NAME_STATIC;

    _func = new LinkedList<>(); _func.add(func);
    _params = new LinkedList<>(params);

    _subDeclatation = new List[2];
    _subDeclatation[0] = _func;
    _subDeclatation[1] = _params;
  }

  public IDeclarationFunction getFunction() { return _func.get(0); }
  public List<IExpressionPure> getParams() { return _params; }

}
