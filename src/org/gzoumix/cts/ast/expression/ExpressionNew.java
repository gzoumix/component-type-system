package org.gzoumix.cts.ast.expression;

import org.gzoumix.cts.ast.ASTNode;
import org.gzoumix.cts.ast.declaration.DeclarationClass;
import org.gzoumix.cts.ast.declaration.IDeclarationClass;

import java.util.LinkedList;
import java.util.List;

public class ExpressionNew extends ASTNode implements IExpression {

  private static final List<String> _NAME_STATIC;
  static{
    _NAME_STATIC = new LinkedList<>();
    _NAME_STATIC.add("ExpressionNew");
  }

  private boolean _is_local;
  private List<IDeclarationClass> _class;
  private List<IExpressionPure> _params;

  public ExpressionNew(boolean is_local, IDeclarationClass klass, List<IExpressionPure> params) {
    _name = _NAME_STATIC;

    _is_local = is_local;
    _class = new LinkedList<>(); _class.add(klass);
    _params = new LinkedList<>(params);

    _subDeclatation = new List[2];
    _subDeclatation[0] = _class;
    _subDeclatation[1] = _params;
  }

  public IDeclarationClass getDeclarationClass() { return _class.get(0); }
  public List<IExpressionPure> getParameters() { return _params; }
}
