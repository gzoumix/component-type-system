package org.gzoumix.cts.ast.expression;


import org.gzoumix.cts.ast.ASTNode;

import java.util.LinkedList;
import java.util.List;

public class ExpressionMethodCall extends ASTNode implements IExpression {

  private static final List<String> _NAME_STATIC;
  static{
    _NAME_STATIC = new LinkedList<>();
    _NAME_STATIC.add("ExpressionMethodCall");
  }


  private boolean _is_async;
  private List<IExpressionPure> _obj;
  private String _name_method;
  private List<IExpressionPure> _params;

  public ExpressionMethodCall(boolean is_async, IExpressionPure obj, String name_method, List<IExpressionPure> params) {
    _name = _NAME_STATIC;

    _is_async = is_async;
    _obj = new LinkedList<>(); _obj.add(obj);
    _name_method = name_method;
    _params = new LinkedList<>(params);

    _subDeclatation = new List[2];
    _subDeclatation[0] = _obj;
    _subDeclatation[1] = _params;
  }

  public IExpressionPure getCallee() { return _obj.get(0); }
  public String getMethodName() { return _name_method; }
  public List<IExpressionPure> getParameters() { return _params; }
  public boolean isAsync() { return _is_async; }
}
