package org.gzoumix.cts.ast.expression;

import org.gzoumix.cts.ast.ASTNode;
import org.gzoumix.cts.ast.declaration.DeclarationVariable;

import java.util.LinkedList;
import java.util.List;

public class ExpressionReference extends ASTNode implements IExpressionPure {

  private static final List<String> _NAME_STATIC;
  static{
    _NAME_STATIC = new LinkedList<>();
    _NAME_STATIC.add("ExpressionReference");
  }
  
  private List<DeclarationVariable> _decl;

  public ExpressionReference(DeclarationVariable decl) {
    _name = _NAME_STATIC;

    _decl = new LinkedList<>(); _decl.add(decl);

    _subDeclatation = new List[1];
    _subDeclatation[0] = _decl;
  }

  public DeclarationVariable getDecl() { return _decl.get(0); }
}
