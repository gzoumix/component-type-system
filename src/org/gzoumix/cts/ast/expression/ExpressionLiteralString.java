package org.gzoumix.cts.ast.expression;

import java.util.LinkedList;
import java.util.List;

public class ExpressionLiteralString implements IExpressionLiteral {

  private String _value;

  public ExpressionLiteralString(String value) { _value = value; }
}
