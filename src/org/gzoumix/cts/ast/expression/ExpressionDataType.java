package org.gzoumix.cts.ast.expression;

import org.gzoumix.cts.ast.ASTNode;
import org.gzoumix.cts.ast.declaration.IDeclarationDataTypeConstructor;
import org.gzoumix.cts.ast.declaration.IDeclarationFunction;

import java.util.LinkedList;
import java.util.List;

public class ExpressionDataType extends ASTNode implements IExpressionPure {

  private static final List<String> _NAME_STATIC;
  static{
    _NAME_STATIC = new LinkedList<>();
    _NAME_STATIC.add("ExpressionDataType");
  }

  private List<IDeclarationDataTypeConstructor> _func;
  private List<IExpressionPure> _params;

  public ExpressionDataType(IDeclarationDataTypeConstructor dataTypeConstructor, List<IExpressionPure> params) {
    _name = _NAME_STATIC;

    _func = new LinkedList<>(); _func.add(dataTypeConstructor);
    _params = new LinkedList<>(params);

    _subDeclatation = new List[2];
    _subDeclatation[0] = _func;
    _subDeclatation[1] = _params;
  }

  public IDeclarationDataTypeConstructor getConstructor() { return _func.get(0); }
  public List<IExpressionPure> getParameters() { return _params; }
}
