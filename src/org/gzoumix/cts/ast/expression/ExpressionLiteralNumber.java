package org.gzoumix.cts.ast.expression;

import java.util.LinkedList;
import java.util.List;

public class ExpressionLiteralNumber implements IExpressionLiteral {

  private int _value;

  public ExpressionLiteralNumber(int value) { _value = value; }
}
