package org.gzoumix.cts.ast.type;

import java.util.Map;
import java.util.Set;

public interface ITypeUse {

  public Set<ITypeUse> superTypeUse(); // set of all the super types of this one
  public ITypeUse replace(TypeUseVariable var, ITypeUse type);
  public ITypeUse replace(Map<TypeUseVariable, ITypeUse> map);
  public ITypeUse canonicalForm();

  //public boolean isSubtypeOf(ITypeUse param);

  public boolean match(ITypeUse type);
  public boolean matchRecord(ITypeUse type);
  public boolean isSubRecordOf(ITypeUse type);

  public boolean equals(Object o);

  public Map<TypeUseVariable,ITypeUse> matchWithUnification(Map<TypeUseVariable, ITypeUse> env, ITypeUse type);
}
