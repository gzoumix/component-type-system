package org.gzoumix.cts.ast.type;

import org.gzoumix.cts.parser.CTSParser;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class RecordStructure implements IRecord {

  private GroupName _group;
  private Map<String, ITypeUse> _params;

  public RecordStructure(GroupName group, Map<String, ITypeUse> params) {
    _group = group;
    _params = params;
  }

  public GroupName getGroupName() { return _group; }
  public Map<String, ITypeUse> getFields() { return _params; }

  @Override
  public boolean equals(Object o) {
    if(o instanceof RecordStructure) {
      if(_group != ((RecordStructure) o)._group) { return false; } // not the same group
      if(((RecordStructure) o)._params.keySet().containsAll(_params.keySet())) {
        for(Map.Entry<String, ITypeUse> entry: _params.entrySet()) {
          ITypeUse oSubType = ((RecordStructure) o)._params.get(entry.getKey());
          if(oSubType == null) { return false; } // they don't have the same number of fields
          else if(!entry.getValue().matchRecord(oSubType)) { return false; }
        }
        return true;
      } else { return false; } // they don't have the same number of fields
    } else { return false; }
  }

  /*@Override
  public boolean isSubtypeOf(IRecord record, Set<String> ports) {
    if(record instanceof RecordStructure) {
      Set<String> fields = ((RecordStructure) record)._params.keySet();
      fields.retainAll(this._params.keySet());
      ports.retainAll(this._params.keySet());
      fields.removeAll(ports);
      for(String field: fields) {
        if(!_params.get(field).isSubtypeOf(((RecordStructure) record)._params.get(field))) { return false; }
      }
      for(String field: ports) {
        if(!((RecordStructure) record)._params.get(field).isSubtypeOf(this._params.get(field))) { return false; }
      }
      return true;
    } else { return false; }
  }*/

  @Override
  public String toString() {
    String res = "[ " + getGroupName().toString() + " | ";
    Iterator<Map.Entry<String, ITypeUse>> iField = getFields().entrySet().iterator();
    while(iField.hasNext()) {
      Map.Entry<String, ITypeUse> entry = iField.next();
      res = res + entry.getKey() + " = " + entry.getValue().toString();
      if(iField.hasNext()) { res = res + "; "; }
    }
    res = res + "]";
    return res;
  }
}
