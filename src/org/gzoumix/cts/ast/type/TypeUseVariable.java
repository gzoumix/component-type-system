package org.gzoumix.cts.ast.type;

import org.gzoumix.cts.ast.ASTNode;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class TypeUseVariable implements ITypeUse {
  private String _name; // the name of the variable in the program

  public TypeUseVariable(String name) { _name = name; }
  public String getName() { return _name; }

  @Override
  public Set<ITypeUse> superTypeUse() {
    Set<ITypeUse> res =  new HashSet<>();
    res.add(this);
    return res;
  }

  @Override
  public ITypeUse replace(TypeUseVariable var, ITypeUse type) {
    if(this.equals(var)) { return type; }
    else { return this; }
  }

  public ITypeUse replace(Map<TypeUseVariable, ITypeUse> map) {
    if(map.containsKey(this)) { return map.get(this); }
    else { return this; }
  }

  @Override
  public ITypeUse canonicalForm() { return this; }

  @Override
  public boolean match        (ITypeUse type) { return this.equals(type); }
  @Override
  public boolean matchRecord  (ITypeUse type) { return this.equals(type); }
  @Override
  public boolean isSubRecordOf(ITypeUse type) { return this.equals(type); }

  @Override
  public Map<TypeUseVariable, ITypeUse> matchWithUnification(Map<TypeUseVariable, ITypeUse> env, ITypeUse type) {
      ITypeUse tmp = env.get(this);
      if(tmp == null) {
        env.put(this, type);
        return env;
      } else {
        if(type.equals(tmp)) { return env; } // we don't do unification inside the right term here.
        else { return null; }
    }
  }




  /* @Override
  public boolean isSubtypeOf(ITypeUse param) {
    return this.equals(param); // a variable is only subtype of itself
  }*/

  public String toString() { return _name; }
}
