package org.gzoumix.cts.ast.type;

public class GroupName {

  private String _name;

  public GroupName(String name) {
    _name = name;
  }

  @Override
  public String toString() { return _name; }

}
