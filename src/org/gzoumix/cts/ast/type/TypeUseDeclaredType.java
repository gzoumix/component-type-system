package org.gzoumix.cts.ast.type;

import org.gzoumix.cts.ast.ASTNode;
import org.gzoumix.cts.ast.declaration.DeclarationDataTypeAlias;
import org.gzoumix.cts.ast.declaration.IDeclaration;
import org.gzoumix.cts.ast.declaration.IDeclarationType;
import org.gzoumix.cts.ast.declaration.IDeclarationWithInstance;
import org.gzoumix.cts.utils.Logger;
import org.gzoumix.cts.utils.Utils;

import java.util.*;

// corresponds to useDataType and useInterface (there might be no way to kwno which one it is when parsing)
public class TypeUseDeclaredType extends ASTNode implements IDeclaration, ITypeUse {

  private static final List<String> _NAME_STATIC;
  static{
    _NAME_STATIC = new LinkedList<>();
    _NAME_STATIC.add("TypeUseDeclaredType");
  }

  private List<IDeclarationType> _type;
  private List<ITypeUse> _parameters;

  private List<IRecord> _record;

  public TypeUseDeclaredType(IDeclarationType type, List<? extends ITypeUse> parameters) {
    _name = _NAME_STATIC;
    _type = new LinkedList<>();
    _type.add(type);
    _parameters = new LinkedList<>(parameters);

    _record = new LinkedList<>();
    _record.add(RecordUnspecified.bot); // by default, the record is BOT (evern for datatypes, it doesn't hurt)

    _subDeclatation = new List[3];
    _subDeclatation[0] = _type;
    _subDeclatation[1] = _parameters;
    _subDeclatation[2] = _record;
  }

  public void setRecord(IRecord record) { _record.clear(); _record.add(record); }
  public IDeclarationType getDeclarationType() { return _type.get(0); }
  public IRecord getRecord() { return _record.get(0); }
  public List<ITypeUse> getParameters() { return _parameters; }


  @Override
  public Set<ITypeUse> superTypeUse() { // No record manipulation: this will be handled in another subtyping method:
  // (otherwise, the size of the set will be far too big andwll actually break things because of potentially missing informations)
    Set<ITypeUse> res = new HashSet<>();
    res.add(this);

    if(getDeclarationType() instanceof IDeclarationWithInstance) { // we have subtyping
      //Logger.LOGGER.logDebug("Looking for super types of an instance of \"" + getDeclarationType().getName() + "\"");
      List<ITypeUse> supertypes = ((IDeclarationWithInstance) getDeclarationType()).getExtendsTypes();
      // replace the type variables with the actual type parameters
      List<TypeUseVariable> params = getDeclarationType().getTypeParameters();
      for(ITypeUse tmp: supertypes) {
        ITypeUse supertype = tmp;
        Iterator<TypeUseVariable> iVar = params.iterator();
        Iterator<ITypeUse> iType = _parameters.iterator();
        while(iVar.hasNext() && iType.hasNext()) {
          supertype = supertype.replace(iVar.next(), iType.next());
        }
        res.addAll(supertype.superTypeUse());
        // no check that the number of parameters match: this is already done.
      }
    }
    return res;
  }

  @Override
  public ITypeUse replace(TypeUseVariable var, ITypeUse type) {
    List<ITypeUse> new_params = new LinkedList<>();
    for(ITypeUse param : _parameters) { new_params.add(param.replace(var, type)); }
    TypeUseDeclaredType res = new TypeUseDeclaredType(getDeclarationType(), new_params);
    res.setRecord(getRecord());
    return res;
  }

  @Override
  public ITypeUse replace(Map<TypeUseVariable, ITypeUse> map) {
    ITypeUse res = this;
    for (Map.Entry<TypeUseVariable, ITypeUse> entry : map.entrySet()) { res = res.replace(entry.getKey(), entry.getValue()); }
    return res;
  }

  @Override
  public ITypeUse canonicalForm() {
    // 1. put the parameters in canonical form
    List<ITypeUse> res_params = new LinkedList<>();
    for(ITypeUse param: getParameters()) { res_params.add(param.canonicalForm()); }

    ITypeUse res;
    IDeclarationType decl = getDeclarationType();
    if(decl instanceof DeclarationDataTypeAlias) {
      res = ((DeclarationDataTypeAlias) decl).getDefinition();
      Map<TypeUseVariable, ITypeUse> map = Utils.createMap(decl.getTypeParameters(), res_params);
      res = res.replace(map);
    } else { res =  new TypeUseDeclaredType(decl, res_params); }
    if(res instanceof TypeUseDeclaredType) { ((TypeUseDeclaredType) res).setRecord(getRecord()); }
    else { Logger.LOGGER.logError("Canonical form of " + this.toString() + "is not a structured type: type = " + res.getClass().getName() + ", toString = " + res.toString()); }
    return res;
  }

  @Override
  public boolean match(ITypeUse type) {
    if(type instanceof TypeUseDeclaredType) {
      if(getDeclarationType().equals(((TypeUseDeclaredType) type).getDeclarationType())) {
        Iterator<ITypeUse> iParamSelf = getParameters().iterator();
        Iterator<ITypeUse> iParamType = ((TypeUseDeclaredType) type).getParameters().iterator();
        while(iParamSelf.hasNext() && iParamType.hasNext()) {
          if(!iParamSelf.next().match(iParamType.next())) { return false; }
        }
        return true;
      } else {return false;}
    }
    else { return false; }
  }

  @Override
  public boolean matchRecord(ITypeUse type) {
    if(type instanceof TypeUseDeclaredType) {
      return _record.equals(((TypeUseDeclaredType) type).getRecord());
    } else { return false; }
  }

  @Override
  public boolean isSubRecordOf(ITypeUse type) {
    if(type instanceof TypeUseDeclaredType) {
      IRecord recordSelf = getRecord();
      IRecord recordType = ((TypeUseDeclaredType) type).getRecord();
      if(recordSelf.equals(RecordUnspecified.bot)) { return true; }
      else if((recordSelf instanceof RecordStructure) && (recordType instanceof RecordStructure)) {
        if(((RecordStructure) recordType).getFields().keySet().containsAll(((RecordStructure) recordSelf).getFields().keySet())) {
          for(Map.Entry<String, ITypeUse> field : ((RecordStructure) recordSelf).getFields().entrySet()) {
            if(!field.getValue().equals(((RecordStructure) recordType).getFields().get(field.getKey()))) { return false; }
          }
          return true;
        } else { return false; } // the target record has fields not specified in the actual record
      } else { return false; } // for instance, [g, ... ] = \bot <- not sound
    } else { return false; } // cannot check the consistency of record with another type structure (for instance, type variables)
  }


  @Override
  public boolean equals(Object o) {
    if(o instanceof ITypeUse) {
      if(this.match((ITypeUse)o)) { return this.matchRecord((ITypeUse)o); }
      else { return false; }
    } else { return false; }
  }

  @Override
  public Map<TypeUseVariable, ITypeUse> matchWithUnification(Map<TypeUseVariable, ITypeUse> env, ITypeUse type) {
    if(type instanceof TypeUseDeclaredType) {
      if(getDeclarationType().equals(((TypeUseDeclaredType) type).getDeclarationType())) {
        Iterator<ITypeUse> iParamSelf = getParameters().iterator();
        Iterator<ITypeUse> iParamType = ((TypeUseDeclaredType) type).getParameters().iterator();
        while(iParamSelf.hasNext() && iParamType.hasNext()) {
          env = iParamSelf.next().matchWithUnification(env, iParamType.next());
          if(env == null) { return null; }
        }
        if(this.matchRecord(type)) { return env; }
        else {return null; }
      } else {return null;}
    }
    else { return null; }
  }


/*
  @Override
  @Deprecated
  public boolean isSubtypeOf(ITypeUse param) {
    if(param instanceof TypeUseDeclaredType) {
      IDeclarationType param_type = ((TypeUseDeclaredType) param).getDeclarationType();
      if(param_type instanceof IDeclarationWithInstance && _type instanceof IDeclarationWithInstance) {
        if(!((IDeclarationWithInstance) _type).isSubtypeOf((IDeclarationWithInstance) param_type)) { return false; }
        if(_record.equals(RecordUnspecified.bot)) {return true; }
        IRecord param_record = ((TypeUseDeclaredType) param).getRecord();
        return _record.isSubtypeOf(param_record, ((IDeclarationWithInstance) _type).getPorts());
      } else { return param_type.equals(_type); }
    } else { return false; }
  }
*/



  public String toString() {
    List<String> params = new LinkedList<>();
    for(ITypeUse param: _parameters) { params.add(param.toString()); }
    String res;
    IDeclarationType decl = getDeclarationType();
    if(decl != null) { res = decl.getName(); }
    else { res = "null"; }
    if(!params.isEmpty()) {
      res = res + " < ";
      res =  res + Utils.concat(params, ", ");
      res = res + " >";
    }

    if(!getRecord().equals(RecordUnspecified.bot)) { res = res + " : " + getRecord().toString(); }
    return res;
  } // temporary for debugging
}
