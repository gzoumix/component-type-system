package org.gzoumix.cts.ast;


import org.gzoumix.cts.ast.declaration.IDeclaration;
import org.gzoumix.cts.ast.declaration.dependency.*;
import org.gzoumix.cts.typing.Environment;
import org.gzoumix.cts.utils.Logger;
import org.gzoumix.cts.utils.Utils;

import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

// simple implementation of dependency resolution
public class ASTNode implements org.gzoumix.typing.environment.IDeclaration<String, ASTNode>, IDeclaration {
  protected List<String> _name;

  protected Map<Environment.PathElement<String>, IUnsolvedDependency> _dependencies;
  protected List[] _subDeclatation;
  private boolean _resolved;

  protected ASTNode() { _dependencies = new HashMap<>(); _resolved = false; }

  public <T extends IUnsolvedDependency> T addDependency(Environment.PathElement<String> path, T dep) {
    T res = (T)_dependencies.get(path);
    if(res == null) {
      res = dep;
      _dependencies.put(path, res);
    }
    return res;
  }

  @Override
  public void resolve(Environment.PathElement<String> path, ASTNode decl) {
    if(!_resolved) {
      _resolved = true; // this is used to break loops: in this structure, with solving, we can very quickly get loops in the object structure.
      // 1. look for definition locally
      boolean found = false;
      IUnsolvedDependency dep = _dependencies.get(path);
      if (dep != null) {
        //Logger.LOGGER.logDebug("\"" + getName() + "\" Solving dependency to \"" + path + "\" -> definition found!");
        _resolved = false;
        this.resolve(dep, decl);
        _resolved = true;
      } else {
        //Logger.LOGGER.logDebug("\"" + getName() + "\" Solving dependency to \"" + path + "\" -> definition not found");
        for (List list : _subDeclatation) {
          //if (list == null) { System.err.println("Bad ASTNode initialization: \"" + this.getClass().getName() + "\""); }
          for (Object o : list) {
            if (o instanceof ASTNode) {
              if (!((ASTNode) o)._resolved) {
                ((ASTNode) o).resolve(path, decl);
              }
            }
          }
        }
      }
      _resolved = false;
    }
  }

  private void resolve(IUnsolvedDependency dep, ASTNode decl) {
    if(!_resolved) {
      _resolved = true; // this is used to break loops: in this structure, with solving, we can very quickly get loops in the object structure.
      //Logger.LOGGER.beginIndent();
      for (List list : _subDeclatation) {
        ListIterator<? super IDeclaration> i = list.listIterator();
        while (i.hasNext()) {
          Object tmp = i.next();
          if (tmp == dep) {
            //Logger.LOGGER.logDebug("dependency replaced in \"" + getName() + "\"");
            i.set(decl);
          } else if (tmp instanceof ASTNode) {
            //Logger.LOGGER.logDebug("Looking inside \"" + ((ASTNode) tmp).getName() + "\"");
                    ((ASTNode) tmp).resolve(dep, decl);
          }
        }
      }
      //Logger.LOGGER.endIndent();
      _resolved = false;
    }
  }

  public List<String> getNameAsList() { return _name; }

  @Override
  public String getName() { return Utils.concatModuleName(_name); }


}
