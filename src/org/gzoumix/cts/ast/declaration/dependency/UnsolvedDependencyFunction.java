package org.gzoumix.cts.ast.declaration.dependency;

import org.gzoumix.cts.ast.declaration.DeclarationVariable;
import org.gzoumix.cts.ast.declaration.IDeclarationFunction;
import org.gzoumix.cts.ast.type.ITypeUse;
import org.gzoumix.cts.typing.Environment;

import java.util.List;

public class UnsolvedDependencyFunction implements IUnsolvedDependency, IDeclarationFunction {

  private Environment.PathElement<String> _dep;

  public UnsolvedDependencyFunction(Environment.PathElement<String> dep) { _dep = dep; }
  public Environment.PathElement<String> getDependency() { return _dep; }

  @Override
  public List<DeclarationVariable> getParameters() {
    System.err.println("Error: accessing an unresolved dependency (Function) \"" + _dep +"\"");
    return null;
  }

  @Override
  public ITypeUse getReturnedType() {
    System.err.println("Error: accessing an unresolved dependency (Function) \"" + _dep +"\"");
    return null;
  }
}
