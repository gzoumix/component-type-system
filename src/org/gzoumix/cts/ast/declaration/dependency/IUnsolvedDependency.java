package org.gzoumix.cts.ast.declaration.dependency;

import org.gzoumix.cts.typing.Environment;

public interface IUnsolvedDependency {

  public Environment.PathElement<String> getDependency();
}
