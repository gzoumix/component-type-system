package org.gzoumix.cts.ast.declaration.dependency;

import org.gzoumix.cts.ast.declaration.IDeclarationMethod;
import org.gzoumix.cts.typing.Environment;

public class UnsolvedDependencyMethod implements IUnsolvedDependency, IDeclarationMethod {

  private Environment.PathElement<String> _dep;

  public UnsolvedDependencyMethod(Environment.PathElement<String> dep) { _dep = dep; }
  public Environment.PathElement<String> getDependency() { return _dep; }

 }
