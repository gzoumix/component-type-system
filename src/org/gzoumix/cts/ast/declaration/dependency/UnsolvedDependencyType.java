package org.gzoumix.cts.ast.declaration.dependency;

import org.gzoumix.cts.ast.declaration.*;
import org.gzoumix.cts.ast.type.ITypeUse;
import org.gzoumix.cts.ast.type.TypeUseDeclaredType;
import org.gzoumix.cts.ast.type.TypeUseVariable;
import org.gzoumix.cts.typing.Environment;
import org.gzoumix.cts.utils.Utils;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;


public class UnsolvedDependencyType implements IUnsolvedDependency, IDeclarationType, IDeclarationClass {

  private Environment.PathElement<String> _dep;

  public UnsolvedDependencyType(Environment.PathElement<String> dep) { _dep = dep; }
  public Environment.PathElement<String> getDependency() { return _dep; }

  // Dummy methods

  @Override
  public TypeUseDeclaredType createInstance() {
    System.err.println("Error #1: reference to the unbound type \"" + (Utils.concatModuleName(_dep)) + "\"");
    return null;
  }

  @Override
  public DeclarationMethod getMethod(String name, List<ITypeUse> typeParams) {
    System.err.println("Error #2: reference to the unbound type \"" + (Utils.concatModuleName(_dep)) + "\"");
    return null;
  }

  @Override
  public List<ITypeUse> getExtendsTypes() {
    System.err.println("Error #3: reference to the unbound type \"" + (Utils.concatModuleName(_dep)) + "\"");
    return null;
  }


  @Override
  public List<TypeUseVariable> getTypeParameters() {
    System.err.println("Error #6: reference to the unbound type \"" + (Utils.concatModuleName(_dep)) + "\"");
    return null;
  }

  @Override
  public List<DeclarationVariable> getParameters() {
    System.err.println("Error #7: reference to the unbound type \"" + (Utils.concatModuleName(_dep)) + "\"");
    return null;
  }

  @Override
  public String getName() {
    System.err.println("Error #8: reference to the unbound type \"" + (Utils.concatModuleName(_dep)) + "\"");
    return "Dependency(" + (Utils.concatModuleName(_dep)) + ")";
  }

  public String toString() { return "Dependency(" + (Utils.concatModuleName(_dep)) + ")"; }

}
