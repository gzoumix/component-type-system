package org.gzoumix.cts.ast.declaration.dependency;

import org.gzoumix.cts.ast.declaration.DeclarationDataType;
import org.gzoumix.cts.ast.declaration.IDeclarationDataTypeConstructor;
import org.gzoumix.cts.ast.type.ITypeUse;
import org.gzoumix.cts.typing.Environment;

import java.util.List;

public class UnsolvedDependencyDataTypeConstructor implements IUnsolvedDependency, IDeclarationDataTypeConstructor {

  private Environment.PathElement<String> _dep;

  public UnsolvedDependencyDataTypeConstructor(Environment.PathElement<String> dep) { _dep = dep; }
  public Environment.PathElement<String> getDependency() { return _dep; }

  @Override
  public List<ITypeUse> getParameters() {
    System.err.println("Error: accessing an unresolved dependency (DataType Constructor) \"" + _dep + "\"");
    return null;
  }

  @Override
  public DeclarationDataType getType() {
    System.err.println("Error: accessing an unresolved dependency (DataType Constructor) \"" + _dep + "\"");
    return null;
  }
}
