package org.gzoumix.cts.ast.declaration;

import org.gzoumix.cts.ast.type.TypeUseVariable;

import java.util.List;

public interface IDeclarationType extends IDeclaration {

  public List<TypeUseVariable> getTypeParameters();
  public String getName();

}
