package org.gzoumix.cts.ast.declaration;

import org.gzoumix.cts.ast.ASTNode;
import org.gzoumix.cts.ast.expression.IExpression;
import org.gzoumix.cts.ast.type.ITypeUse;

import java.util.LinkedList;
import java.util.List;

public class DeclarationVariable extends ASTNode implements IDeclaration {
  private boolean _is_port;

  private List<ITypeUse> _type;
  private List<IExpression> _exp;

  public DeclarationVariable(ITypeUse type, String name, boolean is_port, IExpression exp) {
    _name = new LinkedList<>();
    _name.add(name);

    _type = new LinkedList<>();
    _type.add(type);

    _exp = new LinkedList<>();
    _exp.add(exp);

    _is_port = is_port;

    _subDeclatation = new List[2];
    _subDeclatation[0] = _type;
    _subDeclatation[1] = _exp;
  }



  public boolean isPort() {
    return _is_port;
  }
  public ITypeUse getType() { return _type.get(0).canonicalForm(); }
  public boolean hasExpression() { return _exp.get(0) != null; }
  public IExpression getExpression() { return _exp.get(0); }
}
