package org.gzoumix.cts.ast.declaration;


import org.gzoumix.cts.ast.ASTNode;
import org.gzoumix.cts.ast.type.ITypeUse;
import org.gzoumix.cts.ast.type.TypeUseDeclaredType;
import org.gzoumix.cts.ast.type.TypeUseVariable;
import org.gzoumix.cts.utils.Logger;
import org.gzoumix.cts.utils.Utils;

import java.util.*;

public class DeclarationInterface extends ASTNode implements IDeclarationWithInstance {

  private List<ITypeUse> _extendedInterfaces;
  private List<TypeUseVariable> _typeParameters;
  private List<DeclarationMethod> _methods;


  private Map<String, DeclarationMethod> _map_method;

  public DeclarationInterface(List<String> module_name, String name) { // TODO: test
    _name = Utils.pathType(module_name, name);
    _extendedInterfaces = new LinkedList<>();
    _typeParameters = new LinkedList<>();
    _methods = new LinkedList<>();
    _map_method = new HashMap<>();

    _subDeclatation = new List[2];
    _subDeclatation[0] = _extendedInterfaces;
    _subDeclatation[1] = _methods;
  }


  public TypeUseVariable addTypeParameter(String parameter_name) {
    TypeUseVariable var = new TypeUseVariable(parameter_name);
    _typeParameters.add(var);
    return var;
  }


  public void addExtendedInterface(ITypeUse type) { _extendedInterfaces.add(type); }

  /*
  public Environment.PathElement<String> AddExtendedInterface(String name) {
    UnsolvedDependencyInterface dep = new UnsolvedDependencyInterface(new LinkedList<String>(), name);
    Environment.PathElement<String> dep_path = new Environment.PathElement<>();
    dep_path.add(name);

    dep = super.addDependency(dep_path, dep);
    _extendedInterfaces.add(dep);
    return dep_path;
  }*/

  public void addMethod(DeclarationMethod method) { _methods.add(method); _map_method.put(method.getMethodName(), method); }

  public TypeUseDeclaredType createInstance() {
    return new TypeUseDeclaredType(this, _typeParameters);
  }

  @Override
  public List<ITypeUse> getExtendsTypes() { return _extendedInterfaces; }
  @Override
  public List<TypeUseVariable> getTypeParameters() { return _typeParameters; }
  //@Override
  //public Set<String> getPorts() {
  //  return new HashSet<>();// TODO: it is currently impossible to declare a port in an interface...
  //}



  public DeclarationMethod getMethod(String method_name, List<ITypeUse> typeParameters) {
    // 1. create the mapping for the instantiation of the method
    Map<TypeUseVariable, ITypeUse> map = Utils.createMap(_typeParameters, typeParameters);

    // 2. find the method
    DeclarationMethod res = _map_method.get(method_name);
    if(res != null) { // we need to instantiate that type
      res = res.getIntance(map);
    } else { // the method may be declare in a supertype
      for(ITypeUse super_type: _extendedInterfaces) {
        if(super_type instanceof TypeUseDeclaredType) {
          List<ITypeUse> specParameters = ((TypeUseDeclaredType) super_type).getParameters();
          IDeclarationType decl = ((TypeUseDeclaredType) super_type).getDeclarationType();
          if(decl instanceof IDeclarationWithInstance) {
            List<ITypeUse> actParameters = new LinkedList<>();
            for(ITypeUse type: specParameters) { actParameters.add(type.replace(map)); }
            res = ((IDeclarationWithInstance) decl).getMethod(method_name, actParameters);
            if(res != null) { break; }
          } else {
            Logger.LOGGER.logError("Interface \"" + getName() + "\" extends the non-interface type \"" + decl.getName() + "\"");
          }
        } else {
          Logger.LOGGER.logError("Interface \"" + getName() + "\" extends a variable \"" + super_type + "\"");
        }
      }
    }
    return res;  }




}
