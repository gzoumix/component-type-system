package org.gzoumix.cts.ast.declaration;


import org.gzoumix.cts.ast.ASTNode;
import org.gzoumix.cts.ast.statement.StatementBlock;
import org.gzoumix.cts.ast.type.ITypeUse;
import org.gzoumix.cts.ast.type.TypeUseDeclaredType;
import org.gzoumix.cts.ast.type.TypeUseVariable;
import org.gzoumix.cts.utils.Logger;
import org.gzoumix.cts.utils.Utils;

import java.util.*;

public class DeclarationClass extends ASTNode implements IDeclarationClass {


  private List<TypeUseVariable> _typeParameters;
  private List<DeclarationVariable> _parameters;
  private List<ITypeUse> _implementedInterfaces;
  private List<DeclarationVariable> _fields;
  private List<DeclarationMethod> _methods;

  private Map<String, DeclarationMethod> _map_method;

  // 1. Constructor
  public DeclarationClass(List<String> module_name, String name) { // TODO: test
    _name = Utils.pathClass(module_name, name);
    _typeParameters = new LinkedList<>();
    _parameters = new LinkedList<>();
    _implementedInterfaces = new LinkedList<>();
    _fields = new LinkedList<>();
    _methods = new LinkedList<>();
    _map_method = new HashMap<>();

    _subDeclatation = new List[4];
    _subDeclatation[0] = _parameters;
    _subDeclatation[1] = _implementedInterfaces;
    _subDeclatation[2] = _fields;
    _subDeclatation[3] = _methods;
  }


  // 2. Extension Methods
  public TypeUseVariable addTypeParameter(String parameter_name) {
    TypeUseVariable var = new TypeUseVariable(parameter_name);
    _typeParameters.add(var);
    return var;
  }
  public void addParameter(DeclarationVariable var) { _parameters.add(var); }
  public void addImplementedInterface(ITypeUse i) { _implementedInterfaces.add(i); }
  public void addField(DeclarationVariable var) { _parameters.add(var); }
  public void addMethod(DeclarationMethod method) { _methods.add(method); _map_method.put(method.getMethodName(), method); }

  public void setInit(StatementBlock init) {} // TODO

  //3, Getters
  public List<DeclarationVariable> getAllFields() {
    List<DeclarationVariable> res = new LinkedList<>(_parameters);
    res.addAll(_fields);
    return res;
  }

  public Set<String> getPorts() {
    Set<String> res = new HashSet<>();
    for(DeclarationVariable var: _fields) { if(var.isPort()) { res.add(var.getName()); } }
    return res;
  }

  public List<DeclarationMethod> getMethods() { return _methods; }

  @Override
  public List<ITypeUse> getExtendsTypes() { return _implementedInterfaces; }
  @Override
  public List<TypeUseVariable> getTypeParameters() { return _typeParameters; }

  //@Override
  //public boolean isSubtypeOf(IDeclarationWithInstance param) {
  //  return true; // TODO: perform that test. Currently, it is done by the actual ABS type system.
  //}

  public TypeUseDeclaredType createInstance() {
    return new TypeUseDeclaredType(this, _typeParameters);
  }
  public List<DeclarationVariable> getParameters() { return _parameters; }



  public DeclarationMethod getMethod(String method_name, List<ITypeUse> typeParameters) {
    // 1. create the mapping for the instantiation of the method
    Map<TypeUseVariable, ITypeUse> map = Utils.createMap(_typeParameters, typeParameters);

    // 2. find the method
    DeclarationMethod res = _map_method.get(method_name);
    if(res != null) { // we need to instantiate that type
      res = res.getIntance(map);
    } else { // the method may be declare in a supertype
      for(ITypeUse super_type: _implementedInterfaces) {
        if(super_type instanceof TypeUseDeclaredType) {
          List<ITypeUse> specParameters = ((TypeUseDeclaredType) super_type).getParameters();
          IDeclarationType decl = ((TypeUseDeclaredType) super_type).getDeclarationType();
          if(decl instanceof IDeclarationWithInstance) {
            List<ITypeUse> actParameters = new LinkedList<>();
            for(ITypeUse type: specParameters) { actParameters.add(type.replace(map)); }
            res = ((IDeclarationWithInstance) decl).getMethod(method_name, actParameters);
            if(res != null) { break; }
          } else {
            Logger.LOGGER.logError("Class \"" + getName() + "\" implements the non-interface type \"" + decl.getName() + "\"");
          }
        } else {
          Logger.LOGGER.logError("Class \"" + getName() + "\" implements a variable \"" + super_type + "\"");
        }
      }
    }
    return res;
  }


}
