package org.gzoumix.cts.ast.declaration;


import org.gzoumix.cts.ast.ASTNode;
import org.gzoumix.cts.ast.type.TypeUseVariable;
import org.gzoumix.cts.utils.Utils;

import java.util.LinkedList;
import java.util.List;

public class DeclarationDataType extends ASTNode implements IDeclarationType {

  private List<DeclarationDataTypeConstructor> _constructors;
  private List<TypeUseVariable> _typeParameters;

  // 1. Constructor
  public DeclarationDataType(List<String> module_name, String name) {
    _name = Utils.pathType(module_name, name);
    init();
  }

  public DeclarationDataType(List<String> full_name) {
    _name = new LinkedList<>(full_name);
    init();
  }


  private void init() {
    _typeParameters = new LinkedList<>();
    _constructors = new LinkedList<>();
    _subDeclatation = new List[1];
    _subDeclatation[0] = _constructors;
  }

  // 2. Extension Methods
  public TypeUseVariable addTypeParameter(String parameter_name) {
    TypeUseVariable var = new TypeUseVariable(parameter_name);
    _typeParameters.add(var);
    return var;
  }

  public void addDataTypeConstructor(DeclarationDataTypeConstructor constructor) { _constructors.add(constructor); }


  @Override
  public List<TypeUseVariable> getTypeParameters() { return _typeParameters; }

  public String toString() { return getName(); }

}
