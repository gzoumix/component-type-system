package org.gzoumix.cts.ast.declaration;

import org.gzoumix.cts.ast.type.ITypeUse;

import java.util.List;

public interface IDeclarationFunction extends IDeclaration {
  public List<DeclarationVariable> getParameters();
  public ITypeUse getReturnedType();
}
