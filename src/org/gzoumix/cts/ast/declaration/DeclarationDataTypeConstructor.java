package org.gzoumix.cts.ast.declaration;

import org.gzoumix.cts.ast.ASTNode;
import org.gzoumix.cts.ast.type.ITypeUse;
import org.gzoumix.cts.utils.Utils;

import java.util.LinkedList;
import java.util.List;

public class DeclarationDataTypeConstructor extends ASTNode implements IDeclarationDataTypeConstructor {

  private DeclarationDataType _type;
  private List<ITypeUse> _parameters;

  // 1. Constructor
  public DeclarationDataTypeConstructor(DeclarationDataType type, List<String> module_name, String name) {
    _name = Utils.pathConstructor(module_name, name);

    _type = type;
    _parameters = new LinkedList<>();

    _subDeclatation = new List[1];
    _subDeclatation[0] = _parameters;
  }

  public void addParameter(ITypeUse typeUse) { _parameters.add(typeUse); }

  @Override
  public List<ITypeUse> getParameters() { return _parameters; }
  @Override
  public DeclarationDataType getType() { return _type; }

}
