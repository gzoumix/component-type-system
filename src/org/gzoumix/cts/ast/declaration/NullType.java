package org.gzoumix.cts.ast.declaration;

import org.gzoumix.cts.ast.type.ITypeUse;
import org.gzoumix.cts.ast.type.TypeUseDeclaredType;
import org.gzoumix.cts.ast.type.TypeUseVariable;
import org.gzoumix.cts.utils.Reference;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class NullType implements IDeclarationWithInstance {

  public static final NullType NULL_TYPE = new NullType();
  public static final ITypeUse NULL_TYPE_USE = new TypeUseDeclaredType(NULL_TYPE, new LinkedList<ITypeUse>());
  public static final DeclarationVariable NULL_TYPE_VAR_DECL = new DeclarationVariable(NULL_TYPE_USE, Reference.null_name, false, null);

  private NullType() {}

  @Override
  public TypeUseDeclaredType createInstance() {
    return null;
  }

  @Override
  public DeclarationMethod getMethod(String name, List<ITypeUse> typeParam) {
    return null;
  }

  @Override
  public List<ITypeUse> getExtendsTypes() {
    return null;
  }


  @Override
  public List<TypeUseVariable> getTypeParameters() {
    return null;
  }

  @Override
  public String getName() {
    return "NullType";
  }
}
