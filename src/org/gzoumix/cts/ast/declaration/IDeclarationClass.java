package org.gzoumix.cts.ast.declaration;

import java.util.List;

public interface IDeclarationClass extends IDeclarationWithInstance {

  public List<DeclarationVariable> getParameters();
}
