package org.gzoumix.cts.ast.declaration;

import org.gzoumix.cts.ast.ASTNode;
import org.gzoumix.cts.ast.statement.StatementBlock;
import org.gzoumix.cts.ast.type.GroupName;
import org.gzoumix.cts.ast.type.ITypeUse;
import org.gzoumix.cts.ast.type.TypeUseVariable;
import org.gzoumix.cts.utils.Reference;

import java.util.*;

public class DeclarationMethod extends ASTNode implements IDeclaration {

  private String _methodName;
  private boolean _is_critical;

  private List<DeclarationVariable> _parameters;
  private List<StatementBlock> _block;
  private List<DeclarationVariable> _this;
  private Set<GroupName> _groupCreated;
  private List<ITypeUse> _returnedType;


  public DeclarationMethod(List<String> module_name, String name_class, String name, boolean is_critical) {
    List<String> complete_name = new LinkedList<>(module_name);
    complete_name.add(name_class);
    complete_name.add(name);
    init(complete_name, name, is_critical);
  }

  private DeclarationMethod(List<String> complete_name, String name, boolean is_critical) { init(complete_name, name, is_critical); }

  private void init(List<String> complete_name, String name, boolean is_critical) {
    _name = complete_name;
    _methodName = name;
    _is_critical = is_critical;

    _parameters = new LinkedList<>();
    _block = new LinkedList<>();
    _this = new LinkedList<>();
    _returnedType = new LinkedList<>();

    _subDeclatation = new List[4];
    _subDeclatation[0] = _parameters;
    _subDeclatation[1] = _block;
    _subDeclatation[2] = _this;
    _subDeclatation[3] = _returnedType;
  }


  public void addParameter(DeclarationVariable var) { _parameters.add(var); }
  public void setBlock(StatementBlock block) { if(block != null) { _block.clear(); _block.add(block); } }

  public void setThis(ITypeUse type) { // must be called only once
    DeclarationVariable decl_this = new DeclarationVariable(type, Reference.this_name, false, null);
    _this.clear(); _this.add(decl_this);
  }

  public void setGroupCreated(List<GroupName> names) { _groupCreated = new HashSet<>(names); }
  public StatementBlock getBlock() { if(_block.isEmpty()) { return null; } else {return _block.get(0); } }
  public void setReturnedType(ITypeUse type) { _returnedType.clear(); _returnedType.add(type); }



  public String getMethodName() { return _methodName; }
  public List<DeclarationVariable> getParameters() { return _parameters; }
  public DeclarationVariable getThis() { return _this.get(0); }
  public Set<GroupName> getCreatedGroups() { return _groupCreated; }
  public ITypeUse getReturnedType() { return _returnedType.get(0); }
  //public DeclarationVariable getThisDeclaration() { return _this.get(0); }


  public DeclarationMethod getIntance(Map<TypeUseVariable, ITypeUse> map) {
    DeclarationMethod res = new DeclarationMethod(_name, _methodName, _is_critical);

    // 1. instantiate parameters
    for(DeclarationVariable var: _parameters) {
      ITypeUse var_type = var.getType().replace(map);
      res.addParameter(new DeclarationVariable(var_type, var.getName(), var.isPort(), var.getExpression()));
    }

    // 2. set this
    res.setThis(this.getThis().getType());

    // 3. set created group names
    if(_groupCreated != null) { res.setGroupCreated(new LinkedList<GroupName>(_groupCreated)); }

    // 3. set block
    res.setBlock(this.getBlock());

    // 4. instantiate returned type
    ITypeUse returned_type = getReturnedType().replace(map);
    res.setReturnedType(returned_type);

    return res;
  }

}
