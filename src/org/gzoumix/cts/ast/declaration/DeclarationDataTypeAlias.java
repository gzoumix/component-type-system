package org.gzoumix.cts.ast.declaration;

import org.gzoumix.cts.ast.ASTNode;
import org.gzoumix.cts.ast.type.ITypeUse;
import org.gzoumix.cts.ast.type.TypeUseVariable;
import org.gzoumix.cts.utils.Utils;

import java.util.LinkedList;
import java.util.List;

public class DeclarationDataTypeAlias extends ASTNode implements IDeclarationType {

  private List<TypeUseVariable> _typeParameters;
  private List<ITypeUse> _definition;

  public DeclarationDataTypeAlias(List<String> module_name, String name) {
    _name = Utils.pathType(module_name, name);

    _typeParameters = new LinkedList<>();
    _definition = new LinkedList<>();
    _subDeclatation = new List[1];
    _subDeclatation[0] = _definition;
  }


  public TypeUseVariable addTypeParameter(String parameter_name) {
    TypeUseVariable var = new TypeUseVariable(parameter_name);
    _typeParameters.add(var);
    return var;
  }

  public void setDefinition(ITypeUse definition) {
    _definition.clear();
    _definition.add(definition);
  }

  @Override
  public List<TypeUseVariable> getTypeParameters() { return _typeParameters; }
  public ITypeUse getDefinition() { return _definition.get(0); }
}
