package org.gzoumix.cts.ast.declaration;


import org.gzoumix.cts.ast.ASTNode;
import org.gzoumix.cts.ast.expression.IExpressionPure;
import org.gzoumix.cts.ast.type.ITypeUse;
import org.gzoumix.cts.ast.type.TypeUseVariable;

import java.util.LinkedList;
import java.util.List;

public class DeclarationFunction extends ASTNode implements IDeclarationFunction {

  private List<TypeUseVariable> _typeParameters;
  private List<DeclarationVariable> _parameters;

  private List<IExpressionPure> _definition;
  private List<ITypeUse> _returnedType;

  // 1. Constructor
  public DeclarationFunction(List<String> module_name, String name) {
    _name = new LinkedList<>(module_name);
    _name.add(name);
    init();
  }

  public DeclarationFunction(List<String> full_name) {
    _name = new LinkedList<>(full_name);
    init();
  }

  private void init() {
    _typeParameters = new LinkedList<>();
    _parameters = new LinkedList<>();
    _returnedType = new LinkedList<>();

    _definition = new LinkedList<>();
    _subDeclatation = new List[3];
    _subDeclatation[0] = _parameters;
    _subDeclatation[1] = _definition;
    _subDeclatation[2] = _returnedType;
  }

  // 2. Extension Methods
  public TypeUseVariable addTypeParameter(String parameter_name) {
    TypeUseVariable var = new TypeUseVariable(parameter_name);
    _typeParameters.add(var);
    return var;
  }

  public void addParameter(DeclarationVariable var) { _parameters.add(var); }

  public void setReturnedType(ITypeUse type) {
    _returnedType.clear(); _returnedType.add(type);
  }


  public void setDefinition(IExpressionPure definition) {
    _definition.clear();
    _definition.add(definition);
  }

  public List<DeclarationVariable> getParameters() { return _parameters; }
  public ITypeUse getReturnedType() { return _returnedType.get(0); }

}
