package org.gzoumix.cts.ast.declaration;

import org.gzoumix.cts.ast.type.ITypeUse;

import java.util.List;

public interface IDeclarationDataTypeConstructor extends IDeclaration {
  public List<ITypeUse> getParameters();
  public DeclarationDataType getType();

}
