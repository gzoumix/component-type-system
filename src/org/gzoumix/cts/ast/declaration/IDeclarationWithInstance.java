package org.gzoumix.cts.ast.declaration;

import org.gzoumix.cts.ast.type.ITypeUse;
import org.gzoumix.cts.ast.type.TypeUseDeclaredType;

import java.util.List;
import java.util.Set;

public interface IDeclarationWithInstance extends IDeclarationType {

  public TypeUseDeclaredType createInstance();
  public DeclarationMethod getMethod(String name, List<ITypeUse> parameters);

  public List<ITypeUse> getExtendsTypes();

  //@Deprecated
  //public boolean isSubtypeOf(IDeclarationWithInstance param); // deprecated because erroneous
  //public Set<String> getPorts();
}
