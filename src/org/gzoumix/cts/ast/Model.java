package org.gzoumix.cts.ast;


import org.gzoumix.cts.ast.statement.StatementBlock;
import org.gzoumix.cts.typing.Environment;
import org.gzoumix.cts.utils.Utils;

import java.util.*;

public class Model extends ASTNode {

  private static final List<String> _NAME_STATIC;
  static{
    _NAME_STATIC = new LinkedList<>();
    _NAME_STATIC.add("Model");
  }

  private List<CompilationUnit> _compilationUnits;
  private StatementBlock _main;
  private Environment _env;

  public Model() {
    _name = _NAME_STATIC;

    _compilationUnits = new LinkedList<>();
    _main = null;
    _env = new Environment();
    Utils.initEnvironment(_env);

    _subDeclatation = new List[1];
    _subDeclatation[0] = _compilationUnits;
  }

  public Model(Collection<? extends CompilationUnit> compilationUnits) {
    _compilationUnits = new LinkedList<>(compilationUnits);
    _main = null;
    for(CompilationUnit u: compilationUnits) {
      if(u.hasMain()) { _main = u.getMain(); return; }
    }

    _subDeclatation = new List[1];
    _subDeclatation[0] = _compilationUnits;
  }

  public void addCompilationUnit(CompilationUnit u) {
    _compilationUnits.add(u);
    if((u.hasMain()) && (_main == null)) { _main = u.getMain(); }
  }

  //public void setMain(StatementBlock m) { _main = m; }
  public boolean hasMain() { return _main != null; }
  public StatementBlock getMain() { return _main; }
  public Environment getEnvironment() { return _env; }
  public List<CompilationUnit> getCompilationUnits() { return _compilationUnits; }


}

