package org.gzoumix.cts.ast.statement;

import org.gzoumix.cts.ast.ASTNode;
import org.gzoumix.cts.ast.declaration.DeclarationVariable;

import java.util.LinkedList;
import java.util.List;

public class StatementDeclaration extends ASTNode implements IStatement {

  private static final List<String> _NAME_STATIC;
  static{
    _NAME_STATIC = new LinkedList<>();
    _NAME_STATIC.add("StatementDeclaration");
  }

  private List<DeclarationVariable> _decl;

  public StatementDeclaration(DeclarationVariable decl) {
    _name = _NAME_STATIC;

    _decl = new LinkedList<>();
    _decl.add(decl);

    _subDeclatation = new List[1];
    _subDeclatation[0] = _decl;
  }

  public DeclarationVariable getDeclaration() { return _decl.get(0); }
}
