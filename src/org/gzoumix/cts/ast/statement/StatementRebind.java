package org.gzoumix.cts.ast.statement;

import org.gzoumix.cts.ast.ASTNode;
import org.gzoumix.cts.ast.expression.IExpression;

import java.util.LinkedList;
import java.util.List;

public class StatementRebind extends ASTNode implements IStatement {

  private static final List<String> _NAME_STATIC;
  static{
    _NAME_STATIC = new LinkedList<>();
    _NAME_STATIC.add("StatementRebind");
  }

  private List<IExpression> _obj;
  private String _field;
  private List<IExpression> _exp;

  public StatementRebind(IExpression obj, String field, IExpression exp) {
    _name = _NAME_STATIC;

    _obj = new LinkedList<>(); _obj.add(obj);
    _field = field;
    _exp = new LinkedList<>(); _exp.add(exp);

    _subDeclatation = new List[2];
    _subDeclatation[0] = _obj;
    _subDeclatation[1] = _exp;
  }

  public IExpression getReboundObject() { return _obj.get(0); }
  public IExpression getExpression() { return _exp.get(0); }
  public String getReboundField() { return _field; }
}
