package org.gzoumix.cts.ast.statement;

import org.gzoumix.cts.ast.ASTNode;
import org.gzoumix.cts.ast.expression.IExpression;

import java.util.LinkedList;
import java.util.List;

public class StatementExpression extends ASTNode implements IStatement {

  private static final List<String> _NAME_STATIC;
  static{
    _NAME_STATIC = new LinkedList<>();
    _NAME_STATIC.add("StatementExpression");
  }

  private List<IExpression> _exp;


  public StatementExpression(IExpression exp) {
    _name = _NAME_STATIC;

    _exp = new LinkedList<>(); _exp.add(exp);

    _subDeclatation = new List[1];
    _subDeclatation[0] = _exp;
  }

  public IExpression getExpression() { return _exp.get(0); }

}
