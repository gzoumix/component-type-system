package org.gzoumix.cts.ast.statement;

import org.gzoumix.cts.ast.ASTNode;
import org.gzoumix.cts.ast.expression.IExpressionPure;

import java.util.LinkedList;
import java.util.List;

public class StatementWhile extends ASTNode implements IStatement {

  private static final List<String> _NAME_STATIC;
  static{
    _NAME_STATIC = new LinkedList<>();
    _NAME_STATIC.add("StatementWhile");
  }

  private List<IExpressionPure> _exp;
  private List<IStatement> _stmt;

  public StatementWhile(IExpressionPure exp, IStatement stmt) {
    _name = _NAME_STATIC;

    _exp = new LinkedList<>(); _exp.add(exp);
    _stmt = new LinkedList<>(); _stmt.add(stmt);

    _subDeclatation = new List[2];
    _subDeclatation[0] = _exp;
    _subDeclatation[1] = _stmt;
  }

  public IExpressionPure getExpression() { return _exp.get(0); }
  public IStatement getStatement() { return _stmt.get(0); }
}
