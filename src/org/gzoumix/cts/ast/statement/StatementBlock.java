package org.gzoumix.cts.ast.statement;

import org.gzoumix.cts.ast.ASTNode;

import java.util.LinkedList;
import java.util.List;

public class StatementBlock extends ASTNode implements IStatement {

  private static final List<String> _NAME_STATIC;
  static{
    _NAME_STATIC = new LinkedList<>();
    _NAME_STATIC.add("StatementBlock");
  }

  private List<IStatement> _stmts;

  public StatementBlock() {
    _name = _NAME_STATIC;

    _stmts = new LinkedList<>();

    _subDeclatation = new List[1];
    _subDeclatation[0] = _stmts;
  }

  public void addStatement(IStatement stmt) { _stmts.add(stmt); }
  public List<IStatement> getStatements() { return _stmts; }

}
