package org.gzoumix.cts.ast.statement;

import org.gzoumix.cts.ast.ASTNode;
import org.gzoumix.cts.ast.guard.IGuard;

import java.util.LinkedList;
import java.util.List;

public class StatementAwait extends ASTNode implements IStatement {

  private static final List<String> _NAME_STATIC;
  static{
    _NAME_STATIC = new LinkedList<>();
    _NAME_STATIC.add("StatementAwait");
  }

  private List<IGuard> _guard;

  public StatementAwait(IGuard guard) {
    _name = _NAME_STATIC;

    _guard = new LinkedList<>();
    _guard.add(guard);

    _subDeclatation = new List[1];
    _subDeclatation[0] = _guard;
  }

  public IGuard getGuard() { return _guard.get(0); }
}
