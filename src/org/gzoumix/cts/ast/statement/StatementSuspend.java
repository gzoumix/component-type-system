package org.gzoumix.cts.ast.statement;

import org.gzoumix.cts.ast.ASTNode;

import java.util.LinkedList;
import java.util.List;

public class StatementSuspend extends ASTNode implements IStatement {

  private static final List<String> _NAME_STATIC;
  static{
    _NAME_STATIC = new LinkedList<>();
    _NAME_STATIC.add("StatementSuspend");
  }

  public StatementSuspend() {
    _name = _NAME_STATIC;

    _subDeclatation = new List[0];
  }
}
