package org.gzoumix.cts.ast.statement;

import org.gzoumix.cts.ast.ASTNode;
import org.gzoumix.cts.ast.declaration.DeclarationVariable;
import org.gzoumix.cts.ast.expression.IExpression;

import java.util.LinkedList;
import java.util.List;

public class StatementAssign extends ASTNode implements IStatement {

  private static final List<String> _NAME_STATIC;
  static{
    _NAME_STATIC = new LinkedList<>();
    _NAME_STATIC.add("StatementAssign");
  }

  private DeclarationVariable _var;
  private List<IExpression> _exp;

  public StatementAssign(DeclarationVariable var, IExpression exp) {
    _name = _NAME_STATIC;

    _var = var;
    _exp = new LinkedList<>();
    _exp.add(exp);

    _subDeclatation = new List[1];
    _subDeclatation[0] = _exp;
  }

  public DeclarationVariable getVar() { return _var; }
  public IExpression getExp() { return _exp.get(0); }

}
