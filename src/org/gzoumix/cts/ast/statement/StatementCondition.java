package org.gzoumix.cts.ast.statement;

import org.gzoumix.cts.ast.ASTNode;
import org.gzoumix.cts.ast.expression.IExpression;
import org.gzoumix.cts.ast.expression.IExpressionPure;

import java.util.LinkedList;
import java.util.List;

public class StatementCondition extends ASTNode implements IStatement {

  private static final List<String> _NAME_STATIC;
  static{
    _NAME_STATIC = new LinkedList<>();
    _NAME_STATIC.add("StatementCondition");
  }

  private List<IExpressionPure> _exp;
  private List<IStatement> _stmt_if;
  private List<IStatement> _stmt_else;

  public StatementCondition(IExpressionPure exp, IStatement stmt_if, IStatement stmt_else) {
    _name = _NAME_STATIC;

    _exp = new LinkedList<>(); _exp.add(exp);
    _stmt_if = new LinkedList<>(); _stmt_if.add(stmt_if);
    _stmt_else = new LinkedList<>(); if(stmt_else != null) { _stmt_else.add(stmt_else); }

    _subDeclatation = new List[3];
    _subDeclatation[0] = _exp;
    _subDeclatation[1] = _stmt_if;
    _subDeclatation[2] = _stmt_else;
  }

  public boolean hasElse() { return !_stmt_else.isEmpty(); }
  public IStatement getIf() { return _stmt_if.get(0); }
  public IStatement getElse() { return _stmt_else.get(0); }
}
