package org.gzoumix.cts;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.gzoumix.cts.ast.CompilationUnit;
import org.gzoumix.cts.ast.Model;
import org.gzoumix.cts.parser.CTSLexer;
import org.gzoumix.cts.parser.CTSParser;
import org.gzoumix.cts.parser.CTSVisitor;
import org.gzoumix.cts.typing.ASTVisitorTyping;
import org.gzoumix.cts.typing.Environment;
import org.gzoumix.cts.utils.Logger;
import org.gzoumix.cts.utils.Reference;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;


/*
 The structure of this project is entirely wrong.
  - 1. I need another, more flexible implementation of environments
  - 2. I first need to look at the import, and decide from it which files I need to consider in the typing
  - 3. I need to compute the environment with all declarations (but without their definitions) -- for that, I still need to have unsolved dependencies
  - 4. Here I can start the control: the environment should be without any dandling dependencies
  - 5. Then I can add the definition of everything. And check everything
       - If I have unsolved dependencies, this is an error
       - I can also check if a type is an interface or a datatype, etc
       - One of the most important feature of that design is that while controling the code, we have direct access to the definitions
 */


public class ABSProgramLoader {

  public Model loadABSProgram(String[] name_file_array) throws IOException { //TODO
    // init
    Model res = new Model();
    Environment env = res.getEnvironment();
    CTSVisitor visitor = new CTSVisitor();
    visitor.setEnvironment(env);

    // loading files, translating them in compilation units, and additing them to the model
    for(String name_file: name_file_array) {
      Logger.LOGGER.logDebug("Parsing file \"" + name_file + "\"");
      CompilationUnit compilationUnit = loadABSFile(name_file, visitor);
      res.addCompilationUnit(compilationUnit);
    }
    return res;
  }

  public CompilationUnit loadABSFile(String name_file, CTSVisitor visitor) throws IOException {
    File file = new File(name_file);
    // create a CharStream that reads from standard input
    ANTLRInputStream input = new ANTLRInputStream(new FileInputStream(file));
    // create a lexer that feeds off of input CharStream
    CTSLexer lexer = new CTSLexer(input);
    // create a buffer of tokens pulled from the lexer
    CommonTokenStream tokens = new CommonTokenStream(lexer);
    // create a parser that feeds off the tokens buffer
    CTSParser parser = new CTSParser(tokens);
    return visitor.visitCompilationUnit(name_file, parser.compilation_unit());
  }

  public static void main(String... args) {
    ABSProgramLoader loader = new ABSProgramLoader();
    try {
      // 1. Load the program
      Model model = loader.loadABSProgram(args);
      Logger.LOGGER.logDebug("Computed Environment = ");
      Logger.LOGGER.logDebug(model.getEnvironment().toString());

      // DEBUG: priintling the declaration of basic types
      //Logger.LOGGER.logDebug("Unit   = " + model.getEnvironment().get(Reference.pathElement_unit));
      //Logger.LOGGER.logDebug("Fut    = " + model.getEnvironment().get(Reference.pathElement_fut));
      //Logger.LOGGER.logDebug("Int    = " + model.getEnvironment().get(Reference.pathElement_int));
      //Logger.LOGGER.logDebug("Bool   = " + model.getEnvironment().get(Reference.pathElement_bool));
      //Logger.LOGGER.logDebug("String = " + model.getEnvironment().get(Reference.pathElement_string));

      // TODO: if missing dependency, print it and exit.
      Environment env = model.getEnvironment();
      if(env.hasDependency()) {
        Logger.LOGGER.logError("Error: Missing declarations (Sorry, displaying which declarations are missing will be implemented shortly)");
        System.exit(0);
      }

      // TODO: check the length of all class, method, function, types, constructors, to ensure that they correspond to the declaration

      System.out.println("Starting typing");
      ASTVisitorTyping typing = new ASTVisitorTyping();
      typing.visitTypingModel(model);

    } catch (IOException e) {
      e.printStackTrace();
    }
  }

}
