grammar CTS;

main: compilation_unit EOF;


compilation_unit: ( decl_module )*  ;


decl_module
  : MODULE name_module_type_constructor SEMI export_entry* import_entry* decl* block?
  ;


// Annotations
annotation
  : LBRACK exp_type COLON exp_pure RBRACK
  | LBRACK exp_pure RBRACK
    ;


// Exports and imports DEBUG: any_name_list, import
export_entry
  : EXPORT name_any_list SEMI                                      #ExportElementList
  | EXPORT TIMES SEMI                                              #ExportEverything
  | EXPORT TIMES FROM name_module_type_constructor SEMI            #ExportNonSenseOne
  | EXPORT name_any_list FROM name_module_type_constructor SEMI    #ExportNonSenseTwo
  ;

import_entry
  : IMPORT TIMES FROM name_module_type_constructor SEMI            #ImportEverythingFromModule
  | IMPORT name_any_list FROM name_module_type_constructor SEMI    #ImportElementListFromModule
  | IMPORT name_any_list SEMI                                      #ImportElementList
  ;

// Declarations


decl
  : decl_datatype                             #DeclarationDataType
  | decl_typesyn                              #DeclarationDataTypeAlias
  | decl_function                             #DeclarationFunction
  | decl_interface                            #DeclarationInterface
  | decl_class                                #DeclarationClass
  ;

// datatypeparam_list_decl => pattern_type_list
decl_datatype
  : (annotation)* DATA name_module_type_constructor pattern_type_list? EQUAL decl_data_constructor ( BAR decl_data_constructor )* SEMI
  | (annotation)* DATA name_module_type_constructor pattern_type_list? SEMI
  ;

decl_data_constructor
  : name_module_type_constructor ( LPAREN exp_type ( COMMA exp_type )* RPAREN )?
  ;

decl_typesyn
  : (annotation)* TYPE name_module_type_constructor pattern_type_list? EQUAL exp_type SEMI
  ;

decl_function
  : (annotation)* DEF exp_type name_val pattern_type_list? LPAREN pattern_val_list? RPAREN EQUAL BUILTIN SEMI
  | (annotation)* DEF exp_type name_val pattern_type_list? LPAREN pattern_val_list? RPAREN EQUAL exp_pure SEMI
  ;

decl_interface
  : (annotation)* INTERFACE name_module_type_constructor pattern_type_list? (EXTENDS exp_type_list)? LCBRAC (decl_method SEMI)* RCBRAC
  ;

decl_class
  : (annotation)* CLASS name_module_type_constructor pattern_type_list? (LPAREN pattern_val_list RPAREN)? ( IMPLEMENTS exp_type_list )? LCBRAC
      (decl_field)*
      (method)*
      block?
    RCBRAC
  ;

decl_method
  : (annotation)* CRITICAL? exp_type decl_this_type? name_val LPAREN  ( pattern_val_list )? RPAREN
  ;

decl_field
  : (annotation)* PORT? exp_type name_val ( EQUAL exp_pure )? SEMI
  ;

method
  : decl_method block
  ;

decl_this_type: LSBRAC LCBRAC name_val* RCBRAC BAR record RSBRAC;

// blocks and statements
block
  : (annotation)* LCBRAC ( (annotation)* stmt )*  RCBRAC
  ;

stmt
  : exp_type name_val ( EQUAL exp_eff )? SEMI           #StatementDeclarationVariable
  | SKIP SEMI                                           #StatementSkip
  | val_reference EQUAL exp_eff SEMI                    #StatementAssign
  | AWAIT guard SEMI                                    #StatementAwait
  | SUSPEND SEMI                                        #StatementSuspend
  | IF LPAREN exp_pure RPAREN stmt ( ELSE stmt )?       #StatementCondition
  | WHILE LPAREN exp_pure RPAREN stmt                   #StatementWhile
  | block                                               #StatementBlock
  | RETURN exp_eff SEMI                                 #StatementReturn
  | exp_eff SEMI                                        #StatementExpression
  | REBIND exp_eff COLON name_val EQUAL exp_eff SEMI    #StatementRebind
  | 'assert'exp_eff SEMI                                #StatementAssert
  ;

// guards, expressions
guard
  : name_val QMARK                                      #GuardFuture       // waiting on a future
  | exp_pure                                            #GuardExpression   // waiting on a boolean
  | guard LAND guard                                    #GuardAnd          // conjunction of guards
  | BAR exp_pure BAR                                    #GuardObject       // waiting on transcient state
  ;


exp_eff
  : exp_pure                                                                  #ExpressionPure        // pure expression
  | exp_pure DOT GET                                                          #ExpressionGet         // get expression
  | NEW LOCAL? name_module_type_constructor LPAREN exp_pure_list? RPAREN      #ExpressionNew         // object and cog creation
  | exp_pure EMARK name_val LPAREN exp_pure_list? RPAREN                      #ExpressionMethodCall  // asynchronous method call
  | exp_pure DOT name_val LPAREN exp_pure_list? RPAREN                        #ExpressionMethodCall  // synchronous method call
  | AWAIT exp_pure EMARK name_val LPAREN exp_pure_list? RPAREN                #ExpressionAwait       // await on a method call (similar to get, but without locking)
  ;

exp_pure
  : exp_pure op_binary exp_pure                                               #ExpressionBinaryOperation
  | op_unary exp_pure                                                         #ExpressionUnaryOperation
  | LPAREN exp_pure RPAREN                                                    #ExpressionSubExpression
  | literal                                                                   #ExpressionLiteral
  | name_module_type_constructor (LPAREN exp_pure (COMMA exp_pure)* RPAREN)?  #ExpressionDataType
  | val_reference                                                             #ExpressionReference
  | name_val LPAREN (exp_pure (COMMA exp_pure)*)? RPAREN                      #ExpressionFunctionApplication     // function call
  | LET LPAREN pattern_val RPAREN EQUAL exp_pure IN exp_pure                  #ExpressionLet
  | IF exp_pure THEN exp_pure ELSE exp_pure                                   #ExpressionCondition
  | CASE exp_pure LCBRAC ( pattern_case RARROW exp_pure SEMI )+ RCBRAC        #ExpressionMatch                   // case expressions
  | name_val LBRACK exp_pure_list? RBRACK                                     #ExpressionListConstruction        // list definition
  ;

exp_pure_list: exp_pure ( COMMA exp_pure )*;


exp_type
  : name_module_type_constructor (LSBRAC exp_type ( COMMA exp_type )* RSBRAC)? ( COLON record)? (name_val)?
  ;

exp_type_list: exp_type ( COMMA exp_type)*;

record
  : WILD                                                                      #RecordBottom
  | LBRACK name_val BAR (record_field (SEMI record_field)* )? RBRACK          #RecordConstruct
  ;

record_field : name_val EQUAL exp_type ;

// patterns
pattern_type_list
  : LSBRAC name_module_type_constructor ( COMMA name_module_type_constructor )* RSBRAC
  ;

pattern_val_list
  : pattern_val ( COMMA pattern_val )*
  ;

pattern_val : annotation* exp_type name_val;

pattern_case
  : name_val
  | WILD
  | name_module_type_constructor (LPAREN pattern_case ( COMMA pattern_case )* RPAREN)?
  | literal
  ;



/**********************************************/
/*        KEYWORDS AND IDENTIFIERS            */
/**********************************************/

// White Spaces and Comments management
WhiteSpaces: [\n\r\t ] -> skip;
CommentsOneLine: '//' .*? '\n' -> skip;
CommentsParagraph: '/*' .*? '*/' -> skip;


// Keywords
MODULE     : 'module';
EXPORT     : 'export';
IMPORT     : 'import';
FROM       : 'from';
DATA       : 'data';
TYPE       : 'type';
DEF        : 'def';
BUILTIN    : 'builtin';
INTERFACE  : 'interface';
EXTENDS    : 'extends';
CLASS      : 'class';
IMPLEMENTS : 'implements';
CRITICAL   : 'critical';
PORT       : 'port';
REBIND     : 'rebind';
AWAIT      : 'await';
SUSPEND    : 'suspend';
SKIP       : 'skip';
IF         : 'if';
THEN       : 'then';
ELSE       : 'else';
WHILE      : 'while';
RETURN     : 'return';
THIS       : 'this';
GET        : 'get';
NEW        : 'new';
LOCAL      : 'cog';
CASE       : 'case';
LET        : 'let';
IN         : 'in';



// Names

val_reference: name_val | THIS | THIS DOT name_val;

name_type_list: name_module_type_constructor (COMMA name_module_type_constructor)* ;

name_any_list: ( name_module_type_constructor | name_val ) ( COMMA ( name_module_type_constructor | name_val ) )*;

name_module_type_constructor: UID;
name_val: LID;


// tokens
literal: NUMBER | STRING;
NUMBER: [0-9]+('.'[0-9]+)?; // Numbers
STRING: DQUOTE ( ('\\' [btnr"\\]) | . )*? DQUOTE; // String

LID: [a-z][a-zA-Z0-9_]*;   // Identifiers for variables
UID: [A-Z][a-zA-Z0-9_.]*;    // Identifiers for constructors and types


op_binary: PLUS | MINUS | TIMES | DIV | MOD | LEQUAL | DIFF | LEQ | LSBRAC | GEQ | RSBRAC | LAND | LOR; // syntactic sugar
op_unary : PLUS | MINUS | NEG;                                                                     // syntactic sugar


LPAREN: '(';
RPAREN: ')';
LBRACK: '[';
RBRACK: ']';
LCBRAC: '{';
RCBRAC: '}';
LSBRAC: '<';
RSBRAC: '>';

COMMA : ',';
SEMI  : ';';
COLON : ':';
SQUOTE: '\'';
DQUOTE: '"';
QMARK : '?';
EMARK : '!';
BAR   : '|';
DOT   : '.';
WILD  : '_';

PLUS   : '+';
MINUS  : '-';
TIMES  : '*';
DIV    : '/';
MOD    : '%';
EQUAL  : '=';
LEQUAL : '==';
DIFF   : '!=';
LEQ    : '<=';
GEQ    : '>=';
LAND   : '&&';
LOR    : '||';
NEG    : '~';

FSLASH : '/';
BSLASH : '\\';

LARROW: '<-';
RARROW: '=>';

