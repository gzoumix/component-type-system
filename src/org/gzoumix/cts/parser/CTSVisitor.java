package org.gzoumix.cts.parser;

import org.gzoumix.cts.ast.CompilationUnit;
import org.gzoumix.cts.ast.Module;
import org.gzoumix.cts.ast.ASTNode;
import org.gzoumix.cts.ast.declaration.*;
import org.gzoumix.cts.ast.declaration.dependency.*;
import org.gzoumix.cts.ast.expression.*;
import org.gzoumix.cts.ast.guard.IGuard;
import org.gzoumix.cts.ast.statement.*;
import org.gzoumix.cts.ast.type.*;
import org.gzoumix.cts.typing.Environment;
import org.gzoumix.cts.utils.Logger;
import org.gzoumix.cts.utils.Reference;
import org.gzoumix.cts.utils.Utils;


import org.gzoumix.typing.environment.EnvironmentDeclatation;
import org.gzoumix.typing.environment.EnvironmentVariable;

import java.util.*;


/// TODO:
/// BUG the fields and parameters of the class are not registered in the variable map.


public class CTSVisitor {

  private Environment _env;
  private List<String> _module_name; // For security (in ABS, there isn't really a hierarchy of module) we split the name of a module with '.' as split point

  private ASTNode _currentDeclaration;                                        // the declaration we are currently constructing (used to know where to generate dependencies)
  private Map<String, TypeUseVariable> _mapTypeVariable;                      // the locally declared type variables, with their AST representation
  private Map<String, GroupName> _mapGroupName;                               // the locally declared group names, with their AST representation
  private EnvironmentVariable<String, DeclarationVariable> _mapVariable;      // the locally declared variables
  private Set<Environment.PathElement<String>> _dependencies;                 // the local dependencies


  public CTSVisitor() {
    _mapVariable = new EnvironmentVariable<>();
    _mapVariable.newScope();
    _mapVariable.putVariable(Reference.null_name, NullType.NULL_TYPE_VAR_DECL);
  }


  public void setEnvironment(Environment env) { _env = env; }
  // In the environment, I register:
  // 1. modules that have a main (for dependency solving)



  /*********************************************/
  /*********** MAIN ENTRIES ********************/
  /*********************************************/

  public CompilationUnit visitCompilationUnit(String file_name , CTSParser.Compilation_unitContext ctx) {
    Logger.LOGGER.beginIndent();
    List<Module> list = new LinkedList<>();
    for(CTSParser.Decl_moduleContext module: ctx.decl_module()) {
      list.add(visitModule(module));
    }
    Logger.LOGGER.endIndent();

    return new CompilationUnit(file_name, list);
  }

  private Module visitModule(CTSParser.Decl_moduleContext ctx) {
    _module_name = Utils.splitModuleName(ctx.name_module_type_constructor().getText()); // we entered in that module
    List<String> module_name_class  = new LinkedList<>(_module_name); module_name_class.add("CLASS");
    List<String> module_name_type   = new LinkedList<>(_module_name); module_name_class.add("TYPE");
    List<String> module_name_const = new LinkedList<>(_module_name); module_name_class.add("CONST");

    Logger.LOGGER.logDebug("Parsing Module \"" + _module_name + "\"");

    // 1. manage visibility: For now, we only implements imports (because it is already some work)
    List<Environment.SearchPath<String>> imports = new LinkedList<>();
    imports.add(new Environment.SearchPath<String>(new Environment.PathStar<String>(_module_name), true)); // add visibility to the entire current module
    imports.add(new Environment.SearchPath<String>(new Environment.PathStar<String>(module_name_class), true)); // add visibility to the entire current module (classes)
    imports.add(new Environment.SearchPath<String>(new Environment.PathStar<String>(module_name_type), true)); // add visibility to the entire current module  (types)
    imports.add(new Environment.SearchPath<String>(new Environment.PathStar<String>(module_name_const), true)); // add visibility to the entire current module (constructors)

    for(CTSParser.Import_entryContext import_entry_ctx: ctx.import_entry()) {
      if(import_entry_ctx instanceof CTSParser.ImportEverythingFromModuleContext) {
        List<String> includedModule = Utils.splitModuleName(((CTSParser.ImportEverythingFromModuleContext) import_entry_ctx).name_module_type_constructor().getText());
        imports.add(new Environment.SearchPath<String>(new Environment.PathStar<String>(includedModule), true));
      } else if(import_entry_ctx instanceof CTSParser.ImportElementListFromModuleContext) {
        List<String> module = Utils.splitModuleName(((CTSParser.ImportElementListFromModuleContext) import_entry_ctx).name_module_type_constructor().getText());
        CTSParser.Name_any_listContext names_ctx = ((CTSParser.ImportElementListFromModuleContext) import_entry_ctx).name_any_list();
        List<List<String>> names = new LinkedList<>();
        for(CTSParser.Name_module_type_constructorContext element_ctx: names_ctx.name_module_type_constructor())  { // adding Uppercase names
          names.add(Utils.splitModuleName(element_ctx.getText()));
        }
        for(CTSParser.Name_valContext element_ctx: names_ctx.name_val())  { // adding lowercase names
          names.add(Utils.splitModuleName(element_ctx.getText()));
        }
        // adding the concatenation module.name for all the names in the list
        for(List<String> name: names) {
          List<String> full_name = new LinkedList<>();
          full_name.addAll(module);
          full_name.addAll(name);
          imports.add(new Environment.SearchPath<String>(new Environment.PathElement<>(full_name), true));
        }
      } else if(import_entry_ctx instanceof CTSParser.ImportElementListContext) {
        CTSParser.Name_any_listContext names_ctx = ((CTSParser.ImportElementListContext) import_entry_ctx).name_any_list();
        List<List<String>> names = new LinkedList<>();
        for(CTSParser.Name_module_type_constructorContext element_ctx: names_ctx.name_module_type_constructor())  { // adding Uppercase names
          names.add(Utils.splitModuleName(element_ctx.getText()));
        }
        for(CTSParser.Name_valContext element_ctx: names_ctx.name_val())  { // adding lowercase names
          names.add(Utils.splitModuleName(element_ctx.getText()));
        }
        // adding the concatenation module.name for all the names in the list
        for(List<String> name: names) {
          imports.add(new Environment.SearchPath<String>(new Environment.PathElement<>(name), true));
        }

      }
    }
    _env.resetSearchPath();
    for(Environment.SearchPath<String> path: imports) { _env.addSearchPath(path); }

    List<Environment.SearchPath<String>> exports = new LinkedList<>(); // TODO: no management of exports for now


    // 2. manage all declaration
    Logger.LOGGER.beginIndent();
    List<ASTNode> declarations = new LinkedList<>();
    for(CTSParser.DeclContext declaration: ctx.decl()) {
      declarations.add(visitDeclaration(declaration));
    }

    Module res = new Module(_module_name, imports, exports, declarations, null);

    Logger.LOGGER.endIndent();

    // 3. manage the main
    CTSParser.BlockContext main_ctx = ctx.block();
    StatementBlock main = null;
    if(main_ctx != null) {
      Logger.LOGGER.beginIndent();
      Logger.LOGGER.logDebug(" -> The module \"" + _module_name + "\" has a main");

      _dependencies = new HashSet<>();
      _currentDeclaration = res;
      main = visitBlock(main_ctx);
      _env.put(new Environment.PathElement<String>(_module_name), res, _dependencies);
      _dependencies = new HashSet<>();
      _currentDeclaration = null;
      res.setMain(main);

      Logger.LOGGER.endIndent();
    }


    return res;
  }



  /*********************************************/
  /*********** DECLARATIONS ********************/
  /*********************************************/

  public ASTNode visitDeclaration(CTSParser.DeclContext ctx) {
    ASTNode res = null;

    if(ctx instanceof CTSParser.DeclarationDataTypeContext) {
      res = visitDeclarationDataType(((CTSParser.DeclarationDataTypeContext) ctx).decl_datatype());
    } else if(ctx instanceof CTSParser.DeclarationDataTypeAliasContext) {
      res = visitDeclarationTypeAlias(((CTSParser.DeclarationDataTypeAliasContext) ctx).decl_typesyn());
    } else if(ctx instanceof CTSParser.DeclarationFunctionContext) {
      res = visitDeclarationFunction(((CTSParser.DeclarationFunctionContext) ctx).decl_function());
    } else if(ctx instanceof CTSParser.DeclarationInterfaceContext) {
      res = visitDeclarationInterface(((CTSParser.DeclarationInterfaceContext) ctx).decl_interface());
    } else if(ctx instanceof CTSParser.DeclarationClassContext) {
      res = visitDeclarationClass(((CTSParser.DeclarationClassContext) ctx).decl_class());
    }
    return res;
  }


  private DeclarationDataType visitDeclarationDataType(CTSParser.Decl_datatypeContext ctx) {
    // 1. The Name of the DataType
    String name = ctx.name_module_type_constructor().getText();
    DeclarationDataType res = new DeclarationDataType(_module_name, name);

    // 2. The Parameters of the DataType
    CTSParser.Pattern_type_listContext type_parameters_ctx = ctx.pattern_type_list();
    _mapTypeVariable = new HashMap<>();
    if(type_parameters_ctx != null) {
      List<String> parameters_name = new LinkedList<>();
      for(CTSParser.Name_module_type_constructorContext name_ctx: type_parameters_ctx.name_module_type_constructor()) { parameters_name.add(name_ctx.getText()); }
      for(String parameter_name: parameters_name) {
        _mapTypeVariable.put(parameter_name, res.addTypeParameter(parameter_name));
      }
    }

    // 3. The Constructors
    for(CTSParser.Decl_data_constructorContext constructor_ctx: ctx.decl_data_constructor()) {
      res.addDataTypeConstructor(visitDeclarationDataTypeConstructor(res, constructor_ctx));
    }

    // 4. register the declared type in the environment
    _env.put(new Environment.PathElement<String>(res.getNameAsList()), res, new HashSet<Environment.PathElement<String>>());
    return res;
  }


  private DeclarationDataTypeConstructor visitDeclarationDataTypeConstructor(DeclarationDataType decl, CTSParser.Decl_data_constructorContext ctx) {
    // 1. The Name of the DataType Constructor
    String name = ctx.name_module_type_constructor().getText();
    DeclarationDataTypeConstructor res = new DeclarationDataTypeConstructor(decl, _module_name, name);

    _dependencies = new HashSet<>();
    _currentDeclaration = res;

    // 2. The Parameters of the DataType Constructor
    if(ctx.exp_type() != null) {
      for (CTSParser.Exp_typeContext type_ctx : ctx.exp_type()) {
        res.addParameter(visitExpressionType(type_ctx));
      }
    }

    // 3. register the declared constructor in the environment
    _env.put(new Environment.PathElement<String>(res.getNameAsList()), res, _dependencies);

    // 4. reset temporary sets
    _dependencies = new HashSet<>();
    _currentDeclaration = null;

    return res;
  }


  private DeclarationDataTypeAlias visitDeclarationTypeAlias(CTSParser.Decl_typesynContext ctx) {
    // 1. The Name of the DataType Constructor
    String name = ctx.name_module_type_constructor().getText();
    DeclarationDataTypeAlias res = new DeclarationDataTypeAlias(_module_name, name);

    // 2. The Parameters of the DataType
    CTSParser.Pattern_type_listContext type_parameters_ctx = ctx.pattern_type_list();
    _mapTypeVariable = new HashMap<>();
    if(type_parameters_ctx != null) {
      List<String> parameters_name = new LinkedList<>();
      for(CTSParser.Name_module_type_constructorContext name_ctx: type_parameters_ctx.name_module_type_constructor()) { parameters_name.add(name_ctx.getText()); }
      for(String parameter_name: parameters_name) {
        _mapTypeVariable.put(parameter_name, res.addTypeParameter(parameter_name));
      }
    }

    _dependencies = new HashSet<>();
    _currentDeclaration = res;

    // 3. set the definition of the alias
    res.setDefinition(visitExpressionType(ctx.exp_type()));
    _env.put(new Environment.PathElement<String>(res.getNameAsList()), res, _dependencies);

    // 4. reset temporary sets
    _mapTypeVariable = new HashMap<>();
    _dependencies = new HashSet<>();
    _currentDeclaration = null;

    return res;
  }


  private DeclarationFunction visitDeclarationFunction(CTSParser.Decl_functionContext ctx) {
    // 1. The Name of the DataType Constructor
    String name = ctx.name_val().getText();
    DeclarationFunction res = new DeclarationFunction(_module_name, name);

    // 2. The Type Parameters of the function
    CTSParser.Pattern_type_listContext type_parameters_ctx = ctx.pattern_type_list();
    _mapTypeVariable = new HashMap<>();
    if(type_parameters_ctx != null) {
      List<String> parameters_name = new LinkedList<>();
      for (CTSParser.Name_module_type_constructorContext name_ctx : type_parameters_ctx.name_module_type_constructor()) {
        parameters_name.add(name_ctx.getText());
      }
      for (String parameter_name : parameters_name) {
        _mapTypeVariable.put(parameter_name, res.addTypeParameter(parameter_name));
      }
    }

    _dependencies = new HashSet<>();
    _currentDeclaration = res;

    // 3. The parameters of the function
    _mapVariable.newScope();
    CTSParser.Pattern_val_listContext params_ctx = ctx.pattern_val_list();
    if(params_ctx != null) {
      for (CTSParser.Pattern_valContext param_ctx : params_ctx.pattern_val()) {
        DeclarationVariable var = visitDeclarationVariable(param_ctx);
        _mapVariable.putVariable(var.getName(), var);
        res.addParameter(var);
      }
    }

    // 4. The returned type of the function
    res.setReturnedType(visitExpressionType(ctx.exp_type()));

    // 5. The definition of the function
    if(ctx.exp_pure() != null) { // might be null: when the function is builtin
      res.setDefinition(visitExpressionPure(ctx.exp_pure()));
    }

    _env.put(new Environment.PathElement<String>(res.getNameAsList()), res, _dependencies);

    // 6. reset temporary sets
    _mapTypeVariable = new HashMap<>();
    _mapVariable.popScope();
    _dependencies = new HashSet<>();
    _currentDeclaration = null;

    return res;
  }



  private DeclarationInterface visitDeclarationInterface(CTSParser.Decl_interfaceContext ctx) {
    // 1. The Name of the DataType Constructor
    String name = ctx.name_module_type_constructor().getText();
    DeclarationInterface res = new DeclarationInterface(_module_name, name);

    // 2. The Type Parameters of the function
    CTSParser.Pattern_type_listContext type_parameters_ctx = ctx.pattern_type_list();
    _mapTypeVariable = new HashMap<>();
    if(type_parameters_ctx != null) {
      List<String> parameters_name = new LinkedList<>();
      for (CTSParser.Name_module_type_constructorContext name_ctx : type_parameters_ctx.name_module_type_constructor()) {
        parameters_name.add(name_ctx.getText());
      }
      for (String parameter_name : parameters_name) {
        _mapTypeVariable.put(parameter_name, res.addTypeParameter(parameter_name));
      }
    }

    _dependencies = new HashSet<>();
    _currentDeclaration = res;

    // 3. The extended interfaces (with automatic management of dependencies)
    CTSParser.Exp_type_listContext extended_ctx = ctx.exp_type_list();
    if(extended_ctx != null) {
      for (CTSParser.Exp_typeContext type_ctx : extended_ctx.exp_type()) {
        res.addExtendedInterface(visitExpressionType(type_ctx));
      }
    }

    // 4. The methods of the interface
    _mapVariable.newScope();  // should not be necessary, but it cannot hurt to reset the environment, just in case...
    for(CTSParser.Decl_methodContext method_ctx: ctx.decl_method()) {
      _mapGroupName = new HashMap<>();
      _mapVariable.newScope();
      DeclarationMethod method = visitDeclarationMethod(method_ctx, name);
      res.addMethod(method);
      _mapVariable.popScope(); // remove variable declaration made during parsing the method declaration
    }

    _env.put(new Environment.PathElement<String>(res.getNameAsList()), res, _dependencies);


    // 5. reset temporary sets
    _mapTypeVariable = new HashMap<>();
    _mapGroupName = new HashMap<>();
    _mapVariable.popScope();
    _dependencies = new HashSet<>();
    _currentDeclaration = null;

    return res;
  }


  private DeclarationClass visitDeclarationClass(CTSParser.Decl_classContext ctx) {
    // 1. The Name of the DataType Constructor
    String name = ctx.name_module_type_constructor().getText();
    DeclarationClass res = new DeclarationClass(_module_name, name);

    Logger.LOGGER.logDebug("Parsing the class \"" + name + "\"");
    Logger.LOGGER.beginIndent();


    // 2. The Type Parameters of the class
    CTSParser.Pattern_type_listContext type_parameters_ctx = ctx.pattern_type_list();
    _mapTypeVariable = new HashMap<>();
    if(type_parameters_ctx != null) {
      List<String> parameters_name = new LinkedList<>();
      for (CTSParser.Name_module_type_constructorContext name_ctx : type_parameters_ctx.name_module_type_constructor()) {
        parameters_name.add(name_ctx.getText());
      }
      for (String parameter_name : parameters_name) {
        _mapTypeVariable.put(parameter_name, res.addTypeParameter(parameter_name));
      }
    }

    _dependencies = new HashSet<>();
    _currentDeclaration = res;

    // 3. The parameters of the class
    _mapVariable.newScope();
    CTSParser.Pattern_val_listContext params_ctx = ctx.pattern_val_list();
    if(params_ctx != null) {
      for (CTSParser.Pattern_valContext param_ctx : params_ctx.pattern_val()) {
        DeclarationVariable var = visitDeclarationVariable(param_ctx);
        _mapVariable.putVariable(var.getName(), var);
        res.addParameter(var);
      }
    }

    // 4. The extended interfaces (with automatic management of dependencies)
    CTSParser.Exp_type_listContext implemented_ctx = ctx.exp_type_list();
    if(implemented_ctx != null) {
      for (CTSParser.Exp_typeContext type_ctx : implemented_ctx.exp_type()) {
        res.addImplementedInterface(visitExpressionType(type_ctx));
      }
    }

    // 5. The fields of the class
    for(CTSParser.Decl_fieldContext field_ctx: ctx.decl_field()) {
      DeclarationVariable var = visitDeclarationField(field_ctx);
      res.addField(var);
      _mapVariable.putVariable(var.getName(), var);
    }

    // 6. The methods of the class
    for(CTSParser.MethodContext method_ctx: ctx.method()) {
      res.addMethod(visitMethod(method_ctx, name));
    }

    // 7. The constructor of the class
    if(ctx.block() != null) { res.setInit(visitBlock(ctx.block())); }

    _env.put(new Environment.PathElement<String>(res.getNameAsList()), res, _dependencies);

    // 8. reset temporary sets
    _mapTypeVariable = new HashMap<>();
    _mapVariable.popScope();
    _dependencies = new HashSet<>();
    _currentDeclaration = null;

    Logger.LOGGER.endIndent();


    return res;

  }

  private DeclarationVariable visitDeclarationVariable(CTSParser.Pattern_valContext ctx) {
    String name = ctx.name_val().getText();
    Logger.LOGGER.logDebug("Parsing Variable \"" + name + "\"");
    Logger.LOGGER.beginIndent();
    ITypeUse type = visitExpressionType(ctx.exp_type());
    Logger.LOGGER.endIndent();
    return new DeclarationVariable(type, name, false, null);
  }
  private DeclarationVariable visitDeclarationField(CTSParser.Decl_fieldContext ctx) {
    String name = ctx.name_val().getText();
    Logger.LOGGER.logDebug("Parsing Variable \"" + name + "\"");
    Logger.LOGGER.beginIndent();
    ITypeUse type = visitExpressionType(ctx.exp_type());
    boolean is_port = (ctx.PORT() != null);
    IExpressionPure exp = null; if(ctx.exp_pure() != null) { exp = visitExpressionPure(ctx.exp_pure()); }
    Logger.LOGGER.endIndent();
    return new DeclarationVariable(type, name, is_port, exp);
  }


  private DeclarationMethod visitDeclarationMethod(CTSParser.Decl_methodContext ctx, String name_class) {
    // 1. The Name of the method
    String name = ctx.name_val().getText();
    DeclarationMethod res = new DeclarationMethod(_module_name, name_class, name, (ctx.CRITICAL() != null));

    Logger.LOGGER.logDebug("Parsing Method \"" + name + "\"");
    Logger.LOGGER.beginIndent();

    // 2. the parameters of the method
    CTSParser.Pattern_val_listContext params_ctx = ctx.pattern_val_list();
    if(params_ctx != null) {
      for (CTSParser.Pattern_valContext param_ctx : params_ctx.pattern_val()) {
        DeclarationVariable var = visitDeclarationVariable(param_ctx);
        _mapVariable.putVariable(var.getName(), var);
        res.addParameter(var);
      }
    }

    // 3. Computation of the type of This
    CTSParser.Decl_this_typeContext this_ctx = ctx.decl_this_type();
    if(this_ctx != null) {
      List<GroupName> groups = new LinkedList<>();
      for(CTSParser.Name_valContext group_name_ctx: this_ctx.name_val()) {
        String group_name = group_name_ctx.getText();
        GroupName group = getGroupNameFromName(group_name);
        groups.add(group);
      }
      res.setGroupCreated(groups);
      TypeUseDeclaredType type = ((IDeclarationWithInstance) _currentDeclaration).createInstance();
      IRecord thisRecord = visitRecord(this_ctx.record());
      Logger.LOGGER.logDebug("1. found a record for this: " + thisRecord.toString());
      type.setRecord(thisRecord);
      Logger.LOGGER.logDebug("2 .Check the record for this: " + type.getRecord().toString());
      Logger.LOGGER.logDebug("3. Check the type for this: " + type.toString());
      res.setThis(type);
      Logger.LOGGER.logDebug("4. Checking the type of this:" + res.getThis().getType());
    } else {
      res.setThis(((IDeclarationWithInstance) _currentDeclaration).createInstance());
    }

    // 4. Returned type of the method
    res.setReturnedType(visitExpressionType(ctx.exp_type()));


    Logger.LOGGER.logDebug("5. Checking the type of this:" + res.getThis().getType());

    // 5. add the declared method to the environment
    Environment.PathElement<String> method_path = new Environment.PathElement<>(res.getNameAsList());
    _env.put(method_path, res, new HashSet<EnvironmentDeclatation.PathElement<String>>()); // dependencies are managed at the level of the interface or class

    Logger.LOGGER.endIndent();

    return res;
  }



  private DeclarationMethod visitMethod(CTSParser.MethodContext ctx, String name_class) {
    _mapGroupName = new HashMap<>();
    _mapVariable.newScope();
    DeclarationMethod decl = visitDeclarationMethod(ctx.decl_method(), name_class);

    Logger.LOGGER.beginIndent();

    _mapVariable.putVariable(Reference.this_name, decl.getThis());
    decl.setBlock(visitBlock(ctx.block()));
    _mapVariable.popScope();
    _mapGroupName = new HashMap<>();

    Logger.LOGGER.endIndent();

    return decl;
  }




  /*********************************************/
  /*********** STATEMENTS **********************/
  /*********************************************/

  private IStatement visitStatement(CTSParser.StmtContext ctx) {
    IStatement res = null;

    if(ctx instanceof CTSParser.StatementDeclarationVariableContext) {
      res = visitStatementDeclarationVariable((CTSParser.StatementDeclarationVariableContext) ctx);
    } else if(ctx instanceof CTSParser.StatementAssignContext) {
      res = visitStatementAssign((CTSParser.StatementAssignContext) ctx);
    } else if(ctx instanceof CTSParser.StatementAwaitContext) {
      res = visitStatementAwait((CTSParser.StatementAwaitContext) ctx);
    } else if(ctx instanceof CTSParser.StatementSuspendContext) {
      res = visitStatementSuspend((CTSParser.StatementSuspendContext) ctx);
    } else if(ctx instanceof CTSParser.StatementConditionContext) {
      res = visitStatementCondition((CTSParser.StatementConditionContext) ctx);
    } else if(ctx instanceof CTSParser.StatementWhileContext) {
      res = visitStatementWhile((CTSParser.StatementWhileContext) ctx);
    } else if(ctx instanceof CTSParser.StatementBlockContext) {
      res = visitBlock(((CTSParser.StatementBlockContext)ctx).block());
    } else if(ctx instanceof CTSParser.StatementReturnContext) {
      res = visitStatementReturn((CTSParser.StatementReturnContext) ctx);
    } else if(ctx instanceof CTSParser.StatementExpressionContext) {
      res = visitStatementExpression((CTSParser.StatementExpressionContext) ctx);
    } else if(ctx instanceof CTSParser.StatementRebindContext) {
      res = visitStatementRebind((CTSParser.StatementRebindContext) ctx);
    } else if(ctx instanceof CTSParser.StatementSkipContext) {
      res = new StatementSkip();
    }

    return res;
  }


  private StatementBlock visitBlock(CTSParser.BlockContext ctx) {
    StatementBlock res =  new StatementBlock();

    for(CTSParser.StmtContext stmt_ctx: ctx.stmt()) {
      res.addStatement(visitStatement(stmt_ctx));
    }

    return res;
  }


  private StatementDeclaration visitStatementDeclarationVariable(CTSParser.StatementDeclarationVariableContext ctx) {
    ITypeUse type = visitExpressionType(ctx.exp_type());
    String name = ctx.name_val().getText();
    IExpression exp = null; if(ctx.exp_eff() != null) { exp = visitExpression(ctx.exp_eff()); }
    DeclarationVariable decl = new DeclarationVariable(type, name, false, exp);
    _mapVariable.putVariable(name, decl);

    return new StatementDeclaration(decl);
  }


  private StatementAssign visitStatementAssign(CTSParser.StatementAssignContext ctx) {
    DeclarationVariable var = getDeclarationFromReference(ctx.val_reference());
    IExpression exp = visitExpression(ctx.exp_eff());

    return new StatementAssign(var, exp);
  }



  private StatementAwait visitStatementAwait(CTSParser.StatementAwaitContext ctx) {
    return new StatementAwait(visitGuard(ctx.guard()));
  }


  private StatementSuspend visitStatementSuspend(CTSParser.StatementSuspendContext ctx) {
    return new StatementSuspend();
  }


  private StatementCondition visitStatementCondition(CTSParser.StatementConditionContext ctx) {
    IExpressionPure exp = visitExpressionPure(ctx.exp_pure());
    List<CTSParser.StmtContext> list = ctx.stmt();
    IStatement stmt_if = visitStatement(list.get(0));
    IStatement stmt_else = (list.size() > 1)? (visitStatement(list.get(1))):null;
    return new StatementCondition(exp, stmt_if, stmt_else);
  }

  private StatementWhile visitStatementWhile(CTSParser.StatementWhileContext ctx) {
    IExpressionPure exp = visitExpressionPure(ctx.exp_pure());
    IStatement stmt = visitStatement(ctx.stmt());
    return new StatementWhile(exp, stmt);
  }

  private StatementReturn visitStatementReturn(CTSParser.StatementReturnContext ctx) {
    IExpression exp = visitExpression(ctx.exp_eff());
    return new StatementReturn(exp);
  }

  private StatementExpression visitStatementExpression(CTSParser.StatementExpressionContext ctx) {
    IExpression exp = visitExpression(ctx.exp_eff());
    return new StatementExpression(exp);
  }

  private StatementRebind visitStatementRebind(CTSParser.StatementRebindContext ctx) {
    IExpression obj = visitExpression(ctx.exp_eff(0));
    String field = ctx.name_val().getText();
    IExpression exp = visitExpression(ctx.exp_eff(1));

    return new StatementRebind(obj, field, exp);
  }




  /*********************************************/
  /*********** EXPRESSIONS *********************/
  /*********************************************/

  private IExpression visitExpression(CTSParser.Exp_effContext ctx) {
    IExpression res = null;

    if(ctx instanceof CTSParser.ExpressionPureContext) {
      res = visitExpressionPure(((CTSParser.ExpressionPureContext) ctx).exp_pure());
    } else if(ctx instanceof CTSParser.ExpressionGetContext) {
      res = visitExpressionGet((CTSParser.ExpressionGetContext) ctx);
    } else if(ctx instanceof CTSParser.ExpressionNewContext) {
      res = visitExpressionNew((CTSParser.ExpressionNewContext) ctx);
    } else if(ctx instanceof CTSParser.ExpressionMethodCallContext) {
      res = visitExpressionMethodCall((CTSParser.ExpressionMethodCallContext) ctx);
    } else if(ctx instanceof CTSParser.ExpressionAwaitContext) {
      res = visitExpressionAwait((CTSParser.ExpressionAwaitContext) ctx);
    }

    return res;
  }

  private ExpressionGet visitExpressionGet(CTSParser.ExpressionGetContext ctx) {
    IExpression exp = visitExpressionPure(ctx.exp_pure());
    return new ExpressionGet(exp);
  }

  private ExpressionNew visitExpressionNew(CTSParser.ExpressionNewContext ctx) {
    boolean is_local = (ctx.LOCAL() != null);

    String name_class = ctx.name_module_type_constructor().getText();
    IDeclarationClass klass = getClassFromName(name_class);

    List<IExpressionPure> params = new LinkedList<>();
    CTSParser.Exp_pure_listContext params_ctx = ctx.exp_pure_list();
    if(params_ctx != null) {
      for(CTSParser.Exp_pureContext param_ctx: params_ctx.exp_pure()) {
        params.add(visitExpressionPure(param_ctx));
      }
    }
    return new ExpressionNew(is_local, klass, params);
  }


  private ExpressionMethodCall visitExpressionMethodCall(CTSParser.ExpressionMethodCallContext ctx) {
    boolean is_async = (ctx.EMARK() != null);
    IExpressionPure obj = visitExpressionPure(ctx.exp_pure());

    String name_method = ctx.name_val().getText(); // there is no way to know now what method this is: we don't even know the type of obj

    List<IExpressionPure> params = new LinkedList<>();
    CTSParser.Exp_pure_listContext params_ctx = ctx.exp_pure_list();
    if(params_ctx != null) {
      for(CTSParser.Exp_pureContext param_ctx: params_ctx.exp_pure()) {
        params.add(visitExpressionPure(param_ctx));
      }
    }
    return new ExpressionMethodCall(is_async, obj, name_method, params);
  }

  private ExpressionAwait visitExpressionAwait(CTSParser.ExpressionAwaitContext ctx) { // TODO: the await expression is not implemented yet
    return null;
  }


  private IExpressionPure visitExpressionPure(CTSParser.Exp_pureContext ctx) {
    IExpressionPure res = null;
    if(ctx instanceof CTSParser.ExpressionBinaryOperationContext) {
      res = visitExpressionBinaryOperation((CTSParser.ExpressionBinaryOperationContext) ctx);
    } else if(ctx instanceof CTSParser.ExpressionUnaryOperationContext) {
      res = visitExpressionUnarOperation((CTSParser.ExpressionUnaryOperationContext) ctx);
    } else if(ctx instanceof CTSParser.ExpressionSubExpressionContext) {
      res = visitExpressionPure(((CTSParser.ExpressionSubExpressionContext) ctx).exp_pure());
    } else if(ctx instanceof CTSParser.ExpressionLiteralContext) {
      res = visitExpressionLiteral((CTSParser.ExpressionLiteralContext) ctx);
    } else if(ctx instanceof CTSParser.ExpressionDataTypeContext) {
      res = visitExpressionDataType((CTSParser.ExpressionDataTypeContext) ctx);
    } else if(ctx instanceof CTSParser.ExpressionReferenceContext) {
      res = visitExpressionReference((CTSParser.ExpressionReferenceContext) ctx);
    } else if(ctx instanceof CTSParser.ExpressionFunctionApplicationContext) {
      res = visitExpressionFunctionApplication((CTSParser.ExpressionFunctionApplicationContext) ctx);
    } else if(ctx instanceof CTSParser.ExpressionMatchContext) {
      res = visitExpressionMatch((CTSParser.ExpressionMatchContext) ctx);
    } else if(ctx instanceof CTSParser.ExpressionListConstructionContext) {
      res = visitExpressionListConstruction((CTSParser.ExpressionListConstructionContext) ctx);
    }
    return res;
  }

  private ExpressionFunctionApplication visitExpressionBinaryOperation(CTSParser.ExpressionBinaryOperationContext ctx) {
    IExpressionPure exp1 = visitExpressionPure(ctx.exp_pure(0));
    IExpressionPure exp2 = visitExpressionPure(ctx.exp_pure(1));
    CTSParser.Op_binaryContext op_ctx = ctx.op_binary();
    IDeclarationFunction func = null;
    if(op_ctx.PLUS() != null) {
      func = getFunctionFromPath(Reference.pathElement_binary_plus);
    } else if(op_ctx.MINUS() != null) {
      func = getFunctionFromPath(Reference.pathElement_binary_minus);
    } else if(op_ctx.TIMES() != null) {
      func = getFunctionFromPath(Reference.pathElement_binary_times);
    } else if(op_ctx.DIV() != null) {
      func = getFunctionFromPath(Reference.pathElement_binary_div);
    } else if(op_ctx.MOD() != null) {
      func = getFunctionFromPath(Reference.pathElement_binary_mod);
    } else if(op_ctx.LEQUAL() != null) {
      func = getFunctionFromPath(Reference.pathElement_binary_lequal);
    } else if(op_ctx.DIFF() != null) {
      func = getFunctionFromPath(Reference.pathElement_binary_diff);
    } else if(op_ctx.LEQ() != null) {
      func = getFunctionFromPath(Reference.pathElement_binary_leq);
    } else if(op_ctx.LSBRAC() != null) {
      func = getFunctionFromPath(Reference.pathElement_binary_lneq);
    } else if(op_ctx.GEQ() != null) {
      func = getFunctionFromPath(Reference.pathElement_binary_geq);
    } else if(op_ctx.RSBRAC() != null) {
      func = getFunctionFromPath(Reference.pathElement_binary_gneq);
    } else if(op_ctx.LAND() != null) {
      func = getFunctionFromPath(Reference.pathElement_binary_land);
    } else if(op_ctx.LOR() != null) {
      func = getFunctionFromPath(Reference.pathElement_binary_lor);
    }

    List<IExpressionPure> params = new LinkedList<>(); params.add(exp1); params.add(exp2);
    return new ExpressionFunctionApplication(func, params);
  }

  private ExpressionFunctionApplication visitExpressionUnarOperation(CTSParser.ExpressionUnaryOperationContext ctx) {
    IExpressionPure exp = visitExpressionPure(ctx.exp_pure());
    CTSParser.Op_unaryContext op_ctx = ctx.op_unary();
    IDeclarationFunction func = null;
    if(op_ctx.PLUS() != null) {
      func = getFunctionFromPath(Reference.pathElement_unary_plus);
    } else if(op_ctx.MINUS() != null) {
      func = getFunctionFromPath(Reference.pathElement_unary_minus);
    } else if(op_ctx.NEG() != null) {
      func = getFunctionFromPath(Reference.pathElement_unary_neg);
    }

    List<IExpressionPure> params = new LinkedList<>(); params.add(exp);
    return new ExpressionFunctionApplication(func, params);
  }

  private IExpressionLiteral visitExpressionLiteral(CTSParser.ExpressionLiteralContext ctx) {
    IExpressionLiteral res = null;

    if(ctx.literal().NUMBER() != null) {
      res = new ExpressionLiteralNumber(Integer.parseInt(ctx.literal().getText()));
    } else if(ctx.literal().STRING() != null) {
      res = new ExpressionLiteralString(ctx.literal().getText());
    }
    return res;
  }

  private ExpressionDataType visitExpressionDataType(CTSParser.ExpressionDataTypeContext ctx) {
    List<IExpressionPure> params = new LinkedList<>();
    if(ctx.exp_pure() != null) {
      for(CTSParser.Exp_pureContext param_ctx: ctx.exp_pure()) {
        params.add(visitExpressionPure(param_ctx));
      }
    }

    IDeclarationDataTypeConstructor dataTypeConstructor = getDataTypeConstructorFromPath(Utils.pathConstructor(ctx.name_module_type_constructor().getText()));

    return new ExpressionDataType(dataTypeConstructor, params);
  }

  private ExpressionReference visitExpressionReference(CTSParser.ExpressionReferenceContext ctx) {
    if(getDeclarationFromReference(ctx.val_reference()) == null) { Logger.LOGGER.logError("Error: no type for \"" + ctx.val_reference().getText() + "\""); }
    return new ExpressionReference(getDeclarationFromReference(ctx.val_reference()));
  }

  private ExpressionFunctionApplication visitExpressionFunctionApplication(CTSParser.ExpressionFunctionApplicationContext ctx) {
    List<IExpressionPure> params = new LinkedList<>();
    if(ctx.exp_pure() != null) {
      for(CTSParser.Exp_pureContext param_ctx: ctx.exp_pure()) {
        params.add(visitExpressionPure(param_ctx));
      }
    }

    Environment.PathElement<String> path = new Environment.PathElement<>();
    path.add(ctx.name_val().getText());
    IDeclarationFunction func = getFunctionFromPath(path);

    return new ExpressionFunctionApplication(func, params);
  }


  private ExpressionMatch visitExpressionMatch(CTSParser.ExpressionMatchContext ctx) {
    return null;
  } // TODO: the match is not implemented yet

  private ExpressionFunctionApplication visitExpressionListConstruction(CTSParser.ExpressionListConstructionContext ctx) {

    // 1. Extract the function
    String name_function = ctx.name_val().getText();
    Environment.PathElement<String> path_function = new Environment.PathElement<>(); path_function.add(name_function);
    IDeclarationFunction fun = getFunctionFromPath(path_function);

    // 2. Extract the parameter list
    List<IExpressionPure> params = new LinkedList<>();
    CTSParser.Exp_pure_listContext param_list = ctx.exp_pure_list();
    if(param_list != null) {
      for (CTSParser.Exp_pureContext param : param_list.exp_pure()) {
        params.add(visitExpressionPure(param));
      }
    }

    // 3. translate the list in a sequence of constructors
    ExpressionDataType list = new ExpressionDataType(Reference.dep_listNil, new LinkedList<IExpressionPure>());
    ListIterator<IExpressionPure> iParam = params.listIterator(params.size());
    while(iParam.hasPrevious()) {
      List<IExpressionPure> const_param = new LinkedList<>();
      const_param.add(iParam.previous());
      const_param.add(list);
      list = new ExpressionDataType(Reference.dep_listCons, const_param);
    }

    // 4. add dependencies
    _currentDeclaration.addDependency(Reference.pathElement_listNil, Reference.dep_listNil);
    _dependencies.add(Reference.pathElement_listNil);
    _currentDeclaration.addDependency(Reference.pathElement_listCons, Reference.dep_listCons);
    _dependencies.add(Reference.pathElement_listCons);


    // 5. patching up everything
    List<IExpressionPure> fun_param = new LinkedList<>(); fun_param.add(list);
    ExpressionFunctionApplication res = new ExpressionFunctionApplication(fun, fun_param);

    return res;
  }


  /*********************************************/
  /*********** GUARDS **************************/
  /*********************************************/

  private IGuard visitGuard(CTSParser.GuardContext ctx) {
    return null;
  }


  /*********************************************/
  /*********** TYPE EXPRESSIONS ****************/
  /*********************************************/

  private ITypeUse visitExpressionType(CTSParser.Exp_typeContext ctx) {
    ITypeUse res = null;

    boolean has_parameters = (ctx.LSBRAC() != null);
    List<ITypeUse> params = new LinkedList<>();
    if(has_parameters) {
      for(CTSParser.Exp_typeContext param_ctx: ctx.exp_type()) {
        params.add(visitExpressionType(param_ctx));
      }
    }

    String name = ctx.name_module_type_constructor().getText(); // the name used: Either it is a variables, either it is a declared type
    if(has_parameters || (_mapTypeVariable.get(name) == null)) {
      IDeclarationType type = getTypeFromPath(Utils.pathType(name));
      res = new TypeUseDeclaredType(type, params);
    } else {
      res = _mapTypeVariable.get(name);
    }

    return res;
  }


  private IRecord visitRecord(CTSParser.RecordContext ctx) {
    IRecord res = null;

    if(ctx instanceof CTSParser.RecordBottomContext) { res = RecordUnspecified.bot; }
    else if(ctx instanceof CTSParser.RecordConstructContext) {
      String name_group = ((CTSParser.RecordConstructContext) ctx).name_val().getText();
      GroupName group = getGroupNameFromName(name_group);
      Map<String, ITypeUse> params = new HashMap<>();
      if(((CTSParser.RecordConstructContext) ctx).record_field() != null) {
        for(CTSParser.Record_fieldContext param_ctx: ((CTSParser.RecordConstructContext) ctx).record_field()) {
          String field = param_ctx.name_val().getText();
          ITypeUse param = visitExpressionType(param_ctx.exp_type());
          params.put(field, param);
        }
      }
      res = new RecordStructure(group, params);
    }
    return res;
  }





  /*********************************************/
  /*********** HELPER METHODS ******************/
  /*********************************************/

  private DeclarationVariable getDeclarationFromReference(CTSParser.Val_referenceContext ctx) {
    String name = ctx.getText();
    //System.out.println("Reference = \"" + name + "\"");
    if (name.contains(".")) { // the reference is "this.field" => we need to search for the field in the field environment
      //String[] tmp =  name.split("\\.", 2); System.out.println("  size = " + tmp.length);
      //int i = 0; for(String tmp_name: tmp) { System.out.println("\t" + (i++) + ": \"" + tmp_name + "\""); } System.out.println("end");
      String field = name.split("\\.")[1];
      return _mapVariable.getField(field);
    } else { // the name is in the normal environment
      return _mapVariable.getVariable(name);
    }
  }


  // not useful, as we don't know the type of an expression. Having a more expressive environment could solve that issue.
  // I have to think about it.
  private IDeclarationMethod getMethodFromType(ITypeUse type, String method_name) {
    IDeclarationMethod res = null;
    if(type instanceof TypeUseDeclaredType) {
      IDeclarationType decl = ((TypeUseDeclaredType) type).getDeclarationType();
      if(decl instanceof UnsolvedDependencyType) { // we have a dependency to a type -> need to create a corresponding dependency
        Environment.PathElement<String> method_dep = new Environment.PathElement<>(((UnsolvedDependencyType) decl).getDependency());
        method_dep.add(method_name);
        UnsolvedDependencyMethod dep = new UnsolvedDependencyMethod(method_dep);
        res = _currentDeclaration.addDependency(method_dep, dep);
        _dependencies.add(method_dep);

      } else if (decl instanceof IDeclarationWithInstance) {
        System.err.println("Error #3: Calling a method on a datatype");
      }
    }
    return res;
  }

  private IDeclarationClass getClassFromName(String class_name) {
    Environment.PathElement<String> path = Utils.pathClass(class_name);
    ASTNode tmp = _env.get(path);
    if(tmp == null) { // this is not really necessary: how the environment is coded, it is enough to create a dependency and it will get solved automatically
      Logger.LOGGER.logDebug(" => Creating dependency for (class)    \"" + class_name + "\" in \"" + _currentDeclaration.getName() + "\"");
      UnsolvedDependencyType dep = new UnsolvedDependencyType(path);
      dep = _currentDeclaration.addDependency(path, dep);
      _dependencies.add(path);
      return dep;
    } else if(tmp instanceof DeclarationClass) {
      return (IDeclarationClass) tmp;
    } else {
      Logger.LOGGER.logError("Error #4: \"" + class_name + "\" is not a class");
      return null;
    }
  }

  private IDeclarationFunction getFunctionFromPath(Environment.PathElement<String> path) {
    //Logger.LOGGER.logDebug(" => Creating dependency for (function)    \"" + Utils.concatModuleName(path) + "\" in \"" + _currentDeclaration.getName() + "\"");
    UnsolvedDependencyFunction res = new UnsolvedDependencyFunction(path);
    res = _currentDeclaration.addDependency(path, res);
    _dependencies.add(path);
    return res;
  }

  private IDeclarationDataTypeConstructor getDataTypeConstructorFromPath(Environment.PathElement<String> path) {
    //Logger.LOGGER.logDebug(" => Creating dependency for (constructor) \"" + Utils.concatModuleName(path) + "\" in \"" + _currentDeclaration.getName() + "\"");
    UnsolvedDependencyDataTypeConstructor res = new UnsolvedDependencyDataTypeConstructor(path);
    res = _currentDeclaration.addDependency(path, res);
    _dependencies.add(path);
    return res;
  }

  private IDeclarationType getTypeFromPath(Environment.PathElement<String> path) {
    //Logger.LOGGER.logDebug(" => Creating dependency for (type)        \"" + Utils.concatModuleName(path) + "\" in \"" + _currentDeclaration.getName() + "\"");
    UnsolvedDependencyType res = new UnsolvedDependencyType(path);
    res = _currentDeclaration.addDependency(path, res);
    _dependencies.add(path);
    return res;
  }

  private GroupName getGroupNameFromName(String name) {
    GroupName res = _mapGroupName.get(name);
    if(res == null) { res = new GroupName(name); _mapGroupName.put(name, res); }
    return res;
  }

}
