package org.gzoumix.cts.utils;

import org.gzoumix.cts.ast.declaration.DeclarationFunction;
import org.gzoumix.cts.ast.declaration.DeclarationVariable;
import org.gzoumix.cts.ast.declaration.dependency.UnsolvedDependencyDataTypeConstructor;
import org.gzoumix.cts.ast.declaration.dependency.UnsolvedDependencyFunction;
import org.gzoumix.cts.ast.declaration.dependency.UnsolvedDependencyType;
import org.gzoumix.cts.ast.type.ITypeUse;
import org.gzoumix.cts.ast.type.TypeUseDeclaredType;
import org.gzoumix.cts.ast.type.TypeUseVariable;
import org.gzoumix.cts.typing.Environment;

import java.util.LinkedList;
import java.util.List;

public class Reference {

  public static final String this_name = "this";
  public static final String null_name = "null";

  public static final List<String> path_unit     = Utils.splitModuleName("ABS.StdLib.TYPE.Unit");
  public static final List<String> path_fut      = Utils.splitModuleName("ABS.StdLib.TYPE.Fut");
  public static final List<String> path_int      = Utils.splitModuleName("ABS.StdLib.TYPE.Int");
  public static final List<String> path_bool     = Utils.splitModuleName("ABS.StdLib.TYPE.Bool");
  public static final List<String> path_string   = Utils.splitModuleName("ABS.StdLib.TYPE.String");
  public static final List<String> path_list     = Utils.splitModuleName("ABS.StdLib.TYPE.List");
  public static final List<String> path_listNil  = Utils.splitModuleName("ABS.StdLib.CONST.Nil");
  public static final List<String> path_listCons = Utils.splitModuleName("ABS.StdLib.CONST.Cons");



  public static final Environment.PathElement<String> pathElement_unit   = new Environment.PathElement<>(path_unit);
  public static final Environment.PathElement<String> pathElement_fut    = new Environment.PathElement<>(path_fut);
  public static final Environment.PathElement<String> pathElement_int    = new Environment.PathElement<>(path_int);
  public static final Environment.PathElement<String> pathElement_bool   = new Environment.PathElement<>(path_bool);
  public static final Environment.PathElement<String> pathElement_string = new Environment.PathElement<>(path_string);
  public static final Environment.PathElement<String> pathElement_list   = new Environment.PathElement<>(path_list);


  public static final Environment.PathElement<String> pathElement_listNil = new Environment.PathElement<>(path_listNil);
  public static final Environment.PathElement<String> pathElement_listCons = new Environment.PathElement<>(path_listCons);
  public static final UnsolvedDependencyDataTypeConstructor dep_listNil  = new UnsolvedDependencyDataTypeConstructor(pathElement_listNil);
  public static final UnsolvedDependencyDataTypeConstructor dep_listCons = new UnsolvedDependencyDataTypeConstructor(pathElement_listCons);


  public static final UnsolvedDependencyType dep_unit    = new UnsolvedDependencyType(pathElement_int);
  public static final UnsolvedDependencyType dep_fut     = new UnsolvedDependencyType(pathElement_int);
  public static final UnsolvedDependencyType dep_int     = new UnsolvedDependencyType(pathElement_int);
  public static final UnsolvedDependencyType dep_bool    = new UnsolvedDependencyType(pathElement_bool);
  public static final UnsolvedDependencyType dep_string  = new UnsolvedDependencyType(pathElement_int);

  public static final ITypeUse type_unit    = new TypeUseDeclaredType(dep_unit, new LinkedList<ITypeUse>());
  // cannot create a "fut" type, because it has a parameter
  public static final ITypeUse type_int     = new TypeUseDeclaredType(dep_int, new LinkedList<ITypeUse>());
  public static final ITypeUse type_bool    = new TypeUseDeclaredType(dep_bool, new LinkedList<ITypeUse>());
  public static final ITypeUse type_string  = new TypeUseDeclaredType(dep_string, new LinkedList<ITypeUse>());


  /*
  public static final DeclarationDataType decl_unit;
  public static final DeclarationDataType decl_fut;
  public static final DeclarationDataType decl_int;
  public static final DeclarationDataType decl_bool;
  public static final DeclarationDataType decl_string;

  static {
    decl_unit = new DeclarationDataType(path_unit);
    decl_fut = new DeclarationDataType(path_fut); decl_fut.addTypeParameter("Type");
    decl_int = new DeclarationDataType(pathElement_int);
    decl_bool = new DeclarationDataType(path_bool);
    decl_string = new DeclarationDataType(path_string);
  }*/


  public static final List<String> path_unary_plus = Utils.splitModuleName("ABS.StdLib.!unary_plus!");
  public static final List<String> path_unary_minus = Utils.splitModuleName("ABS.StdLib.!unary_minus!");
  public static final List<String> path_unary_neg = Utils.splitModuleName("ABS.StdLib.!unary_neg!");

  public static final List<String> path_binary_plus = Utils.splitModuleName("ABS.StdLib.!binary_plus!");
  public static final List<String> path_binary_minus = Utils.splitModuleName("ABS.StdLib.!binary_minus!");
  public static final List<String> path_binary_times = Utils.splitModuleName("ABS.StdLib.!binary_times!");
  public static final List<String> path_binary_div = Utils.splitModuleName("ABS.StdLib.!binary_div!");
  public static final List<String> path_binary_mod = Utils.splitModuleName("ABS.StdLib.!binary_mod!");
  public static final List<String> path_binary_lequal = Utils.splitModuleName("ABS.StdLib.!binary_lequal!");
  public static final List<String> path_binary_diff = Utils.splitModuleName("ABS.StdLib.!binary_diff!");
  public static final List<String> path_binary_leq = Utils.splitModuleName("ABS.StdLib.!binary_leq!");
  public static final List<String> path_binary_lneq = Utils.splitModuleName("ABS.StdLib.!binary_lneq!");
  public static final List<String> path_binary_gneq = Utils.splitModuleName("ABS.StdLib.!binary_gneq!");
  public static final List<String> path_binary_geq = Utils.splitModuleName("ABS.StdLib.!binary_geq!");
  public static final List<String> path_binary_land = Utils.splitModuleName("ABS.StdLib.!binary_land!");
  public static final List<String> path_binary_lor = Utils.splitModuleName("ABS.StdLib.!binary_lor!");


  public static final Environment.PathElement<String> pathElement_unary_plus = new Environment.PathElement<>(path_unary_plus);
  public static final Environment.PathElement<String> pathElement_unary_minus = new Environment.PathElement<>(path_unary_minus);
  public static final Environment.PathElement<String> pathElement_unary_neg = new Environment.PathElement<>(path_unary_neg);

  public static final Environment.PathElement<String> pathElement_binary_plus = new Environment.PathElement<>(path_binary_plus);
  public static final Environment.PathElement<String> pathElement_binary_minus = new Environment.PathElement<>(path_binary_minus);
  public static final Environment.PathElement<String> pathElement_binary_times = new Environment.PathElement<>(path_binary_times);
  public static final Environment.PathElement<String> pathElement_binary_div = new Environment.PathElement<>(path_binary_div);
  public static final Environment.PathElement<String> pathElement_binary_mod = new Environment.PathElement<>(path_binary_mod);
  public static final Environment.PathElement<String> pathElement_binary_lequal = new Environment.PathElement<>(path_binary_lequal);
  public static final Environment.PathElement<String> pathElement_binary_diff = new Environment.PathElement<>(path_binary_diff);
  public static final Environment.PathElement<String> pathElement_binary_leq = new Environment.PathElement<>(path_binary_leq);
  public static final Environment.PathElement<String> pathElement_binary_lneq = new Environment.PathElement<>(path_binary_lneq);
  public static final Environment.PathElement<String> pathElement_binary_gneq = new Environment.PathElement<>(path_binary_gneq);
  public static final Environment.PathElement<String> pathElement_binary_geq = new Environment.PathElement<>(path_binary_geq);
  public static final Environment.PathElement<String> pathElement_binary_land = new Environment.PathElement<>(path_binary_land);
  public static final Environment.PathElement<String> pathElement_binary_lor = new Environment.PathElement<>(path_binary_lor);


  public static final DeclarationFunction decl_unary_plus;
  public static final DeclarationFunction decl_unary_minus;
  public static final DeclarationFunction decl_unary_neg;

  public static final DeclarationFunction decl_binary_plus;
  public static final DeclarationFunction decl_binary_minus;
  public static final DeclarationFunction decl_binary_times;
  public static final DeclarationFunction decl_binary_div;
  public static final DeclarationFunction decl_binary_mod;
  public static final DeclarationFunction decl_binary_lequal;
  public static final DeclarationFunction decl_binary_diff;
  public static final DeclarationFunction decl_binary_leq;
  public static final DeclarationFunction decl_binary_lneq;
  public static final DeclarationFunction decl_binary_gneq;
  public static final DeclarationFunction decl_binary_geq;
  public static final DeclarationFunction decl_binary_land;
  public static final DeclarationFunction decl_binary_lor;

  static { // TODO: add parameters and type
    // not possible to do so, because it depends on the declaration of the types int and bool, which are not present at the moment in the environment
    // I hence need dependencies, and this is directly that more complicated
    // To conclude, I will finalize the construction of these function in Utils, when I will have access to the environment in which I can put the dependencies

    DeclarationVariable var_int  = new DeclarationVariable(type_int, "param_int", false, null);
    DeclarationVariable var_bool = new DeclarationVariable(type_bool, "param_bool", false, null);
    TypeUseVariable var_type;
    DeclarationVariable var;

    decl_unary_plus = new DeclarationFunction(path_unary_plus);
    decl_unary_plus.addParameter(var_int); decl_unary_plus.setReturnedType(type_int);
    decl_unary_minus = new DeclarationFunction(path_unary_minus);
    decl_unary_minus.addParameter(var_int); decl_unary_minus.setReturnedType(type_int);
    decl_unary_neg = new DeclarationFunction(path_unary_neg);
    decl_unary_neg.addParameter(var_bool); decl_unary_neg.setReturnedType(type_bool);

    decl_binary_plus = new DeclarationFunction(path_binary_plus);
    decl_binary_plus.addParameter(var_int); decl_binary_plus.addParameter(var_int); decl_binary_plus.setReturnedType(type_int);
    decl_binary_minus = new DeclarationFunction(path_binary_minus);
    decl_binary_minus.addParameter(var_int); decl_binary_minus.addParameter(var_int); decl_binary_minus.setReturnedType(type_int);
    decl_binary_times = new DeclarationFunction(path_binary_times);
    decl_binary_times.addParameter(var_int); decl_binary_times.addParameter(var_int); decl_binary_times.setReturnedType(type_int);
    decl_binary_div = new DeclarationFunction(path_binary_div);
    decl_binary_div.addParameter(var_int); decl_binary_div.addParameter(var_int); decl_binary_div.setReturnedType(type_int);
    decl_binary_mod = new DeclarationFunction(path_binary_mod);
    decl_binary_mod.addParameter(var_int); decl_binary_mod.addParameter(var_int); decl_binary_mod.setReturnedType(type_int);

    decl_binary_lequal = new DeclarationFunction(path_binary_lequal);
    var_type = decl_binary_lequal.addTypeParameter("N"); var = new DeclarationVariable(var_type, "param_N", false, null);
    decl_binary_lequal.addParameter(var); decl_binary_lequal.addParameter(var); decl_binary_lequal.setReturnedType(type_bool);
    decl_binary_diff = new DeclarationFunction(path_binary_diff);
    var_type = decl_binary_lequal.addTypeParameter("N"); var = new DeclarationVariable(var_type, "param_N", false, null);
    decl_binary_diff.addParameter(var); decl_binary_diff.addParameter(var); decl_binary_diff.setReturnedType(type_bool);

    decl_binary_leq = new DeclarationFunction(path_binary_leq);
    decl_binary_leq.addParameter(var_int); decl_binary_leq.addParameter(var_int); decl_binary_leq.setReturnedType(type_bool);
    decl_binary_lneq = new DeclarationFunction(path_binary_lneq);
    decl_binary_lneq.addParameter(var_int); decl_binary_lneq.addParameter(var_int); decl_binary_lneq.setReturnedType(type_bool);
    decl_binary_gneq = new DeclarationFunction(path_binary_gneq);
    decl_binary_gneq.addParameter(var_int); decl_binary_gneq.addParameter(var_int); decl_binary_gneq.setReturnedType(type_bool);
    decl_binary_geq = new DeclarationFunction(path_binary_geq);
    decl_binary_geq.addParameter(var_int); decl_binary_geq.addParameter(var_int); decl_binary_geq.setReturnedType(type_bool);

    decl_binary_land = new DeclarationFunction(path_binary_land);
    decl_binary_land.addParameter(var_bool); decl_binary_land.addParameter(var_bool); decl_binary_land.setReturnedType(type_bool);
    decl_binary_lor = new DeclarationFunction(path_binary_lor);
    decl_binary_lor.addParameter(var_bool); decl_binary_lor.addParameter(var_bool); decl_binary_lor.setReturnedType(type_bool);
  }
}

