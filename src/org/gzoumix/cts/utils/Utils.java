package org.gzoumix.cts.utils;

import org.gzoumix.cts.ast.declaration.IDeclaration;
import org.gzoumix.cts.ast.declaration.IDeclarationType;
import org.gzoumix.cts.ast.declaration.dependency.UnsolvedDependencyType;
import org.gzoumix.cts.ast.expression.ExpressionDataType;
import org.gzoumix.cts.ast.expression.IExpressionPure;
import org.gzoumix.cts.ast.type.ITypeUse;
import org.gzoumix.cts.ast.type.TypeUseDeclaredType;
import org.gzoumix.cts.typing.Environment;

import java.util.*;

public class Utils {


  public static List<String> splitModuleName(String s) {
    return new LinkedList<>(Arrays.asList(s.split("\\.")));
  }


  public static String concat(Collection<String> list, String sep) {
    String res = "";
    Iterator<String> i = list.iterator();
    while (i.hasNext()) {
      res = res + i.next();
      if (i.hasNext()) {
        res = res + sep;
      }
    }
    return res;
  }

  public void toto() {
    String[] tmp = { "1", "2" };
    String res = concat(Arrays.asList(tmp), "_");

  }

  public static String concatModuleName(List<String> list) { return concat(list, ".");  }


  public static Environment.PathElement<String> pathClass(String name) { return pathClass(new LinkedList<String>(), name); }
  public static Environment.PathElement<String> pathType(String name) { return pathType(new LinkedList<String>(), name); }
  public static Environment.PathElement<String> pathConstructor(String name) { return pathConstructor(new LinkedList<String>(), name); }

  public static Environment.PathElement<String> pathClass(List<String> path, String name) {
    List<String> extpath = new LinkedList<>(path);
    extpath.add("CLASS"); extpath.add(name);
    return new Environment.PathElement<String>(extpath);
  }

  public static Environment.PathElement<String> pathType(List<String> path, String name) {
    List<String> extpath = new LinkedList<>(path);
    extpath.add("TYPE"); extpath.add(name);
    return new Environment.PathElement<String>(extpath);
  }

  public static Environment.PathElement<String> pathConstructor(List<String> path, String name) {
    List<String> extpath = new LinkedList<>(path);
    extpath.add("CONST"); extpath.add(name);
    return new Environment.PathElement<String>(extpath);
  }


  public static ITypeUse futType(Environment env, ITypeUse type) {
    List<ITypeUse> params = new LinkedList<>(); params.add(type);
    return new TypeUseDeclaredType((IDeclarationType)env.get(Reference.pathElement_fut), params);
  }



  public static <N> Set<N> singleton(N el) { Set<N> res = new HashSet<>(); res.add(el); return res; }
  public static <N> Set<N> doubleleton(N el1, N el2) { Set<N> res = new HashSet<>(); res.add(el1); res.add(el2); return res; }


  // initializes the environemnt with implicitly declared functions, like +, -, *, etc
  public static void initEnvironment(Environment env) {
    env.resetSearchPath();

    Reference.decl_unary_plus.addDependency(Reference.pathElement_int, Reference.dep_int);
    env.put(Reference.pathElement_unary_plus, Reference.decl_unary_plus, singleton(Reference.pathElement_int));

    Reference.decl_unary_minus.addDependency(Reference.pathElement_int, Reference.dep_int);
    env.put(Reference.pathElement_unary_minus, Reference.decl_unary_minus, singleton(Reference.pathElement_int));

    Reference.decl_unary_neg.addDependency(Reference.pathElement_bool, Reference.dep_bool);
    env.put(Reference.pathElement_unary_neg, Reference.decl_unary_neg, singleton(Reference.pathElement_bool));


    Reference.decl_binary_plus.addDependency(Reference.pathElement_int, Reference.dep_int);
    env.put(Reference.pathElement_binary_plus, Reference.decl_binary_plus, singleton(Reference.pathElement_int));

    Reference.decl_binary_minus.addDependency(Reference.pathElement_int, Reference.dep_int);
    env.put(Reference.pathElement_binary_minus, Reference.decl_binary_minus, singleton(Reference.pathElement_int));

    Reference.decl_binary_times.addDependency(Reference.pathElement_int, Reference.dep_int);
    env.put(Reference.pathElement_binary_times, Reference.decl_binary_times, singleton(Reference.pathElement_int));

    Reference.decl_binary_div.addDependency(Reference.pathElement_int, Reference.dep_int);
    env.put(Reference.pathElement_binary_div, Reference.decl_binary_div, singleton(Reference.pathElement_int));

    Reference.decl_binary_mod.addDependency(Reference.pathElement_int, Reference.dep_int);
    env.put(Reference.pathElement_binary_mod, Reference.decl_binary_mod, singleton(Reference.pathElement_int));

    Reference.decl_binary_lequal.addDependency(Reference.pathElement_bool, Reference.dep_bool);
    env.put(Reference.pathElement_binary_lequal, Reference.decl_binary_lequal, singleton(Reference.pathElement_bool));

    Reference.decl_binary_diff.addDependency(Reference.pathElement_bool, Reference.dep_bool);
    env.put(Reference.pathElement_binary_diff, Reference.decl_binary_diff, singleton(Reference.pathElement_bool));

    Reference.decl_binary_leq.addDependency(Reference.pathElement_int, Reference.dep_int);
    Reference.decl_binary_leq.addDependency(Reference.pathElement_bool, Reference.dep_bool);
    env.put(Reference.pathElement_binary_leq, Reference.decl_binary_leq, doubleleton(Reference.pathElement_int, Reference.pathElement_bool));

    Reference.decl_binary_lneq.addDependency(Reference.pathElement_int, Reference.dep_int);
    Reference.decl_binary_lneq.addDependency(Reference.pathElement_bool, Reference.dep_bool);
    env.put(Reference.pathElement_binary_lneq, Reference.decl_binary_lneq, doubleleton(Reference.pathElement_int, Reference.pathElement_bool));

    Reference.decl_binary_gneq.addDependency(Reference.pathElement_int, Reference.dep_int);
    Reference.decl_binary_gneq.addDependency(Reference.pathElement_bool, Reference.dep_bool);
    env.put(Reference.pathElement_binary_gneq, Reference.decl_binary_gneq, doubleleton(Reference.pathElement_int, Reference.pathElement_bool));

    Reference.decl_binary_geq.addDependency(Reference.pathElement_int, Reference.dep_int);
    Reference.decl_binary_geq.addDependency(Reference.pathElement_bool, Reference.dep_bool);
    env.put(Reference.pathElement_binary_geq, Reference.decl_binary_geq, doubleleton(Reference.pathElement_int, Reference.pathElement_bool));

    Reference.decl_binary_land.addDependency(Reference.pathElement_bool, Reference.dep_bool);
    env.put(Reference.pathElement_binary_land, Reference.decl_binary_land, singleton(Reference.pathElement_bool));

    Reference.decl_binary_lor.addDependency(Reference.pathElement_bool, Reference.dep_bool);
    env.put(Reference.pathElement_binary_lor, Reference.decl_binary_lor, singleton(Reference.pathElement_bool));
  }

  public static <K,V> Map<K,V> createMap(List<K> keys, List<V> values) {
    Map<K, V> res = new HashMap<>();
    Iterator<K> iVar = keys.iterator();
    Iterator<V> iParam = values.iterator();
    while(iVar.hasNext() && iParam.hasNext()) { res.put(iVar.next(), iParam.next()); }
    return res;
  }

}

