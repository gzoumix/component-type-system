package org.gzoumix.cts.utils;

public class Logger extends org.gzoumix.logger.Logger{

  private Object o;
  public static final Logger LOGGER = new Logger();

  private Logger() {
    super();
    o = this.addLogFile(System.out);
    this.addLoggedObject(o, System.out, Level.ALL);
    //this.setLogLevel(o, Level.DEBUG);
  }

  public void log(org.gzoumix.logger.Logger.Level level, String s) { log(o, level, s); }

  public void logPanic(String s) { log(o, Level.PANIC, s); }
  public void logError(String s) { log(o, Level.ERROR, s); }
  public void logWarning(String s) { log(o, Level.WARNING, s); }
  public void logDebug(String s) { log(o, Level.DEBUG, s); }
  public void logNormal(String s) { log(o, Level.NORMAL, s); }
  public void logInfo(String s) { log(o, Level.INFO, s); }
  public void logAll(String s) { log(o, Level.ALL, s); }


  public void beginIndent() { super.beginIndent(o); }
  public void endIndent() { super.endIndent(o); }

}
