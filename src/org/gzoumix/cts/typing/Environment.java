package org.gzoumix.cts.typing;

import org.gzoumix.cts.ast.ASTNode;
import org.gzoumix.typing.environment.EnvironmentDeclatation;

import java.util.Arrays;

public class Environment extends EnvironmentDeclatation<String, ASTNode> {

  public static final String[] arrayStdLibBase = {"ABS", "StdLib"};
  public static final String[] arrayStdLibBaseType = {"ABS", "StdLib", "TYPE"};
  public static final String[] arrayStdLibBaseConst = {"ABS", "StdLib", "CONST"};
  public static final String[] arrayStdLibScheduler = {"ABS", "Scheduler"};
  public static final String[] arrayStdLibForeignInterface = {"ABS", "FLI"};
  public static final String[] arrayStdLibDeploymentComponent = {"ABS", "DC"};
  public static final String[] arrayStdLibMetaABS = {"ABS", "Meta"};
  public static final String[] arrayStdLibProductLine = {"ABS", "Productline"};

  public static final SearchPath<String> stdLibBase = new SearchPath<String>(new PathStar<String>(Arrays.asList(arrayStdLibBase)), true);
  public static final SearchPath<String> stdLibBaseType = new SearchPath<String>(new PathStar<String>(Arrays.asList(arrayStdLibBaseType)), true);
  public static final SearchPath<String> stdLibBaseConst = new SearchPath<String>(new PathStar<String>(Arrays.asList(arrayStdLibBaseConst)), true);
  public static final SearchPath<String> stdLibScheduler = new SearchPath<String>(new PathStar<String>(Arrays.asList(arrayStdLibScheduler)), true);
  public static final SearchPath<String> stdLibForeignInterface = new SearchPath<String>(new PathStar<String>(Arrays.asList(arrayStdLibForeignInterface)), true);
  public static final SearchPath<String> stdLibDeploymentComponent = new SearchPath<String>(new PathStar<String>(Arrays.asList(arrayStdLibDeploymentComponent)), true);
  public static final SearchPath<String> stdLibMetaABS = new SearchPath<String>(new PathStar<String>(Arrays.asList(arrayStdLibMetaABS)), true);
  public static final SearchPath<String> stdLibProductLine = new SearchPath<String>(new PathStar<String>(Arrays.asList(arrayStdLibProductLine)), true);

  @Override
  public void resetSearchPath() {
    super.resetSearchPath();
    addSearchPath(stdLibBase);
    addSearchPath(stdLibBaseType);
    addSearchPath(stdLibBaseConst);
    addSearchPath(stdLibScheduler);
    addSearchPath(stdLibForeignInterface);
    addSearchPath(stdLibDeploymentComponent);
    addSearchPath(stdLibMetaABS);
    addSearchPath(stdLibProductLine);
  }

}
