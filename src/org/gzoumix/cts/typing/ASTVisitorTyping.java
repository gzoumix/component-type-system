package org.gzoumix.cts.typing;

import org.gzoumix.cts.ast.CompilationUnit;
import org.gzoumix.cts.ast.Model;
import org.gzoumix.cts.ast.Module;
import org.gzoumix.cts.ast.declaration.*;
import org.gzoumix.cts.ast.expression.*;
import org.gzoumix.cts.ast.guard.IGuard;
import org.gzoumix.cts.ast.statement.*;
import org.gzoumix.cts.ast.type.*;
import org.gzoumix.cts.utils.Logger;
import org.gzoumix.cts.utils.Reference;
import org.gzoumix.cts.utils.Utils;


import java.util.*;

public class ASTVisitorTyping {



  private Environment _env;
  private DeclarationVariable _this;
  private ITypeUse _returnedType;

  /*********************************************/
  /*********** TYPING VISITOR ******************/
  /*********************************************/


  public void visitTypingModel(Model model) {
    _env = model.getEnvironment();

    for(CompilationUnit compilationUnit: model.getCompilationUnits()) {
      visitTypingCompilationUnit(compilationUnit);
    }
  }

  private void visitTypingCompilationUnit(CompilationUnit compilationUnit) {
    Logger.LOGGER.logDebug("Typing the file \"" + compilationUnit.getName() + "\"");
    Logger.LOGGER.beginIndent();
    for(Module module: compilationUnit.getModules()) {
      visitTypingModule(module);
    }
    Logger.LOGGER.endIndent();
  }

  private void visitTypingModule(Module module) {
    for(IDeclaration declaration: module.getDeclarations()) {
      visitTypingDeclaration(declaration);
    }

    if(module.hasMain()) {
      visitTypingStatementBlock(module.getMain());
    }
  }


  private void visitTypingDeclaration(IDeclaration declaration) {
    if(declaration instanceof DeclarationClass) { // for now, we only check the classes. we should also check the
      visitInheritanceDeclarationClass((DeclarationClass) declaration);
      visitTypingDeclarationClass((DeclarationClass) declaration);
    }
    else if(declaration instanceof DeclarationInterface) {
      visitInheritanceDeclarationInterface((DeclarationInterface) declaration);
    }
  }

  private void visitTypingDeclarationClass(DeclarationClass declClass) {
    Logger.LOGGER.logDebug("Typing the class \"" + declClass.getName() + "\"");
    Logger.LOGGER.beginIndent();
    // we need to check the code of the methods and the init
    for(DeclarationMethod method: declClass.getMethods()) {
      Logger.LOGGER.logDebug("Typing the method \"" + method.getMethodName() + "\"");
      Logger.LOGGER.beginIndent();
      _this = method.getThis();
      _returnedType = method.getReturnedType();
      visitTypingStatementBlock(method.getBlock());
      Logger.LOGGER.endIndent();
    }
    Logger.LOGGER.endIndent();
  }

  /// Inheritance
  private void visitInheritanceDeclarationInterface(DeclarationInterface declaration) {

  }

  private void visitInheritanceDeclarationClass(DeclarationClass declaration) {

  }



  /// Statements
  // TODO: we need to deal with the return value. How to do that? I need an environment where to store that information.

  private void visitTypingStatement(IStatement statement) {
    if(statement instanceof StatementAssign) {
      visitTypingStatementAssign((StatementAssign) statement);
    } else if(statement instanceof StatementAwait) {
      visitTypingStatementAwait((StatementAwait) statement);
    } else if(statement instanceof StatementBlock) {
      visitTypingStatementBlock((StatementBlock) statement);
    } else if(statement instanceof StatementCondition) {
      visitTypingStatementCondition((StatementCondition) statement);
    } else if(statement instanceof StatementDeclaration) {
      visitTypingStatementDeclaration((StatementDeclaration) statement);
    } else if(statement instanceof StatementExpression) {
      visitTypingStatementExpression((StatementExpression) statement);
    } else if(statement instanceof StatementRebind) {
      visitTypingStatementRebind((StatementRebind) statement);
    } else if(statement instanceof StatementReturn) {
      visitTypingStatementReturn((StatementReturn) statement);
    } else if(statement instanceof StatementSuspend) {
      visitTypingStatementSuspend((StatementSuspend) statement);
    } else if(statement instanceof StatementWhile) {
      visitTypingStatementWhile((StatementWhile) statement);
    }

  }


  private void visitTypingStatementAssign(StatementAssign stmt) {
    ITypeUse typeVar = stmt.getVar().getType();
    ITypeUse typeExp = getType(stmt.getExp());
    if(matchType(typeVar, typeExp)) {
      Logger.LOGGER.logDebug(" -- Assign -- ok");
    } else {
      Logger.LOGGER.logDebug(" -- Assign -- no");
    }
  }


  private void visitTypingStatementAwait(StatementAwait stmt) {
    visitTypingGuard(stmt.getGuard());
  }


  private void visitTypingStatementBlock(StatementBlock block) {
    for(IStatement statement: block.getStatements()) {
      visitTypingStatement(statement);
    }
  }


  private void visitTypingStatementCondition(StatementCondition stmt) {
    visitTypingStatement(stmt.getIf());
    if(stmt.hasElse()) { visitTypingStatement(stmt.getElse()); }
  }

  private void visitTypingStatementDeclaration(StatementDeclaration stmt) {
    DeclarationVariable decl = stmt.getDeclaration();
    Logger.LOGGER.logDebug("    Checking typing of Declaration of Variable \"" + decl.getName() + "\""); System.err.flush();
    if(decl.hasExpression()) {
      ITypeUse typeVar = decl.getType();
      ITypeUse typeExp = getType(decl.getExpression());
      Logger.LOGGER.logDebug("Variable \"" + decl.getName() + "\" has type " + typeVar.toString() + " and its expression has type " + typeExp.toString());
      if (matchType(typeVar, typeExp)) {
        Logger.LOGGER.logDebug(" -- Declaration -- ok");
      } else {
        Logger.LOGGER.logDebug(" -- Declaration -- no");
      }
    }
  }

  private void visitTypingStatementExpression(StatementExpression stmt) {
    visitTypingExpression(stmt.getExpression());
  }


  private void visitTypingStatementRebind(StatementRebind stmt) {
    ITypeUse typeThis = _this.getType();
    ITypeUse typeReboundObject = getType(stmt.getReboundObject());

    Logger.LOGGER.logDebug("Type of this  = "  + typeThis + " // Type of rebound object = " + typeReboundObject);

    ITypeUse typeExp  = getType(stmt.getExpression());
    if((typeThis instanceof TypeUseDeclaredType) && (typeReboundObject instanceof TypeUseDeclaredType)) {
      IRecord recordThis = ((TypeUseDeclaredType) typeThis).getRecord();
      IRecord recordReboundObject = ((TypeUseDeclaredType) typeReboundObject).getRecord();
      if((recordThis instanceof RecordStructure) && (recordReboundObject instanceof RecordStructure)) {
        if(((RecordStructure) recordThis).getGroupName().equals(((RecordStructure) recordReboundObject).getGroupName())) { // up until now, everything is ok
          // now, I have to check that the record of the field is a subRecord of the one assigned to it
          ITypeUse typeField = ((RecordStructure) recordReboundObject).getFields().get(stmt.getReboundField());
          if(typeField != null) {
            if(typeField.isSubRecordOf(typeExp)) {
              Logger.LOGGER.logDebug("Rebind Statement Typed Checked");
            } else {
              Logger.LOGGER.logError("REBIND TYPE ERROR: the rebound field \"" + stmt.getReboundField() + "\" has not the right record");
              System.exit(-1);
            }
          } else {
            Logger.LOGGER.logError("REBIND TYPE ERROR: the rebound field \"" + stmt.getReboundField() + "\" is not specified in the record");
            System.exit(-1);
          }
        } else {
          Logger.LOGGER.logError("REBIND TYPE ERROR: the this object and the rebound object do not live in the same cog");
          System.exit(-1);
        }
      } else {
        if(recordThis instanceof RecordStructure) {
          Logger.LOGGER.logError("REBIND TYPE ERROR: the record of the rebound object is unspecified (BOT)");
        } else { Logger.LOGGER.logError("REBIND TYPE ERROR: the record of this is unspecified (BOT)"); }
        System.exit(-1);
      }
    } else {
      Logger.LOGGER.logError("REBIND TYPE ERROR: the type of this is " + typeThis.getClass().getName() + " and the type of the rebound object is " + typeReboundObject.getClass().getName());
      System.exit(-1);
    }

    //TODO: for this, I need the record of this, the record of the rebound object, and I need to access the field of a record.
  }

  private void visitTypingStatementReturn(StatementReturn stmt) {
    ITypeUse typeVar = _returnedType;
    ITypeUse typeExp = getType(stmt.getExpression());
    if(matchType(typeVar, typeExp)) {
      Logger.LOGGER.logDebug(" -- Return Statement -- ok");
    } else {
      Logger.LOGGER.logDebug(" -- Return Statement -- no");
    }
  }

  private void visitTypingStatementSuspend(StatementSuspend stmt) { } // suspend is always well typed

  private void visitTypingStatementWhile(StatementWhile stmt) {
    ITypeUse typeVar = new TypeUseDeclaredType((IDeclarationType)_env.get(Reference.pathElement_bool), new LinkedList<ITypeUse>());
    ITypeUse typeExp = getType(stmt.getExpression());
    if(typeVar.equals(typeExp)) {
      Logger.LOGGER.logDebug(" -- While Expression -- ok");
    } else {
      Logger.LOGGER.logDebug(" -- While Expression -- no");
    }
    visitTypingStatement(stmt.getStatement());
  }


//// Guards


  private void visitTypingGuard(IGuard guard) {
    // TODO: for now, we consider that guards are well-typed
  }


  //// Expressions

  private void visitTypingExpression(IExpression exp) {
    // TODO: for now, we consider that expressions are well-typed
  }

  /*********************************************/
  /*********** GET_TYPE VISITOR ****************/
  /*********************************************/

  //TODO: Deal with Record

  // Hmm, it seems that we don't have principal typing in general for any expression in ABS... (and in general in Java): you can inherit twice from the same interface, with different parameters

  private ITypeUse getType(IExpression exp) {
    //Logger.LOGGER.logDebug("Typing expression of type " + exp.getClass());
    ITypeUse res = null;
    if(exp instanceof ExpressionAwait) {

    } else if(exp instanceof ExpressionDataType) {
      res = getTypeExpressionDataType((ExpressionDataType) exp);
    } else if(exp instanceof ExpressionFunctionApplication) {
      res = getTypeExpressionFunctionApplication((ExpressionFunctionApplication) exp);
    } else if(exp instanceof ExpressionGet) {
      res = getTypeExpressionGet((ExpressionGet) exp);
    } else if(exp instanceof ExpressionLiteralNumber) {
      res = getTypeExpressionLiteralNumber((ExpressionLiteralNumber) exp);
    } else if(exp instanceof ExpressionLiteralString) {
      res = getTypeExpressionLiteralString((ExpressionLiteralString) exp);
    } else if(exp instanceof ExpressionMatch) {
      res = getTypeExpressionMatch((ExpressionMatch) exp);
    } else if(exp instanceof ExpressionMethodCall) {
      res = getTypeExpressionMethodCall((ExpressionMethodCall) exp);
    } else if(exp instanceof ExpressionNew) {
      res = getTypeExpressionNew((ExpressionNew) exp);
    } else if(exp instanceof ExpressionReference) {
      res = getTypeExpressionReference((ExpressionReference) exp);
    }
    return res.canonicalForm(); // shoule not occur
  }

  private ITypeUse getTypeExpressionDataType(ExpressionDataType exp) {
    DeclarationDataType type = exp.getConstructor().getType();
    ITypeUse res = new TypeUseDeclaredType(type, type.getTypeParameters());
    // and now we apply to res the substitution coming from the match between the parameters of the Datatype and the actual types in parameter
    Iterator<ITypeUse> iTypeParam = exp.getConstructor().getParameters().iterator();
    Iterator<IExpressionPure> iParam = exp.getParameters().iterator();
    while(iTypeParam.hasNext() && iParam.hasNext()) {
      ITypeUse formalType = iTypeParam.next();
      ITypeUse realType = getType(iParam.next());
      Map<TypeUseVariable, ITypeUse> map = tExtract(formalType, realType);
      for(Map.Entry<TypeUseVariable, ITypeUse> entry: map.entrySet()) {
        res = res.replace(entry.getKey(), entry.getValue());
      }
    }
    return res;
  }

  private ITypeUse getTypeExpressionFunctionApplication(ExpressionFunctionApplication exp) {
    List<IExpressionPure> params = exp.getParams();
    List<DeclarationVariable> paramsTypes = exp.getFunction().getParameters();
    ITypeUse res = exp.getFunction().getReturnedType();
    // and now, we need to compute the matching of parameters types, and apply them to res
    Iterator<DeclarationVariable> iTypeParam = paramsTypes.iterator();
    Iterator<IExpressionPure> iParam = params.iterator();
    while(iTypeParam.hasNext() && iParam.hasNext()) {
      ITypeUse formalType = iTypeParam.next().getType();
      ITypeUse realType = getType(iParam.next());
      Map<TypeUseVariable, ITypeUse> map = tExtract(formalType, realType);
      for(Map.Entry<TypeUseVariable, ITypeUse> entry: map.entrySet()) {
        res = res.replace(entry.getKey(), entry.getValue());
      }
    }
    return res;
  }

  private ITypeUse getTypeExpressionGet(ExpressionGet exp) {
    ITypeUse type = getType(exp.getSubExpression());
    if(type instanceof TypeUseDeclaredType) {
      if(((TypeUseDeclaredType) type).getDeclarationType().equals(_env.get(Reference.pathElement_fut))) {
        return ((TypeUseDeclaredType) type).getParameters().get(0); // we suppose that the type is well construct
      } else {
        Logger.LOGGER.logError("The type of the parameter of the get is not a future: " + ((TypeUseDeclaredType) type).getDeclarationType().getName());
      }
    } else {
      Logger.LOGGER.logError("The type of the parameter of the get is typed with a variable: " + type.toString());
    }
    Logger.LOGGER.logError(" => Expect a follow up NullPointerException");
    return null;
  }

  private ITypeUse getTypeExpressionLiteralNumber(ExpressionLiteralNumber exp) {
    DeclarationDataType intType = (DeclarationDataType) _env.get(Reference.pathElement_int);
    return new TypeUseDeclaredType(intType, new LinkedList<ITypeUse>());
  }

  private ITypeUse getTypeExpressionLiteralString(ExpressionLiteralString exp) {
    DeclarationDataType stringType = (DeclarationDataType) _env.get(Reference.pathElement_string);
    return new TypeUseDeclaredType(stringType, new LinkedList<ITypeUse>());
  }

  private ITypeUse getTypeExpressionMatch(ExpressionMatch exp) { // TODO: How to do that ?
    return null;
  }

  private ITypeUse getTypeExpressionMethodCall(ExpressionMethodCall exp) {
    ITypeUse typeCallee = getType(exp.getCallee());
    ITypeUse res = null;
    if(typeCallee instanceof TypeUseDeclaredType) {
      List<ITypeUse> typeParams = ((TypeUseDeclaredType) typeCallee).getParameters();
      //if(typeParams == null) { Logger.LOGGER.logError("the parameters of the type is null. Should not happen"); }
      IDeclarationType decl = ((TypeUseDeclaredType) typeCallee).getDeclarationType();
      if (decl instanceof IDeclarationWithInstance) {
        Logger.LOGGER.logDebug("Method call -> callee is of type \"" + decl.getName() + "\" and method name is \"" + exp.getMethodName() + "\"");
        DeclarationMethod method = ((IDeclarationWithInstance) decl).getMethod(exp.getMethodName(), typeParams);
        res = method.getReturnedType();
        /* THIS CODE SHOULD NOT BE USEFUL, AS THE TYPE PARAMETERS SHOULD ENTIRELY BE DEFINED AT THE CLASS LEVEL (POSSIBLY)
        // and now, we need to compute the matching of parameters types, and apply them to res
        Iterator<DeclarationVariable> iTypeParam = method.getParameters().iterator();
        Iterator<IExpressionPure> iParam = exp.getParameters().iterator();
        while (iTypeParam.hasNext() && iParam.hasNext()) {
          ITypeUse formalType = iTypeParam.next().getType();
          ITypeUse realType = getType(iParam.next());
          Map<TypeUseVariable, ITypeUse> map = tExtract(formalType, realType);
          for (Map.Entry<TypeUseVariable, ITypeUse> entry : map.entrySet()) {
            res = res.replace(entry.getKey(), entry.getValue());
          }
        }*/
      } else {
        Logger.LOGGER.logError("Error: type \"" + ((TypeUseDeclaredType) typeCallee).getDeclarationType().getName() + "\" is not a class nor an interface");
      }
    } else {
      Logger.LOGGER.logError("Error: type \"" + typeCallee + "\" is a Type Variable :" + typeCallee.getClass());
    }
    if(exp.isAsync()) {
      res = Utils.futType(_env, res);
    }

    return res;
  }

  private ITypeUse getTypeExpressionNew(ExpressionNew exp) {
    IDeclarationClass decl = exp.getDeclarationClass();
    ITypeUse res = decl.createInstance();
    Iterator<DeclarationVariable> iTypeParam = decl.getParameters().iterator();
    Iterator<IExpressionPure> iParam = exp.getParameters().iterator();
    while(iTypeParam.hasNext() && iParam.hasNext()) {
      ITypeUse formalType = iTypeParam.next().getType();
      ITypeUse realType = getType(iParam.next());
      Map<TypeUseVariable, ITypeUse> map = tExtract(formalType, realType);
      for(Map.Entry<TypeUseVariable, ITypeUse> entry: map.entrySet()) {
        res = res.replace(entry.getKey(), entry.getValue());
      }
    }
    return res;
  }

  private ITypeUse getTypeExpressionReference(ExpressionReference exp) { return exp.getDecl().getType(); }


  /*********************************************/
  /*********** TEXTRACT FUNCTION ***************/
  /*********************************************/
  // this is necessary to compute the type of a Datatype constructor, to know what is the type of a typeVariable, Same thing for function

  private static Map<TypeUseVariable, ITypeUse> tExtract(ITypeUse t1, ITypeUse t2) {
    Map<TypeUseVariable, ITypeUse> res = new HashMap<>();
    if(t1 instanceof TypeUseVariable) {
      res.put((TypeUseVariable) t1, t2);
    } else if((t1 instanceof TypeUseDeclaredType) && (t2 instanceof TypeUseDeclaredType)) {
      Set<ITypeUse> t2supertypes = t2.superTypeUse();
      TypeUseDeclaredType t2good = null;
      for(ITypeUse tmp: t2supertypes) {
        if(tmp instanceof TypeUseDeclaredType) {
          if(((TypeUseDeclaredType) tmp).getDeclarationType().equals(((TypeUseDeclaredType) t1).getDeclarationType())) {
            t2good = (TypeUseDeclaredType) tmp;
            break;
          }
        }
      }
      if(t2good == null) { // the parameter actually is not a subtype of what is specified
        Logger.LOGGER.logError("Error: type \"" + ((TypeUseDeclaredType) t2).getDeclarationType() + "\" is not a subtype of \"" + ((TypeUseDeclaredType) t1).getDeclarationType() + "\"");
        return res;
      }
      // we now have two types using the same base type: we can hence do recursive
      Iterator<ITypeUse> iT1Params = ((TypeUseDeclaredType) t1).getParameters().iterator();
      Iterator<ITypeUse> iT2Params = t2good.getParameters().iterator();
      while(iT1Params.hasNext() && iT2Params.hasNext()) {
        res.putAll(tExtract(iT1Params.next(), iT2Params.next()));
      }
    } else {// we have two types with a list of constructors. We need to map them For that, data types would greatly improve the typing
      Logger.LOGGER.logError("Error in matching two types");
    }
    return res;
  }



  /*********************************************/
  /*********** IS_SUBTYPE FUNCTION *************/
  /*********************************************/

  // TODO: add support for inheritance, NullType, and records
  private boolean matchType(ITypeUse target, ITypeUse type) {
    // match the types: compute the set of super types of type, and see if one of them matches with target
    // 1. test the nullType case
    boolean nullTypeCase = (type instanceof TypeUseDeclaredType);
    if(nullTypeCase) { nullTypeCase = ((TypeUseDeclaredType) type).getDeclarationType().equals(NullType.NULL_TYPE); }
    if(nullTypeCase) {
      if(target instanceof TypeUseDeclaredType) {
        if (((TypeUseDeclaredType) target).getDeclarationType() instanceof DeclarationDataTypeConstructor) { return false; } // cannot assign null to a datatype
        else { return true; } // we assign null to an object: it is always safe, do not need to look at records
      } else { return false; } // cannot assign null to an element without a real type
    } else {
      // 2. check the normal case
      Set<ITypeUse> set = type.superTypeUse();
      for (ITypeUse supType : set) {
        if (target.match(supType)) {
          // 3. we found a match. We now need to check that the record matches
          if (target instanceof TypeUseVariable) { return true; } // we have the same type variable, so they have the same record
          else if((target instanceof TypeUseDeclaredType)) { // we need to control the record
            return target.isSubRecordOf(type);
          } else { return false; } // should not occur, but someone can add unexpected classes
        }
      }
    }
    return false;
  }

}
